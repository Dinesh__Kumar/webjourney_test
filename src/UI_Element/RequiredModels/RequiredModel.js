import React, { Component } from 'react'
import DefaultModel from '../../UI_Element/Models/defaultModel/DefaultModel';
import Style from './requireModel.module.css'
import congratsLogo from '../../#cashe/congrats.png';
import ic_casheLogo from '../../#cashe/ic_cashe.png'   ;


const  RequiredModel =(props)=>  {
    if(props.modelType==='ownershipstatus'){
        return(
            <div>
                <DefaultModel>
                            <div className={`${Style.educationRadioElement}`}>
                                <input type="radio" name="ownership"  required={true}
                                id="Company-Provided" value="Company-Provided" onChange={props.onChange}/>
                                <label htmlFor="Company-Provided"> Company-Provided</label>
                            </div>

                            <div className={`${Style.educationRadioElement}`}>
                                <input type="radio" name="ownership" id="Owned"
                                 value="Owned" onChange={props.onChange}/>
                                <label htmlFor="Owned"> Owned</label>
                            </div>


                            <div className={`${Style.educationRadioElement}`}>
                                <input type="radio" name="ownership" id="Rented" 
                                value="Rented" onChange={props.onChange}/>
                                <label htmlFor="Rented"> Rented</label>
                            </div>

                            <div className={`${Style.educationRadioElement}`}>
                                <input type="radio" name="ownership" id="Other" 
                                value="Other" onChange={props.onChange}/>
                                <label htmlFor="Other"> Other</label>
                            </div>


                            <div className={`container-fluid ${Style.bottmBtn}`}>
                                  <button   onClick={props.onClick} >Ok</button>
                                 </div>
        
                        </DefaultModel>

            </div>
        )
    }else if(props.modelType==="residingwith"){
return(
        <div>
        <DefaultModel>
            <h4>Residing With</h4>

                    <div className={`${Style.educationRadioElement}`}>
                        <input type="radio" name="residing"  required={true}
                        id="Family" value="Family" onChange={props.onChange}/>
                        <label htmlFor="Family"> Family</label>
                    </div>

                    <div className={`${Style.educationRadioElement}`}>
                        <input type="radio" name="residing" id="Friends"
                         value="Friends" onChange={props.onChange}/>
                        <label htmlFor="Friends"> Friends</label>
                    </div>


                    <div className={`${Style.educationRadioElement}`}>
                        <input type="radio" name="residing" id="Alone" 
                        value="Alone" onChange={props.onChange}/>
                        <label htmlFor="Alone"> Alone</label>
                    </div>

                    <div className={`container-fluid ${Style.bottmBtn}`}>
                          <button   onClick={props.onClick} >Ok</button>
                         </div>

                </DefaultModel>

    </div>
)
    }else if(props.modelType==="NOYS"){
        return(
            <div>
            <DefaultModel>
                <span style={{fontSize:'18px', padding:'5px 0px'}}>Number Of Years At Current Address</span>
    
                        <div className={`${Style.educationRadioElement}`}>
                            <input type="radio" name="NOYS"  required={true}
                            id="6m" value="6m" onChange={props.onChange}/>
                            <label htmlFor="6m"> Less than Six Month</label>
                        </div>
    
                        <div className={`${Style.educationRadioElement}`}>
                            <input type="radio" name="NOYS" id="6m2y"
                             value="6m2y" onChange={props.onChange}/>
                            <label htmlFor="6m2y"> Six month to Two Years</label>
                        </div>
    
    
                        <div className={`${Style.educationRadioElement}`}>
                            <input type="radio" name="NOYS" id="2y5y" 
                            value="2y5y" onChange={props.onChange}/>
                            <label htmlFor="2y5y"> Two Years to five Years</label>
                        </div>
                        <div className={`${Style.educationRadioElement}`}>
                            <input type="radio" name="NOYS" id="5y" 
                            value="5y" onChange={props.onChange}/>
                            <label htmlFor="5y"> More than Five Years</label>
                        </div>
    
    
                        <div className={`container-fluid ${Style.bottmBtn}`}>
                              <button   onClick={props.onClick} >Ok</button>
                             </div>
    
                    </DefaultModel>
    
        </div>
        )
    }else if(props.modelType==='successModel'){
        <div>
            <DefaultModel>
    

           {props.children}              
    
    
    
    
            </DefaultModel>
    </div>
    }else if(props.modelType==='education'){
        return(
        <DefaultModel>
        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="education" 
            id="graduate" value="Graduate"   onChange={props.onChange}/>
            <label htmlFor="graduate"> Graduate</label>
        </div>

        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="education" id="UnderGraduate"
             value="UnderGraduate"   onChange={props.onChange}/>
            <label htmlFor="UnderGraduate"> UnderGraduate</label>
        </div>


        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="education" id="Postgraduate" 
            value="Postgraduate"  onChange={props.onChange}/>
            <label htmlFor="Postgraduate"> Postgraduate</label>
        </div>


        <div className={`container-fluid ${Style.bottmBtn}`}>
              <button   onClick={props.onClick}>Ok</button>
             </div>


    </DefaultModel>
        )
    }else if(props.modelType==='employmentType'){
        return(
        <DefaultModel>
        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="employment"  required={true}
            id="Salaried full-time" value="Salaried full-time"  onChange={props.onChange}/>
            <label htmlFor="Salaried full-time"> Salaried full-time</label>
        </div>

        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="employment" id="UnEmployment"
             value="UnEmployment"   onChange={props.onChange}/>
            <label htmlFor="UnEmployment"> UnEmployment</label>
        </div>


        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="employment" id=" Self-Employed" 
            value="Self-Employed" onChange={props.onChange}/>
            <label htmlFor="Self-Employed"> Self-Employed</label>
        </div>


        <div className={`container-fluid ${Style.bottmBtn}`}>
              <button   onClick={props.onClick} >Ok</button>
             </div>


    </DefaultModel>
        )
    }else if(props.modelType==='designation'){
        return(
        <DefaultModel>
        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="designation"  required={true}
            id="Executive" value="Executive"  onChange={props.onChange}/>
            <label htmlFor="Executive"> Executive</label>
        </div>

        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="designation" id="Middle Management"
             value="Middle Management" onChange={props.onChange}/>
            <label htmlFor="Middle Management"> Middle Management</label>
        </div>


        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="designation" id="Sr.Management" 
            value="Sr.Management" onChange={props.onChange}/>
            <label htmlFor="Sr.Management">Sr.Management</label>
        </div>

        
        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="designation" id="Leadership role" 
            value="Leadership role"                      onChange={props.onChange}                  />
            <label htmlFor="Leadership role">Leadership role</label>
        </div>



        <div className={`container-fluid ${Style.bottmBtn}`}>
              <button   onClick={props.onClick} >Ok</button>
             </div>


    </DefaultModel>
        )
    }else if(props.modelType==='salaryRecevied'){
        return(
        <DefaultModel>
        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="salaryRecevied"  required={true}
            id="Cash" value="Cash"   onChange={props.onChange}/>
            <label htmlFor="Cash">Cash</label>
        </div>

        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="salaryRecevied" id="Cheque"
             value="Cheque"    onChange={props.onChange}/>
            <label htmlFor="Cheque">Cheque</label>
        </div>


        <div className={`${Style.educationRadioElement}`}>
            <input type="radio" name="salaryRecevied" id="Direct Account Transfer" 
            value="Direct Account Transfer" onChange={props.onChange}/>
            <label htmlFor="Direct Account Transfer">Direct Account Transfer</label>
        </div>


        <div className={`container-fluid ${Style.bottmBtn}`}>
              <button    onClick={props.onClick} >Ok</button>
             </div>


    </DefaultModel>
        )

    }else if(props.modelType==='welcomesuccess'){
        return (
            <div className={Style.DefaultModelStyle}>
                <DefaultModel loanApplyedBtn="loanApplyedBtn">
                                <div style={{padding:'10px',boxSizing:'border-box' ,textAlign:'center'}}>
                                    {/* <h4 style={{color:'#bf610f'}}>Yah!</h4> */}

                                    <div>
                                      <img src={congratsLogo} width="150px"/>
                                    </div>
                                    <p style={{fontSize:'18px',color:'#000000'}}> Yayy!</p>
                                <span  style={{fontSize:'18px',color:'#000000'}}>You're elegible for loan amount of
                                </span>

                                    <h4  style={{fontSize:'20px',color:"#f48120"}}>Rs.1234</h4>
                
                                </div>
                                    <div className={`container-fluid ${Style.LoanappliedbottmBtn}`}>
                                        <button   onClick={props.submitSuccessHandler}>Ok</button>
                                </div>
                           
                            </DefaultModel>

            </div>
        )
    }else if(props.modelType==='residentialAddressSuccessModel'){

        return (
            <div className={Style.DefaultModelStyle}>
                <DefaultModel loanApplyedBtn="loanApplyedBtn">
                <div style={{padding:'10px',boxSizing:'border-box'}}>
                <div className={Style.logoStyle}>
                <img src={ic_casheLogo} width="40px"/> <span style={{fontSize:'25px' }}>CASHe</span>
                </div>   
                      <span>Awesome! Thank for letting us know where you stay!</span>

                          </div>
                              <div className={`container-fluid ${Style.bottmBtn}`}>
                                  <button   onClick={props.submitSuccessHandler}>Ok</button>
                             </div>
                 </DefaultModel>

            </div>
        )
        }else if(props.modelType==='employmentDetailsSuccessModel'){

        return (
            <div className={Style.DefaultModelStyle}>
                <DefaultModel loanApplyedBtn="loanApplyedBtn">
                <div style={{padding:'10px',boxSizing:'border-box'}}>
                <div className={Style.logoStyle}>
                <img src={ic_casheLogo} width="40px"/> <span style={{fontSize:'25px' }}>CASHe</span>
                </div>   
                      <span>Awesome! Thank for letting us know where you stay!</span>

                          </div>
                              <div className={`container-fluid ${Style.bottmBtn}`}>
                                  <button   onClick={props.submitSuccessHandler}>Ok</button>
                             </div>
                 </DefaultModel>

            </div>
        )
        
    }else if(props.modelType==='bankDetailsSuccessModel'){

        return (
            <div className={Style.DefaultModelStyle}>
                <DefaultModel loanApplyedBtn="loanApplyedBtn">
                <div style={{padding:'10px',boxSizing:'border-box'}}>
                <div className={Style.logoStyle}>
                <img src={ic_casheLogo} width="40px"/> <span style={{fontSize:'25px' }}>CASHe</span>
                </div>   
                      <span>
                          Perfect! Your bank details are with us now.
                          We'll make sure you eill get the money rigth in htis account.
                      </span>

                          </div>
                              <div className={`container-fluid ${Style.bottmBtn}`}>
                                  <button   onClick={props.submitSuccessHandler}>Ok</button>
                             </div>
                 </DefaultModel>

            </div>
        )
        
    }else if(props.modelType==='jobfunction'){
        return(
            <DefaultModel>

      <span style={{fontSize:'18px', padding:'5px 0px'}}>JOB FUNCTION</span>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="jobfunction"  required={true}
                id="RD" value="RD"   onChange={props.onChange}/>
                <label htmlFor="RD">Research & Development (R & D)</label>
            </div>
    
            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="jobfunction" id="Administration"
                 value="Administration"    onChange={props.onChange}/>
                <label htmlFor="Administration">Administration</label>
            </div>
    
    
            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="jobfunction" id="Customer service" 
                value="Customer service" onChange={props.onChange}/>
                <label htmlFor="Customer service">Customer service</label>
            </div>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="jobfunction" id="Sales" 
                value="Sales" onChange={props.onChange}/>
                <label htmlFor="Sales">Sales</label>
            </div>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="jobfunction" id="Marketing" 
                value="Marketing" onChange={props.onChange}/>
                <label htmlFor="Marketing">Marketing</label>
            </div>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="jobfunction" id="IT" 
                value="IT" onChange={props.onChange}/>
                <label htmlFor="IT">IT and Engineering</label>
            </div>
    
    
            <div className={`container-fluid ${Style.bottmBtn}`}>
                  <button    onClick={props.onClick} >Ok</button>
                 </div>
    
        </DefaultModel>
            )
    }else if(props.modelType==='worksector'){
        return(
            <DefaultModel>

      <span style={{fontSize:'18px', padding:'5px 0px'}}>JOB FUNCTION</span>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="worksector"  required={true}
                id="RD" value="RD"   onChange={props.onChange}/>
                <label htmlFor="RD">Research & Development (R & D)</label>
            </div>
    
            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="worksector" id="Administration"
                 value="Administration"    onChange={props.onChange}/>
                <label htmlFor="Administration">Administration</label>
            </div>
    
    
            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="worksector" id="Customer service" 
                value="Customer service" onChange={props.onChange}/>
                <label htmlFor="Customer service">Customer service</label>
            </div>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="worksector" id="Sales" 
                value="Sales" onChange={props.onChange}/>
                <label htmlFor="Sales">Sales</label>
            </div>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="worksector" id="Marketing" 
                value="Marketing" onChange={props.onChange}/>
                <label htmlFor="Marketing">Marketing</label>
            </div>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="worksector" id="IT" 
                value="IT" onChange={props.onChange}/>
                <label htmlFor="IT">IT and Engineering</label>
            </div>
    
    
            <div className={`container-fluid ${Style.bottmBtn}`}>
                  <button    onClick={props.onClick} >Ok</button>
                 </div>
    
        </DefaultModel>
            )
    }else if(props.modelType==='organization'){
        return(
            <DefaultModel>

      <span style={{fontSize:'18px', padding:'5px 0px'}}>WORK SECTOR</span>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="organization"  required={true}
                id="Agriculture" value="Agriculture"   onChange={props.onChange}/>
                <label htmlFor="Agriculture">Agriculture</label>
            </div>
    
            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="organization" id="Banking and FInancial Services"
                 value="Banking and FInancial Services"    onChange={props.onChange}/>
                <label htmlFor="Banking and FInancial Services">Banking and Financial Services</label>
            </div>
    

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="organization" id="Information" 
                value="Information" onChange={props.onChange}/>
                <label htmlFor="Information">Information Techmology</label>
            </div>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="organization" id="Healthcare" 
                value="Healthcare" onChange={props.onChange}/>
                <label htmlFor="Healthcare">Healthcare</label>
            </div>
            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="organization"  required={true}
                id="Agriculture" value="Agriculture"   onChange={props.onChange}/>
                <label htmlFor="Agriculture">Agriculture</label>
            </div>
    
            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="organization" id="Banking and FInancial Services"
                 value="Banking and FInancial Services"    onChange={props.onChange}/>
                <label htmlFor="Banking and FInancial Services">Banking and Financial Services</label>
            </div>
    

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="organization" id="Information" 
                value="Information" onChange={props.onChange}/>
                <label htmlFor="Information">Information Techmology</label>
            </div>

            <div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="organization" id="Healthcare" 
                value="Healthcare" onChange={props.onChange}/>
                <label htmlFor="Healthcare">Healthcare</label>
            </div><div className={`${Style.educationRadioElement}`}>
                <input type="radio" name="organization"  required={true}
                id="Agriculture" value="Agriculture"   onChange={props.onChange}/>
                <label htmlFor="Agriculture">Agriculture</label>
            </div>
    
        
    
            <div className={`container-fluid ${Style.bottmBtn}`}>
                  <button    onClick={props.onClick} >Ok</button>
                 </div>
    
        </DefaultModel>
            )

    }else{
        return(
        <div>
            Noting selected
        </div>
        )
        
    }
    
    
//         return (
            
//                        <div className={` ${Style.welcomepageinner} `} id="welcomefromhead">

//                     {openSuccessModel&&
//                             <div  className={`conatiner ${Style.DefaultModelStyle}`}>
//                             <DefaultModel loanApplyedBtn="loanApplyedBtn">
//                                 <div style={{padding:'10px',boxSizing:'border-box' ,textAlign:'center'}}>
//                                     <h3>HURRAY!</h3>
//                                     <h4>LOGO</h4>
//                                 <span>{'Robo'}, by completing your profile,you can get
//                                     cash upto
//                                 </span>

//                                     <h4>Rs.xxxxxxxxxx</h4>
                
//                                 </div>
//                                     <div className={`container-fluid ${Style.LoanappliedbottmBtn}`}>
//                                         <button   onClick={submitSuccessHandler}>Ok</button>
//                                 </div>
                           
//                             </DefaultModel>
//                             </div>
//                         }
//                         {/* -------------------------------------- */}
                                
//                         {openModel&&
//                         <div  className={`${Style.DefaultModelStyle} container`}>
//                         <DefaultModel>
//                             <div className={`${Style.educationRadioElement}`}>
//                                 <input type="radio" name="education" 
//                                 id="graduate" value="Graduate" onChange={educationChangeHandler}/>
//                                 <label htmlFor="graduate"> Graduate</label>
//                             </div>

//                             <div className={`${Style.educationRadioElement}`}>
//                                 <input type="radio" name="education" id="UnderGraduate"
//                                  value="UnderGraduate" onChange={educationChangeHandler}/>
//                                 <label htmlFor="UnderGraduate"> UnderGraduate</label>
//                             </div>


//                             <div className={`${Style.educationRadioElement}`}>
//                                 <input type="radio" name="education" id="Postgraduate" 
//                                 value="Postgraduate" onChange={educationChangeHandler}/>
//                                 <label htmlFor="Postgraduate"> Postgraduate</label>
//                             </div>


//                             <div className={`container-fluid ${Style.bottmBtn}`}>
//                                   <button   onClick={educationSubmitHandler}>Ok</button>
//                                  </div>
           

//                         </DefaultModel>
//                         </div>
//                         }
// {/* ---------------------------------------------------------------------------------- */}
//                         {openOwnerstatusModel&&
//                         <div  className={`${Style.DefaultModelStyle} container-fluid`}>
//                         <DefaultModel>
//                             <div className={`${Style.educationRadioElement}`}>
//                                 <input type="radio" name="ownership"  required={true}
//                                 id="Company-Provided" value="Company-Provided" onChange={OwnerShipChangeHandler}/>
//                                 <label htmlFor="Company-Provided"> Company-Provided</label>
//                             </div>

//                             <div className={`${Style.educationRadioElement}`}>
//                                 <input type="radio" name="ownership" id="Owned"
//                                  value="Owned" onChange={OwnerShipChangeHandler}/>
//                                 <label htmlFor="Owned"> Owned</label>
//                             </div>


//                             <div className={`${Style.educationRadioElement}`}>
//                                 <input type="radio" name="ownership" id="Rented" 
//                                 value="Rented" onChange={OwnerShipChangeHandler}/>
//                                 <label htmlFor="Rented"> Rented</label>
//                             </div>

//                             <div className={`${Style.educationRadioElement}`}>
//                                 <input type="radio" name="ownership" id="Other" 
//                                 value="Other" onChange={OwnerShipChangeHandler}/>
//                                 <label htmlFor="Other"> Other</label>
//                             </div>


//                             <div className={`container-fluid ${Style.bottmBtn}`}>
//                                   <button   onClick={OwnerShipSubmitHandler} >Ok</button>
//                                  </div>
           

//                         </DefaultModel>
//                         </div>
                        
//                         }

// {/* ------------------------------------------------------------------------------------------------------------ */}
//                                 {openEmploymentTypeModel&&
//                                 <div  className={`${Style.DefaultModelStyle} container-fluid`}>
//                                 <DefaultModel>
//                                     <div className={`${Style.educationRadioElement}`}>
//                                         <input type="radio" name="employment"  required={true}
//                                         id="Salaried full-time" value="Salaried full-time" onChange={EmploymentTypeChangeHandler}/>
//                                         <label htmlFor="Salaried full-time"> Salaried full-time</label>
//                                     </div>
        
//                                     <div className={`${Style.educationRadioElement}`}>
//                                         <input type="radio" name="employment" id="UnEmployment"
//                                          value="UnEmployment" onChange={EmploymentTypeChangeHandler}/>
//                                         <label htmlFor="UnEmployment"> UnEmployment</label>
//                                     </div>
        
        
//                                     <div className={`${Style.educationRadioElement}`}>
//                                         <input type="radio" name="employment" id=" Self-Employed" 
//                                         value="Self-Employed" onChange={EmploymentTypeChangeHandler}/>
//                                         <label htmlFor="Self-Employed"> Self-Employed</label>
//                                     </div>

        
//                                     <div className={`container-fluid ${Style.bottmBtn}`}>
//                                           <button   onClick={EmploymentTypeHandler} >Ok</button>
//                                          </div>
                   
        
//                                 </DefaultModel>
//                                 </div>
                                
                                
//                             }


// {/*  --------------------------------------------------------------*/}

//                         {openDesignationModel&&
//                           <div  className={`${Style.DefaultModelStyle} container-fluid`}>
//                           <DefaultModel>
//                               <div className={`${Style.educationRadioElement}`}>
//                                   <input type="radio" name="designation"  required={true}
//                                   id="Executive" value="Executive" onChange={DesignationChangeHandler}/>
//                                   <label htmlFor="Executive"> Executive</label>
//                               </div>
  
//                               <div className={`${Style.educationRadioElement}`}>
//                                   <input type="radio" name="designation" id="Middle Management"
//                                    value="Middle Management" onChange={DesignationChangeHandler}/>
//                                   <label htmlFor="Middle Management"> Middle Management</label>
//                               </div>
  
  
//                               <div className={`${Style.educationRadioElement}`}>
//                                   <input type="radio" name="designation" id="Sr.Management" 
//                                   value="Sr.Management" onChange={DesignationChangeHandler}/>
//                                   <label htmlFor="Sr.Management">Sr.Management</label>
//                               </div>

                              
//                               <div className={`${Style.educationRadioElement}`}>
//                                   <input type="radio" name="designation" id="Leadership role" 
//                                   value="Leadership role" onChange={DesignationChangeHandler}/>
//                                   <label htmlFor="Leadership role">Leadership role</label>
//                               </div>


  
//                               <div className={`container-fluid ${Style.bottmBtn}`}>
//                                     <button   onClick={DesginationCloseHadler} >Ok</button>
//                                    </div>
             
  
//                           </DefaultModel>
//                           </div>
//                           }
// {/* ------------------------------------------------------------------------------------------- */}

//                         {openSalaryReceviedModel &&
                        
//                         <div  className={`${Style.DefaultModelStyle} container-fluid`}>
//                         <DefaultModel>
//                             <div className={`${Style.educationRadioElement}`}>
//                                 <input type="radio" name="salaryRecevied"  required={true}
//                                 id="Cash" value="Cash" onChange={SalaryReceviedChangeHandler}/>
//                                 <label htmlFor="Cash">Cash</label>
//                             </div>

//                             <div className={`${Style.educationRadioElement}`}>
//                                 <input type="radio" name="salaryRecevied" id="Cheque"
//                                  value="Cheque" onChange={SalaryReceviedChangeHandler}/>
//                                 <label htmlFor="Cheque">Cheque</label>
//                             </div>


//                             <div className={`${Style.educationRadioElement}`}>
//                                 <input type="radio" name="salaryRecevied" id="Direct Account Transfer" 
//                                 value="Direct Account Transfer" onChange={SalaryReceviedChangeHandler}/>
//                                 <label htmlFor="Direct Account Transfer">Direct Account Transfer</label>
//                             </div>


//                             <div className={`container-fluid ${Style.bottmBtn}`}>
//                                   <button   onClick={SalaryReceviedCloseHadler} >Ok</button>
//                                  </div>
//                         </DefaultModel>
//                         </div>
//                         }
                        
//                     </div>
            
//         )
// }
}

export default RequiredModel