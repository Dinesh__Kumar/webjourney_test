import React, { Component } from 'react'
import Style from './model.module.css';
import BackdropModel from '../backdrop/Backdrop';
const ShopWithCasheEModel =(props)=> {
        return (
            <div className={`container ${Style.containerHeadStyle}`}>
            <div className={` ${Style.BackdropManupulateStyle}`}>
            <BackdropModel/>
            </div>

            <div className={`${Style.containerStyle}`}>

                <button className={Style.closeModelstyle} onClick={props.closeModelHandler}>X</button>

                 {props.children}   
            </div>
        </div>

        )
    }


    export default ShopWithCasheEModel
