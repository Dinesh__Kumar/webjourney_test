import React, { Component } from 'react'
import Style from './default.module.css';
import BackdropModel from '../../backdrop/Backdrop';
const DefaultModel =(props)=> {
        return (
            <div>
            
           
            <div className={` ${Style.containerHeadStyle}`}>
            <div className={` ${Style.BackdropManupulateStyle}`}>
                <BackdropModel/>
                </div>

            <div className={` ${Style.containerStyle}`}>
                <div className={`${Style.containerinnerStyle}`} style={{padding:props.padding}}>
                {props.children} 

                </div>
            </div>
        </div>
        </div>
        )
    }


export default DefaultModel
