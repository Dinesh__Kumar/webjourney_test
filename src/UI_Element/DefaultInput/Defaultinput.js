import React, { Component, useState } from 'react'
import Style from './Defaultinput.module.css';
import TextField from '@material-ui/core/TextField';

import { orange } from '@material-ui/core/colors';
import { Input } from '@material-ui/core';
import { withStyles } from "@material-ui/core/styles";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';


const styles = {
    label: {
        fontSize: 15,

        "&$focusedLabel": {
          color: "green"
        },
        "&$erroredLabel": {
          color: "red"
        },
        
      },
      focusedLabel: {},
      erroredLabel: {},
      
      error: {},
      underline: {
        "&$error:after": {
          borderBottomColor: "red"
        },
        "&:after": {
          borderBottom: `1px solid orange`
        }
      },
  };

 

//main function
const  DefaultInput =(props)=> {


    const { classes } = props;

const [radioValue,setRadioValue]=useState('')
const [isAgree,setIsAgree]=useState(false)

const handleRadioChange=(e)=>{
    console.log(e.target)
    setRadioValue(e.target.value)
  }
  const radioAgreeHandler=(e)=>{
     //  console.log(e.target)
     console.log('hey')

      // setIsAgree(true)
      setIsAgree(!isAgree)

  }
  const changeAgreeHandler=()=>{
      setIsAgree(!isAgree)
  }

 
  
    if(props.inputType==='genderRadioButton'){

        return(
            <div className={`container-fluid ${Style.inputStyle}`}>

        <RadioGroup  name={props.name}  value={radioValue} onChange={handleRadioChange}
         className={props.iserror?`${Style.errorradioBtn}`: `${Style.radioBtn}`}>

         <div className={Style.innerradioBtn}>
         <FormControlLabel value={props.value1} control={<Radio  />} label={props.label1} />
          <FormControlLabel value={props.value2} control={<Radio />} label={props.label2} />
         </div>
          
        </RadioGroup>
               <span style={{color:'red'}}>{props.error}</span>
               {/* <div  className={` ${Style.elementContainer}  `}>
                                       
                                       <div   className={` ${Style.innerelementContainer}`} >

                                        <div className={`form-check form-check-inline ${Style.radioElement}`}>
                                        <input className={`form-check-input`} type="radio" required name="gender" id="male" value="male"/>
                                        <label className={`form-check-label`} htmlFor="male">Male</label>
                                        </div>

                                        <div className={`form-check form-check-inline ${Style.radioElement}`}>
                                        <input className="form-check-input" type="radio" name="gender" id="female" value="female"/>
                                        <label className="form-check-label" htmlFor="female">Female</label>
                                        </div>
                        </div>
                 </div> */}

            </div>
        
        )

    }else if(props.inputType==="agreeRadioButton"){
        return(

  <div className={`container ${Style.privacyagreeRadioElement}`}>

  <RadioGroup  name={props.name?props.name:'agree'}  
         value={props.agreeText?props.agreeText:isAgree} 
         onChange={props.onChange?props.onChange:changeAgreeHandler} 
        //  onClick={props.confirmHandler?props.confirmHandler:radioAgreeHandler}
        >

         <div className={Style.innerradioBtn}>
         <FormControlLabel value={props.agreeText?props.agreeText:isAgree} 
          control={<Radio checked={props.checked?props.checked:isAgree}  onClick={props.confirmHandler?props.confirmHandler:radioAgreeHandler}/>}
          label={props.agreeTexte} />
         </div>
          
  </RadioGroup>
  <label htmlFor="agree">
      {props.agreeText}

  
   </label>
{/*                                         
  <div className={`${Style.confirmRedio}`}>
  <input type="radio" name="agree"  checked={props.checked}
   id="agree" value="agree" onChange={props.onChange} onClick={props.confirmHandler}/>
   </div>
  <label htmlFor="agree">
      {props.agreeText}
  
  
   </label> */}

</div>
        )


    } else{

        return(
            <div className={`container-fluid ${Style.inputStyle}`}>
                <TextField id="standard-basic" 
                label={props.label} 
                fullWidth={true}
                 name={props.name} 
                autoComplete="off"
                onChange={props.onChangeHandler} 
                
                InputLabelProps={{
                    classes: {
                      root: classes.label,
                      focused: classes.focusedLabel,
                      error: classes.erroredLabel
                    }
                  }}

                  InputProps={{
                    classes: {
                      root: classes.underline,
                      error: classes.error
                    }
                  }}
                  
                  error={props.iserror}
                  required={props.required}
                  type={props.type}
                  name={props.name}
                  value={props.value}
                  onFocus={props.onFocus}
                  onBlur={props.onBlur}
                //   {...props}
                onKeyDown={props.onKeyDown}
                onClick={props.onClick}
                disabled={props.disabled}
                  />

                  <span className={`${Style.errorStyle}`}>{props.error}</span>


                {/* <Input type="text" label="Mobile Number" placeholder="mobile" id="standard-basic"/> */}
                
            </div>
        )
    }
}


export default withStyles(styles)(DefaultInput)