import React from "react";
import { css } from "@emotion/core";
import Loader from "react-loader-spinner";
import ic_cashe  from '../../#cashe/ic_cashe.png';

import {ClipLoader,BarLoader,PropagateLoader,ScaleLoader,RotateLoader	,SyncLoader,MoonLoader,ClockLoader,CircleLoader,RingLoader,
  PuffLoader
} from "react-spinners";

import Style from './spinner.module.css'
import  Backdrop  from '../backdrop/Backdrop2';
 
// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;
 
const Spinner = (props)=> {

    return (
      <div className={Style.loading}>

        <div className={Style.Backdrop}>
          <Backdrop/>
        </div>

        <div className={Style.Loader}>
     
        <div className={Style.logo}>
          <img src={ic_cashe} width="50px" height="50px"/>
        </div>

     <Loader
        type="TailSpin"
        color="white"
        height={60}
        secondaryColor="white"
        width={70}
        timeout={props.time?props.time:1000000*9000000} //3 secs
      />
      </div>

      </div>
    )
  
    }

  //   return (
  //     <div className="sweet-loading">
  //       <BounceLoader	
  //         css={override}
  //         width={20}
  //         height={3}
  //         color={"skyblue"}
  //         loading={props.loading}
  //       />
  //     </div>
  //   );
  // }


export default  Spinner