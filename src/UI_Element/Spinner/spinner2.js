import React from "react";
import { css } from "@emotion/core";
import Loader from "react-loader-spinner";
import ic_cashe  from '../../#cashe/ic_cashe.png';

import {ClipLoader,BarLoader,PropagateLoader,ScaleLoader,RotateLoader	,SyncLoader,MoonLoader,ClockLoader,CircleLoader,RingLoader,
  PuffLoader
} from "react-spinners";

import Style from './spinner.module.css'
 
// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;
 
const Spinner = (props)=> {

    return (
      <div className={Style.loading}>
        {/* <SyncLoader	
          css={override}
          width={1000}
          size={45}
          height={3}
          color={" rgb(253, 87, 10)"}
          loading={true}
        /> */}
        <div className={Style.logo}>
          <img src={ic_cashe} width="50px" height="50px"/>
        </div>

     <Loader
        type="TailSpin"
        color="grey"
        height={60}
        width={70}
        timeout={1000000} //3 secs
      />
      </div>
    )
  
    }

  //   return (
  //     <div className="sweet-loading">
  //       <BounceLoader	
  //         css={override}
  //         width={20}
  //         height={3}
  //         color={"skyblue"}
  //         loading={props.loading}
  //       />
  //     </div>
  //   );
  // }


export default  Spinner