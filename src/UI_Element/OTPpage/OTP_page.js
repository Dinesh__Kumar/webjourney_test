import { MergeType } from '@material-ui/icons';
import React, { useEffect, useRef, useState } from 'react'
import Style from "./otp.module.css";
const OTP_page =(props)=> {

const myref1=useRef()
const myref2=useRef()
const myref3=useRef()
const myref4=useRef()

    const [pin1,setPin1]=useState("");
    const [pin2,setPin2]=useState("");
    const [pin3,setPin3]=useState("");
    const [pin4,setPin4]=useState("");

    useEffect(()=>{
        if(pin1.length==1 && pin2.length==1 && pin3.length==1&& pin4.length==1){
            props.ValidateOTPHandler()
        }
    },[pin1,pin2,pin3,pin4])

const pinChangeHandler=(e)=>{
    console.log(e)
    console.log()
    if(e.target.id=='p1'){
        if(e.target.value.length<=e.target.maxLength){
            setPin1(e.target.value)
        }
        if(e.target.value.length==1){
            myref2.current.focus()

        }
        // if(pin1!=""){
        //   console.log()
        // }
    }
    if(e.target.id=='p2'){
        if(e.target.value.length<=e.target.maxLength){
        setPin2(e.target.value)
        }
        if(e.target.value.length==1){
            myref3.current.focus()

        }

    }
    if(e.target.id=='p3'){
        if(e.target.value.length<=e.target.maxLength){
        setPin3(e.target.value)
        }
        if(e.target.value.length==1){
            myref4.current.focus()

        }

    }
    if(e.target.id=='p4'){
        if(e.target.value.length<=e.target.maxLength){
        setPin4(e.target.value)
        }

    }
}

const onBlurHandler=(e)=>{
    console.log(e.target.value)
}

        return (
            <div className={Style.main}>
              
              <div className={Style.p1}>
                  <input type="number"
                  className={pin1.length==1?Style.success:""} 
                  id="p1"
                  onChange={pinChangeHandler}
                  maxLength={1}
                  value={pin1}
                  ref={myref1}
                  autoFocus={true}
                  onKeyPress={(e)=>e.key === 'e' && e.preventDefault()}
                  onBlur={onBlurHandler}

                  />
              </div>

              <div className={Style.p2}>
                  <input type="number"
                  maxLength={1}
                  className={pin2.length==1?Style.success:""} 

                  id="p2"
                  ref={myref2}
                  onKeyPress={(e)=>e.key === 'e' && e.preventDefault()}
                  onChange={pinChangeHandler}
                  value={pin2}
                  onBlur={onBlurHandler}

                    />
              </div>

              <div className={Style.p3}>
                  <input type="number"
                   onChange={pinChangeHandler}
                   ref={myref3}
                   className={pin3.length==1?Style.success:''} 

                   onKeyPress={(e)=>e.key === 'e' && e.preventDefault()}
                   maxLength={1}
                   value={pin3}
                   id="p3"
                   onBlur={onBlurHandler}

                    />
              </div>

              <div className={Style.p4}>
                  <input type="number"
                  maxLength={1}
                  onKeyPress={(e)=>e.key === 'e' && e.preventDefault()}
                  onChange={pinChangeHandler}
                  value={pin4}
                  className={pin4.length==1?Style.success:""} 

                  ref={myref4}
                  id="p4"
                  autoFocus={pin4.length==1&&false}
                  
                  />
              </div>
            </div>
        )
    }

export default OTP_page
