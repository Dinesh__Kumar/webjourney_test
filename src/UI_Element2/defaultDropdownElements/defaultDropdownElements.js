

import React, { useState } from 'react'
import Style from './dropdownElement.module.css'
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText'
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import { makeStyles } from '@material-ui/core/styles';
import './global.css'

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
    nested: {
      paddingLeft: theme.spacing(4),
    },
  }));

 
const DefaultDropdownElements =(props)=> {
    const classes = useStyles();

    const [open,setOpen]=useState(false)
    const handleClick=()=>{
      setOpen(!open)
    }

        return (

               <List>
             
                <div className={`${Style.labelText} container`} onClick={handleClick} style={{backgroundColor:props.bgColor}} >
                    <div className={Style.innerLabelText} >
                        
                       <p style={{color:props.labelTextColor}}>{props.label}</p>
                       <span style={{color:props.statusTextColor,
                        backgroundColor:props.rightbtnBgColor}} >{props.status}</span>
                    </div>
                   

                    {open ? <ExpandLess /> : <ExpandMore />}
                </div>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
               <div className={Style.listData}>
                    {props.children}
               </div>
               
                </List>
            </Collapse>

               </List>
                
        )
    }

    export default DefaultDropdownElements
