import React, { useState } from 'react'
import Style from './defaultModal.module.css'
import Modal from 'react-bootstrap/Modal'
// import cash62 from '../../#NewLogos/Home/Group 1324.png'
// import cash90 from '../../#NewLogos/Home/Group 1325.png'
import loanSuccssLogo from '../../#NewLogos/loansuccesslogo/loanSuccssLogo.PNG'
// import {useHistory} from 'react-router-dom'
// import { useSelector,useDispatch } from "react-redux";
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import {red} from '@material-ui/core/colors'

import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { Document, Page,pdfjs } from 'react-pdf';

const  DeafaultModal =(props)=> {

    pdfjs.GlobalWorkerOptions.workerSrc = 
    `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
const [openModal,setOpenModel]=useState(false)
const [numPages, setNumPages] = useState(null);

const [pageNumber, setPageNumber] = useState(1);

function onDocumentLoadSuccess({ numPages }) {
  setNumPages(numPages);
}


console.log(props.pdfData)
    if(props.modalType==='successModal'){

    
        return (
            <div className={`${Style.main} `}>
                <Modal
              
                show={props.openProductModalFlag}
                onHide={props.closeProductModalHandler}
                centered
                scrollable={true}
                aria-labelledby="contained-modal-title-vcenter" 
                >
                        {/* <Modal.Header >
                           <Modal.Title id="contained-modal-title-vcenter" className={Style.headerTitle}>
                                    Loan Products {props.title}
                            </Modal.Title>
                        </Modal.Header> */}

                        
                        <Modal.Body>
                                <div className={Style.ProductContainer}>
                                {props.children}

                                <p className={Style.title}>Loan Applied Successfully</p>

                                <div className={Style.logo}>
                                    <img src={loanSuccssLogo} alt='logo'/>
                                </div>


                                <p className={Style.description}>Your loan application is being reviewed. We will let you know the status shortly.</p>
                               
                                        <div className={Style.Button}>
                                            <button onClick={props.closeModalHandler}>OK</button>
                                        </div>
                                        
                               
                                </div>         
                        </Modal.Body>
                </Modal>
            </div>
        )
    

}else if (props.modalType==='All_PDF_Image_View_Modal'){

    const prevPageHadler=()=>{
        if(pageNumber>1){
            setPageNumber(pageNumber-1)

        }
    }

    const nextPageHadler=()=>{
        setPageNumber(pageNumber+1)
    }

    return (
        <div className={`${Style.main} `}>
            <Modal
            show={props.openModalFlag}
            onHide={props.closeModalHandler}
            centered
            scrollable={true}
            aria-labelledby="contained-modal-title-vcenter"
            >

                    <Modal.Body>
                            <div className={Style.BodyContainer}>
                            {/* {props.children} */}

                        

                            <Document
                                   file={props.pdfData}
                                onLoadSuccess={onDocumentLoadSuccess}
                                className={Style.pdfmain}
                                
                            >
                                <Page pageNumber={pageNumber} 
                                 height={500}

                                    // width={350}

                                scale={1.0}
                                
                                
                                />
                            </Document>


                            <p>Page {pageNumber} of {numPages}</p>

                           
                                    <div className={Style.closeBtn}>
                                        <button onClick={props.closeModalHandler}><HighlightOffIcon   style={{  fontSize:35,color:red[500]}}/></button>
                                    </div>

                                     <div className={Style.next_prev_btn}>
                                      <button onClick={prevPageHadler}><NavigateBeforeIcon style={{  fontSize:20}}/></button>
                                        <button  onClick={nextPageHadler}><NavigateNextIcon style={{  fontSize:20}}/></button>
                                    </div>
                            
                           
                            </div>         
                    </Modal.Body>
            </Modal>
        </div>
    )

}

else{

    return <div>No Modal Found</div>

}

}
    export default DeafaultModal
