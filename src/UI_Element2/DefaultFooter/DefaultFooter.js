import React, { Component,useState } from 'react'
import Style from './footer.module.css'
import DiscountIcon from '../../#NewLogos/Home/Teeny icon/discount.png'
import WalletIcon from '../../#NewLogos/Home/Teeny icon/wallet.png'
import ProfileIcon from '../../#NewLogos/Home/Teeny icon/user-circle.png'
import {NavLink} from 'react-router-dom'

const DefaultFooter=(props)=> {
    // console.log(props.disabled)

    const [currentEle,setCurrentele]=useState('profile')

    const activeHandler=()=>{

    }

    if(props.FooterType==='profileFooter'){

        return (
        <div className={`${Style.ProfileFooter} container`} >

            <NavLink className={Style.Profileicon} to="/Myprofile" exact={true} activeClassName={Style.active}>

                <img src={ProfileIcon} width="30px"/>
                <p>Profile</p>

            </NavLink>

            <NavLink className={Style.Walleticon}  to="/MyLoans" exact={true} activeClassName={Style.active}>
            <img src={WalletIcon} width="30px"/>
            <p>Loans</p>

            </NavLink>

            <NavLink className={Style.Discounticon}  to="/Offers" exact={true} activeClassName={Style.active}>
            <img src={DiscountIcon} width="30px"/>
            <p>Offers</p>

            </NavLink>
            
        </div>
        )

    }else{

    
        return (
            <div className={Style.Footer} >
            <button onClick={props.onClick} disabled={!props.disabled} 
            style={{width:props.width,height:props.height,backgroundColor:props.backgroundColor}}>
                {props.value}</button>
        </div>
        )
    }
}
    export default DefaultFooter
