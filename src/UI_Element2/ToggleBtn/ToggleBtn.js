import React, { Component } from 'react'
import Style from './ToggleButton.module.css'
const ToggleBtn =(props)=> {
        return (
            <label className={Style.switch}>
                    <input type='checkbox' onChange={props.toggleChangeHandler} checked={props.ischecked}/>
                    <span className={`${Style.slider} ${Style.round}`}></span>
          </label>
          
        )
    }

    export default ToggleBtn
