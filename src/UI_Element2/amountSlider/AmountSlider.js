import React, { Component, useEffect, useState } from 'react'
import Slider from '@material-ui/core/Slider';
import { withStyles, makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    root: {
      width: window.innerWidth - theme.spacing(3) * 3,
      // width:200

    },
    margin: {
      height: theme.spacing(3),
    },
  }));

  const PrettoSlider = withStyles({
    root: {
      color: '#f48120',
      height: 8,
    },
    thumb: {
        boxShadow:'0 1px 10px 0 black',

        boxSizing:'border-box',
      height: 24,
      width: 24,
      backgroundColor: '#fff',
      border: '2px solid #2f73da',
      marginTop: -8,
      marginLeft: -12,
      '&:focus, &:hover, &$active': {
        // boxShadow: 'inherit',
        boxShadow:'0 1px 10px 0 black',

      },
    },
    active: {},
    valueLabel: {
      left: 'calc(-50% + 4px)',
    },
    track: {
      height: 8,
      borderRadius: 4,
    },
    rail: {
      height: 8,
      // width:window.innerWidth -100,
      borderRadius: 4,
      backgroundColor:'rgb(255, 220, 154)'
    },

    mark: {
      // backgroundColor: 'orange',
      height: 4,
      // width: 2,
      // marginTop: 0,
    },

    valueLabel:{
      // backgroundColor:'red',
      marginLeft:6
    }


  })(Slider);
 

const AmountSlider =(props)=>  {
    const classes = useStyles();


    const followersMarks = [

      {
        value: 0,
        scaledValue: 7000,
        label: "7k"
      },

      {
        value: 10,
        scaledValue: 1000,
        label: "10k"
      },
      {
        value: 20,
        scaledValue: 20000,
        label: "20k"
      },
      {
        value: 30,
        scaledValue: 30000,
        label: "30k"
      },
      {
        value: 40,
        scaledValue: 40000,
        label: "40k"
      },
      {
        value: 50,
        scaledValue: 50000,
        label: "50k"
      },
      // {
      //   value: 60,
      //   scaledValue: 60000,
      //   label: "60k"
      // },
      // {
      //   value: 125,
      //   scaledValue: 100000,
      //   label: "100k"
      // },
      // {
      //   value: 150,
      //   scaledValue: 250000,
      //   label: "250k"
      // },
      // {
      //   value: 175,
      //   scaledValue: 500000,
      //   label: "500k"
      // },
      // {
      //   value: 200,
      //   scaledValue: 1000000,
      //   label: "1M"
      // }
    ];
    useEffect(()=>{

    },[])
    const scale = (value,b) => {
      let label=value.toString()
      // label.concat('K')
      return label
            // console.log(value,b)

      // const previousMarkIndex = Math.floor(value / 15);
      // const previousMark = followersMarks[previousMarkIndex];
      // const remainder = value % 15;

      // console.log(previousMarkIndex,previousMark,remainder)
      // if (remainder === 0) {
      //   return previousMark.scaledValue;
      // }
      // const nextMark = followersMarks[previousMarkIndex + 1];
      // const increment = (nextMark.scaledValue - previousMark.scaledValue) / 15;
      // console.log(increment,nextMark)
      // return remainder * increment + previousMark.scaledValue;
    };
    
    function numFormatter(num) {
      if (num > 999 && num <= 50000) {
        return (num / 1000).toFixed(0) + "K"; // convert to K for number from > 1000 < 1 million
      } else if (num >= 1000000) {
        return (num / 1000000).toFixed(0) + "M"; // convert to M for number from > 1 million
      } else if (num < 900) {
        return num; // if value < 1000, nothing to do
      }
    }

  
        return (
          // <div className={'container'} >

                        <PrettoSlider
                           onChange={props.onChange}
                            className={classes.root}
                            //  value={props.value}
                             aria-label="pretto slider"
                             min={props.min?props.min:7}
                            step={1}
                            max={props.max?props.max:50}
                            defaultValue={props.defaultValue?props.defaultValue:20000}
                            valueLabelFormat={props.numFormatter?props.numFormatter:numFormatter}
                            marks={props.followersMarks?props.followersMarks:followersMarks}
                            scale={props.scale?props.scale:scale}
                            valueLabelDisplay="auto"
                            aria-labelledby="non-linear-slider"
                            onChangeCommitted={props.onChangeCommitted}

                            />
            // </div>
        )
    }
export default AmountSlider
