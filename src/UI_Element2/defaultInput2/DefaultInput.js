import React, { Component, useState } from 'react'
import Style from './Defaultinput.module.css';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';

import { orange } from '@material-ui/core/colors';
import { Input } from '@material-ui/core';
import { withStyles } from "@material-ui/core/styles";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Autocomplete from '@material-ui/lab/Autocomplete';

import FormLabel from '@material-ui/core/FormLabel';
import  '../../UI_Element/GlobalCss/globalStyle.css';

const styles = {
    label: {
        fontSize: 18,

        "&$focusedLabel": {
          color: "#ea7713"
        },
        "&$erroredLabel": {
          color: "red"
        },
        
      },
      focusedLabel: {},
      erroredLabel: {},
      
      cssOutlinedInput: {
        '&$outlineerror $notchedOutline': {
          borderColor: `red !important`,
        },
        '&$focusedError $notchedOutline':{
          borderColor:`#ea7713`
        }

        // '& $notchedOutline':{
        //     borderColor:'grey !important'
        // }
        
      },
      outlineerror: {},
      focusedError:{},
      notchedOutline:{},
    
      //----------------------------

      option: {
        // Hover
        '&[data-focus="true"]': {
          backgroundColor: '#ffa500',
          borderColor: 'transparent',
        },
        // Selected
        '&[aria-selected="true"]': {
          backgroundColor: '#ffa500',
          borderColor: 'transparent',
        },
        
      },
     
  };

  // const styles = (theme) => ({
  //   ////....
  //   option: {
  //     // Hover
  //  with light-grey
  //     '&[data-focus="true"]': {
  //       backgroundColor: '#F8F8F8',
  //       borderColor: 'transparent',
  //     },
  //     // Selected
  //  has dark-grey
  //     '&[aria-selected="true"]': {
  //       backgroundColor: 'grey',
  //       borderColor: 'transparent',
  //     },
  //   },
  // });


//main function
const  DefaultInput =(props)=> {
// console.log(props.iserror)

    const { classes } = props;

const [radioValue,setRadioValue]=useState('')
const [isAgree,setIsAgree]=useState(false)

const ArrayData=["Cashe","Amazon","Filpkart","Google","Cashe","Amazon","Filpkart","Google",
"Cashe","Amazon","Filpkart","Google",
"Cashe","Amazon","Filpkart","Google",
"Cashe","Amazon","Filpkart","Google","Cashe",
"Amazon","Filpkart","Google"
]

const handleRadioChange=(e)=>{
    console.log(e.target)
    setRadioValue(e.target.value)
  }
  const radioAgreeHandler=(e)=>{
     //  console.log(e.target)
     console.log('hey')

      // setIsAgree(true)
      setIsAgree(!isAgree)

  }
  const changeAgreeHandler=()=>{
      setIsAgree(!isAgree)
  }

  if(props.inputType==='stayingWithRadioButton'){

    return(
      <div className={`container-fluid ${Style.inputRadioStyle}`}>

      <FormLabel error={props.error} className={Style.labeTitle}>{props.title}</FormLabel>

       <RadioGroup  name={props.name}  value={props.radioValue} onChange={props.RadioChangeHandler}
                        className={`${Style.salaryradioBtn}`}>
          <div className={Style.salaryinnerradioBtn}>
          <FormControlLabel value={props.value1} control={<Radio color="primary" />} label={props.label1} />
          <FormControlLabel value={props.value2} control={<Radio  color="primary"/>} label={props.label2} />
          <FormControlLabel value={props.value3} control={<Radio  color="primary"/>} label={props.label3} />
          </div>

  </RadioGroup>
 <span style={{color:'red'}}>{props.error}</span>
</div>

    
    )

}else if(props.inputType==='textareaField'){
console.log('texErrorFlag',props.texErrorFlag,props.iserror)
 return(
  <div className={`container-fluid ${Style.inputTextareaStyle}`}>
    <span className={Style.textareaLabel}>Address</span>
       <textarea rows={4} 
        className={props.iserror?Style.errorTextarea:Style.successTextarea}        
        name={props.name} 
        autoComplete='off'
        onChange={props.onChangeHandler} 
          // error={props.iserror}
          required={props.required}
          value={props.value}
          onFocus={props.onFocus}
          onBlur={props.onBlur}
          onKeyDown={props.onKeyDown}
          onClick={props.onClick}
          disabled={props.disabled}
       
       />

{props.texErrorFlag?<span className={Style.errorText}>{props.errorText}</span>:<span className={Style.errorText}></span>}


    </div>
 )

}  else if(props.inputType==='genderRadioButton'){

        return(
            <div className={`container-fluid ${Style.inputRadioStyle}`}>
            <FormLabel error={props.error} className={Style.labeTitle}>{props.title}</FormLabel>
        <RadioGroup  name={props.name}  value={props.radioValue} onChange={props.RadioChangeHandler}
         className={`${Style.radioBtn}`}>

         <div className={Style.innerradioBtn}>
         <FormControlLabel value={props.value1} control={<Radio color="primary" />} label={props.label1} />
          <FormControlLabel value={props.value2} control={<Radio  color="primary"/>} label={props.label2} />
         </div>
          
        </RadioGroup>
               <span style={{color:'red'}}>{props.error}</span>
            </div>
        
        )

    }else if(props.inputType==='salaryReceviedAs'){
        return(
            <div className={`container-fluid ${Style.inputRadioStyle}`}>

                    <FormLabel error={props.error} className={Style.labeTitle}>{props.title}</FormLabel>

                <RadioGroup  name={props.name}  value={props.radioValue} onChange={props.RadioChangeHandler}
                                      className={`${Style.salaryradioBtn}`}>
                    <div className={Style.salaryinnerradioBtn}>
                    <FormControlLabel value={props.value1} control={<Radio color="primary" />} label={props.label1} />
                    <FormControlLabel value={props.value2} control={<Radio  color="primary"/>} label={props.label2} />
                    <FormControlLabel value={props.value3} control={<Radio  color="primary"/>} label={props.label3} />
                    </div>
          
                </RadioGroup>
               <span style={{color:'red'}}>{props.error}</span>
            </div>
        
        )

    }else if(props.inputType==='employmentStatusRadioButton'){
      return(
          <div className={` container ${Style.defaultinputRadioStyle}`}>
      <FormControl component="fieldset" error={props.error} className={classes.formControl}>

                  <FormLabel error={props.error} className={Style.labeTitle}>{props.title}</FormLabel>

              <RadioGroup  name={props.name}  value={props.radioValue} onChange={props.RadioChangeHandler}
                                    className={`${Style.defaultRadioBtn}`}>

                  <FormControlLabel value={props.value1} control={<Radio color="primary" />} label={props.label1} />
                  <FormControlLabel value={props.value2} control={<Radio  color="primary"/>} label={props.label2} />
                  <FormControlLabel value={props.value3} control={<Radio  color="primary"/>} label={props.label3} />
        
              </RadioGroup>
          </FormControl>

             {/* <span style={{color:'red'}}>{props.error}</span> */}
          </div>
      
      )

  }  else if(props.inputType==="agreeRadioButton"){
        return(

  <div className={`container ${Style.privacyagreeRadioElement}`}>

  <RadioGroup  name={props.name?props.name:'agree'}  
         value={props.agreeText?props.agreeText:isAgree} 
         onChange={props.onChange?props.onChange:changeAgreeHandler} 
        //  onClick={props.confirmHandler?props.confirmHandler:radioAgreeHandler}
        >
         <div className={Style.innerradioBtn}>
         <FormControlLabel 
          value={props.agreeText?props.agreeText:isAgree} 
          control={<Radio checked={props.checked?props.checked:isAgree}  
                          onClick={props.confirmHandler?props.confirmHandler:radioAgreeHandler}
                          color="primary"
                          
                          /> }

          label={props.agreeText}
          />
         </div>
          
  </RadioGroup>
   {/* <span className={Style.agreeText}>
      {props.agreeText}
   </span> */}

</div>
        )


    }else if(props.inputType==='selectType'){
         
       return( 
        <div className={`container-fluid ${Style.inputSelectStyle}`}>
            <Autocomplete
                // id="education"
                id="size-small-outlined"
                options={props.ArrayData?props.ArrayData:ArrayData}
                onChange={props.onChangeHandler} 
                name="education"
                getOptionLabel={(option) => option}
                
                value={props.value}
                onOpen={props.onClick}
                classes={{
                  option: classes.option
                }}
                style={{
                  width:props.width
                }}
                renderInput={(params) => (
                <TextField
                 {...params}
                 id="outlined-basic"
                 variant="outlined" 
                 label={props.label}
                 error={props.iserror}
                 placeholder={props.placeholder?props.placeholder:'Please Select'} 
                 fullWidth={true}
                 name="education"

                              
                 />
                )}
                
            />
         {props.iserror?<span className={Style.errorText}>{props.errorText}</span>:""}

         </div>

       )} else{

        return(
            <div className={`container-fluid ${Style.inputStyle}`}>
               <TextField
                // id="standard-basic" 
                id="outlined-basic"
                variant="outlined"
                label={props.label} 
                fullWidth={true}
                name={props.name} 
                autoComplete='off'
                onChange={props.onChangeHandler} 
                // helperText={props.helperText}

                style={{ width: props.width }}
                InputLabelProps={{
                    classes: {
                      root: classes.label,
                      focused: classes.focusedLabel,
                      error: classes.erroredLabel

                    
                    }
                  }}

                  InputProps={{
                    classes: {
                    root: classes.cssOutlinedInput,
                    // error: classes.outlineerror,
                    notchedOutline:classes.notchedOutline,
                    error:classes.outlineerror,
                    focused:classes.focusedError,
                    
                    }
                  }}
                  // className={Style.myStyle}
                  error={props.iserror}
                  required={props.required}
                  type={props.type}
                  name={props.name}
                  value={props.value}
                  onFocus={props.onFocus}
                  onBlur={props.onBlur}
                  focused={props.focused}
                  onKeyDown={props.onKeyDown}
                  onClick={props.onClick}
                  disabled={props.disabled}
                  />

                  {/* <span className={`${Style.errorStyle}`}>{props.error}</span> */}


                {props.iserror?<span className={Style.errorText}>{props.errorText}</span>:""}
            </div>
        )
    }
}


export default withStyles(styles)(DefaultInput)