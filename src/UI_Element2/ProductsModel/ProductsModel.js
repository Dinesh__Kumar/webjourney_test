import React, { Component } from 'react'
import DefaultModel from '../../UI_Element/Models/defaultModel/DefaultModel'
import Style from './productModel.module.css'
import Modal from 'react-bootstrap/Modal'
import cash62 from '../../#NewLogos/Home/Group 1324.png'
import cash90 from '../../#NewLogos/Home/Group 1325.png'
import cash180 from '../../#NewLogos/Home/Group 1326.png'
import {useHistory} from 'react-router-dom'
import { useSelector,useDispatch } from "react-redux";

import {productsModalHandler} from '../../Reduxstore2/Actions/DefaultControllerActions'
import {applyLoanHandler} from '../../Reduxstore2/Actions/LoansActions'

const  ProductsModel =(props)=> {
const history=useHistory()
    const dispatch=useDispatch();

    const clickHanlder=(loanType)=>{

        dispatch(applyLoanHandler(loanType,{},""))


        // if(loanType=='62'){
            
            history.push({
                pathname:`/SelectLoanAmount/${loanType}`,
                // search: `?loanType=${loanType}`
            })
        // }
        

        dispatch(productsModalHandler(false))


    }
        return (
            <div className={`${Style.main} `}>
                <Modal
              
                show={props.openProductModalFlag}
                onHide={props.closeModalHandler}
                centered
                scrollable={true}
                aria-labelledby="contained-modal-title-vcenter"

                
                >
                        <Modal.Header closeButton>
                           <Modal.Title id="contained-modal-title-vcenter" className={Style.headerTitle}>
                                    Loan Products
                            </Modal.Title>
                        </Modal.Header>

                        
                        <Modal.Body>
                                <div className={Style.ProductContainer}>
                                  <p className={Style.title}>Cashe 62</p>

                                        <div className={Style.content}> 
                                            
                                            <div className={Style.logo}>
                                              <img src={cash62}  alt="logo"/>
                                            </div>
    
                                             <div className={Style.LoanDescription}>

                                             <div className={Style.Loan_box1}>
                                                   <span>Duration</span>
                                                   <span>Eligibility</span>
                                                   <span>Interest</span>

                                                </div>

                                                <div className={Style.Loan_box2}>
                                                   <span>62 Days</span>
                                                   <span>Rs 34,000 </span>
                                                   <span>2.75%</span>
                                                </div>
                                             </div>
                                        </div>

                                        <button onClick={()=>clickHanlder('Cashe62')}>Applynow</button>

                                </div>


                                <hr/>

                                {/* -------------------------------------------------------------- */}

                                <div className={Style.ProductContainer}>
                                <p className={Style.title}>Cashe 90</p>

                                <div className={Style.content}> 
                                            
                                            <div className={Style.logo}>
                                              <img src={cash90}  alt="logo"/>
                                            </div>
    
                                             <div className={Style.LoanDescription}>

                                             <div className={Style.Loan_box1}>
                                                   <span>Duration</span>
                                                   <span>Eligibility</span>
                                                   <span>Interest</span>

                                                </div>

                                                <div className={Style.Loan_box2}>
                                                   <span>90 Days</span>
                                                   <span>Rs 34,000 </span>
                                                   <span>2.75%</span>
                                                </div>


                                             </div>
                                        
                                        
                                        </div>
                                        <button onClick={()=>clickHanlder('Cashe90')}>Applynow</button>
                                </div>

                                <hr/>

                                {/* --------------------------------------- */}

                                <div className={Style.ProductContainer}>
                                <p className={Style.title}>Cashe 180</p>

                                <div className={Style.content}> 
                                            
                                            <div className={Style.logo}>
                                              <img src={cash180}  alt="logo"/>
                                            </div>
    
                                             <div className={Style.LoanDescription}>

                                             <div className={Style.Loan_box1}>
                                                   <span>Duration</span>
                                                   <span>Eligibility</span>
                                                   <span>Interest</span>

                                                </div>

                                                <div className={Style.Loan_box2}>
                                                   <span>180 Days</span>
                                                   <span>Rs 34,000 </span>
                                                   <span>2.75%</span>
                                                </div>


                                             </div>
                                        
                                        
                                        </div>
                                        <button onClick={()=>clickHanlder('Cashe180')}>Applynow</button>
                                </div>
                                <hr/>
                            </Modal.Body>
                </Modal>
            </div>
        )
    }

    export default ProductsModel
