import React, { Component ,useCallback,useEffect,useRef, useState} from 'react'
import Webcam from "react-webcam";
import Style from './style.module.css';
import { useDispatch,useSelector } from 'react-redux';
import { useHistory} from 'react-router-dom';
import {  captureImage,cameraOpenHandler} from '../../reduxstore/Actions/cameraActions';

const  Camera=(props)=> {
    const webcamRef = useRef(null);
    // const [openCamera,setOpenCamera]=useState(props.iscamOpen?props.iscamOpen:false)
    
    const [imageData,setImageData]=useState('')
    const  dispatch = useDispatch()
    const camStateData=useSelector(state=>state.CameraData)


const history=useHistory()

    const videoConstraints = {
        // width: 300,
        // height: 300,
        facingMode: "user"
      };

 
      const capture = useCallback(() => {
          const imageSrc = webcamRef.current.getScreenshot();
          setImageData(imageSrc)
          dispatch(captureImage(imageSrc))
                // props.camCloseHandler(false)
                // setOpenCamera(false)
                dispatch(cameraOpenHandler(false))
                history.push('/DocumentsUpload')

        },[webcamRef]);
     
       
useEffect(()=>{

    return ()=>{
        console.log('cam unmounted')
        // props.camCloseHandler(false)
        // setOpenCamera(false)
        dispatch(cameraOpenHandler(false))

    }
},[])

console.log(imageData)
        return (
            <div className={`container-fluid ${Style.camHead}`}>

            { camStateData.cameraOpen &&
            <div className={`container-fluid ${Style.caminnerHead}`}>

                  <Webcam
                    audio={false}
                    ref={webcamRef}
                    screenshotFormat="image/jpeg"
                    className={Style.cam}
                    // height={300}
                    
                    // width={300}
                    mirrored={false}
                    videoConstraints={videoConstraints}
                   />
                      </div>
            }

    <button onClick={capture} disabled={!camStateData.cameraOpen}  className={Style.buttonCapture}>Capture</button>

            </div>

            
        )
    }

    export default Camera