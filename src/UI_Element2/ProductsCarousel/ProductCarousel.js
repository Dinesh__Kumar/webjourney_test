import React, {useState ,useEffect} from 'react'
import Style from "./ProductCarousel.module.css";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ic_cashe_logo from '../../#cashe/ic_cashe_logo.png'
import {useHistory} from 'react-router-dom'
import {useDispatch} from 'react-redux'
import {applyLoanHandler} from '../../Reduxstore2/Actions/LoansActions'

import { Swiper, SwiperSlide } from 'swiper/react';
import '../GlobelCss/globel.css'
import 'swiper/swiper-bundle.css'
import SwiperCore, { EffectCube ,Autoplay,EffectCoverflow,Virtual,Navigation,Pagination} from 'swiper';

SwiperCore.use([Virtual,Navigation,Pagination]);






const  ProductCarousel=()=> {
        const history=useHistory()
        const dispatch = useDispatch()
    let [LoanTypes,setLoanTypes]=useState([
        
        {name:'62',title:'62 days',productType:'Cashe62'},
        {name:'90',title:'90 days',productType:'Cashe90'},
        {name:'180',title:'180 days',productType:'Cashe180'},
        {name:'272',title:'272 days',productType:'Cashe272'},
        {name:'365',title:'365 days',productType:'Cashe365'},

            ])



 useEffect(() => {
 window.scrollTo(0,0)
}, [])
   



const setting={
    dots: false,
    infinite: false,
    // slidesToShow:2,
    // slidesToScroll:2
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 2,
      
  }


  const LoanHandler=(loanType)=>{
      console.log(loanType)
    dispatch(applyLoanHandler(loanType,{},""))

    history.push({
        pathname:`/SelectLoanAmount/${loanType}`,
        // search: `?loanType=${loanType}`
    })

  }
        return (

                <Swiper
                slidesPerView={3}
                spaceBetween={3}
                slidesPerGroup={3}
                className={Style.swiper}
                // loop={true}
                // loopFillGroupWithBlank={true}
                navigation
                pagination={{ clickable: true }}

                // navigation={{
                    
                //     nextEl: '.swiper-button-next',
                //     prevEl: '.swiper-button-prev',
                // }}
                // pagination={{
                //     el: '.swiper-pagination',
                //     clickable: true,
                // }}
                
                >

                            { LoanTypes.map((ele,index)=>{
                                            return(
                                                <SwiperSlide  className={Style.outerele}  key={ele.name} virtualIndex={index} >

                                                    <div className={Style.innerele} onClick={()=>LoanHandler(ele.productType)} >
                                                    <img src={ic_cashe_logo} />
                                                    <p className={Style.name} >{ele.name}</p>
                                                    </div>
                                                <p className={Style.title}>{ele.title}</p>

                                                </SwiperSlide>
                                                
                                                
                                                )
                                        })   
                            }

                </Swiper>
            
        
                // <Slider {...setting} arrows={false} className={Style.mainSlider}  
                // slidesToScroll={true} dots={false}  >
                            
                //             { LoanTypes.map((ele,index)=>{
                //                             return(
                //                                 <div  className={Style.outerele}  key={ele.name}>

                //                                     <div className={Style.innerele} onClick={()=>LoanHandler(ele.productType)} >
                //                                     <img src={ic_cashe_logo} />
                //                                  <p className={Style.name} >{ele.name}</p>
                //                                 </div>
                //                                 <p className={Style.title}>{ele.title}</p>

                //                                 </div>
                                                
                                                
                //                                 )
                //                         })   
                //             }
                //             </Slider>
        )
    
}


export default ProductCarousel