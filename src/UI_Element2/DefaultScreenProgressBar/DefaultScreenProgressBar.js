import React, { Component } from 'react'
import Style from './DefaultScreenProgressBar.module.css'

import {useSelector} from 'react-redux'
const DefaultScreenProgressBar =()=> {
    const FormStateData=useSelector(state=>state.FormData2)
    const CurrentScreenStateData=useSelector(state=>state.CurrentScreenData)
    console.log(CurrentScreenStateData)
    console.log(FormStateData)
    
        return (
            <div className={`${Style.main} container`}>
                <span className={ (CurrentScreenStateData.currentScreen.includes('CurrentAddress') || FormStateData.residentialAddress_IsValid)
                ?Style.successLable:Style.disableLable}></span>
                <span className={(CurrentScreenStateData.currentScreen.includes('PersonalDetails') || FormStateData.personalDetails_IsValid)?Style.successLable:Style.disableLable}></span>
                <span className={(CurrentScreenStateData.currentScreen.includes('EmploymentDetails') || FormStateData.employmentDetails_IsValid)?Style.successLable:Style.disableLable}></span>
                <span className={(CurrentScreenStateData.currentScreen.includes('BankDetails') || FormStateData.bankDetails_IsValid)?Style.successLable:Style.disableLable}></span>
                <span className={(CurrentScreenStateData.currentScreen.includes('PhotoProofs') || FormStateData.documnetDetails_IsValid)?Style.successLable:Style.disableLable}></span>

                
            </div>
        )
    
}

export default DefaultScreenProgressBar
