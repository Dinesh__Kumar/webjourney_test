import React, { useRef, useEffect, useState } from 'react'
import  Style from "./defautFrom.module.css";
import DefaultInput from "../defaultInput2/DefaultInput";
import { addBasicFromData,addDoucumnetData,delteDoucumnetData } from "../../Reduxstore2/Actions/Formactions";
import {useSelector,useDispatch } from "react-redux";

import {useHistory} from 'react-router-dom'
import RequiredModel from "../../UI_Element/RequiredModels/RequiredModel";
import DefaultModal from '../../UI_Element2/DefaultModal/DefaultModal'
// .modalType==='All_PDF_Image_View_Modal'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {blue} from '@material-ui/core/colors';
import ic_profile  from '../../#cashe/ic_profile.png'
import Camera from '../Camera/camera'
import {  cameraOpenHandler} from '../../reduxstore/Actions/cameraActions';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import ic_pdf from '../../#cashe/ic_pdf.png'
import ToggleButton from 'react-toggle'

import ToggleButton2 from '../ToggleBtn/ToggleBtn'

import "react-toggle/style.css" 


const  DefaultForm =(props)=>  {



const history=useHistory()
const dispacth=useDispatch()
const [currentElement,setCurrentElement]=useState('')
const [dateType,setDateType]=useState('text');
const [textError,setTextError]=useState(false)
// const [errorFlag,setErrorFlag]=useState(false)
const [toggleChecked,setToggleChecked]=useState(false)
const [onBlurError,setOnblurError]=useState(false)
const [onBlurElement,setOnBlurElement]=useState([])
const [textAreaError,settextAreaError]=useState(false)
const [docSide,setDocSide]=useState('')
const [iscamOpen,setisCamOpen]=useState(false)
const [openDefaultModal ,setOpenDefaultModal]=useState(false)
const [pdfData,setPdfData]=useState('')

const FormStateData=useSelector(state=>state.FormData2)

const camStateData=useSelector(state=>state.CameraData)


console.log(FormStateData)

console.log(FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length)
// console.log( !(FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length==2))
const fileRef1=useRef()
const fileRef2=useRef()

const Proof_fileRef1=useRef()
const Proof_fileRef2=useRef()
    const fileRef3=useRef()
 
    const camOpenHandler=(flag)=>{
        history.push(`/DocumentsUpload/UploadProfile/${123}`)
        dispacth(cameraOpenHandler(true))

    }

const openDefaultModalHandler=(contentType,data)=>{
    if(contentType==='PDF'){
        setPdfData(data)
    }
    setOpenDefaultModal(true)
}

const closeDefaultModalHandler=()=>{
    setOpenDefaultModal(false)
}

const toggleChangeHandler=(e)=>{
    console.log('dinesh',e.target.checked)

    setToggleChecked(!toggleChecked)
}
    const pickHandler=(value,eleType)=>{
        setDocSide(value)
        if(value=='Front' && eleType=='idProofDocumentType'){
            Proof_fileRef1.current.click()

        }else if(value==='Back' && eleType=='idProofDocumentType'){
            Proof_fileRef2.current.click()

        }else if(value=='Front' && eleType=='panCardDocumnet'){
            fileRef1.current.click()

        }else if(value==='Back' && eleType=='panCardDocumnet'){
            fileRef2.current.click()

        }else{
            fileRef3.current.click()
        }
     }

     const docChangeHandler=(e)=>{
             console.log(e.target.name)
                // let file={
                //     name: "FNT logo.png",
                //     size: 39643
                //     type: "image/png"
                // }
             dispacth(addDoucumnetData(e.target.name,e.target.files[0],docSide))
     }

     const docChangeHandler2=(e)=>{
        console.log(e.target.name)
          
        dispacth(addDoucumnetData(e.target.name,e.target.files[0],docSide))
}

    
     const removeDocHandler=(side,Elename,id)=>{
         let data=''
         console.log(side)

         if(id){
            dispacth(delteDoucumnetData(Elename,data,side,id))
         }else{
            dispacth(delteDoucumnetData(Elename,data,side))

         }
     }
    const onChangeHandler=(e)=>{
        // console.log(e.target.files[0])
        setTextError(false)
        dispacth(addBasicFromData(e.target.name,e.target.value))
        let newArr=onBlurElement.filter(ele=>ele!==(e.target.name))
        setOnBlurElement(newArr)
        setOnblurError(false)
        if(e.target.value.length>0){
            settextAreaError(false)

        }else{
            settextAreaError(true)

        }

    }

    useEffect(()=>{
        window.scrollTo(0,0)
    },[])


    const onChangeSelectHandler=(e,value)=>{
        // console.log(currentElement)
        console.log(value)
        if(value!==null){
            dispacth(addBasicFromData(currentElement,value))
        }else{
            dispacth(addBasicFromData(currentElement,""))
 
        }


    }

   
    const clickHandler=(ele,value)=>{
        // console.log('dinesh',ele)
        console.log("hey im clicked")
        setCurrentElement(ele)
        setTextError(true)
        
    }

    const DateFocusHandler=()=>{
        setDateType('date')
    }
    const onBlurHandler=(e)=>{
        setDateType('text')


        if(!FormStateData.formData[e.target.name].isValid){
            setOnBlurElement([...onBlurElement,e.target.name])
            setOnblurError(true)
        }else{
            setOnblurError(false)
 
        }
       
    }

    const textBlurHandler=(e)=>{
        console.log('blur error ')
        if(!FormStateData.formData[currentElement].isValid){
            setOnBlurElement([...onBlurElement,e.target.name])
            setOnblurError(true)
        }else{
            setOnblurError(false)
 
        }
    }
console.log(onBlurElement)
    const textAreaFocusHandler=(e)=>{
        console.log('hey i focus')
        // if(!FormStateData.formData[e.target.name].isValid){
            settextAreaError(true)

        // }

    }

  let myForm
  if(props.formType==='basicInfo_form1'){
   myForm=<form onSubmit={props.submitHandler} className={Style.formHead}>
                    <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            required={true}
                            onChangeHandler={onChangeHandler}
                            type="text"
                            name="name"
                            label="Name"
                            iserror={(!FormStateData.formData.name.isValid && currentElement==='name') ||
                                 onBlurElement.includes('name')
                              }
                            // errorInput={(FormStateData.formData.name.isValid)}

                            value={FormStateData.formData.name.name}
                            onClick={()=>clickHandler('name',FormStateData.formData.name.name)}
                            // helperText={!!(textError && currentElement==='name')?"As per the PAN Card":""}
                            helperText={!!(textError && currentElement==='name')?"Please Enter Your Name":""}
                            errorText="Please Enter Your Name As Per PAn"
                            onBlur={textBlurHandler}

                            />
                        </div>
                        

                        <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            inputType='genderRadioButton'
                            RadioChangeHandler={onChangeHandler}

                                radioValue={FormStateData.formData.gender.gender}
                                name="gender"
                                value1="Male"
                                value2="Female"
                                label1="Male"
                                label2="Female"
                                error={false}
                                title="Gender"
                              
                            />
                        </div>
                        <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            required={true}
                            onChangeHandler={onChangeHandler}
                            type="email"
                            name="email"
                            label="Email Address"
                            iserror={(!FormStateData.formData.email.isValid && currentElement==='email')||
                            onBlurElement.includes('email')
                        }

                            value={FormStateData.formData.email.email}
                            onClick={()=>clickHandler('email',FormStateData.formData.email.email)}
                            helperText={!!(textError && currentElement==='email')?"Please enter the email":""}
                            errorText="Please Enter Email"
                            onBlur={textBlurHandler}

                            
                            />
                        </div>


                       



                        <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            required={true}
                            onChangeHandler={onChangeHandler}
                            type={dateType}
                            name="DOB"
                            label="Date of Birth"
                            width={260}

                            iserror={(!FormStateData.formData.DOB.isValid && currentElement==='DOB')}
                            value={FormStateData.formData.DOB.DOB}
                            onClick={()=>clickHandler('DOB')}
                            onFocus={DateFocusHandler}
                            onBlur={onBlurHandler}

                            />
                        </div>

                        <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            inputType='selectType'
                            // required={true}
                            onChangeHandler={onChangeSelectHandler}
                            label="Education"
                            name="education"
                            width={260}
                            // label="Education"
                            ArrayData={props.educationData}
                            iserror={onBlurElement.includes('education')|| (!FormStateData.formData.education.isValid && currentElement==='education')}
                            value={FormStateData.formData.education.education}
                             onClick={()=>clickHandler('education')}
                             errorText="Select your company name"
                             onBlur={textBlurHandler}

                            />
                        </div>

                        <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            required={true}
                            onChangeHandler={onChangeHandler}
                            type="number"
                            name="pinCode"
                            width={260}
                            label="PinCode"
                            iserror={onBlurElement.includes('pinCode')|| (!FormStateData.formData.pinCode.isValid && currentElement==='pinCode')}
                            value={FormStateData.formData.pinCode.pinCode}
                            onClick={()=>clickHandler('pinCode')}
                            />
                        </div>

                        <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            required={true}
                            onChangeHandler={onChangeHandler}
                            type="city"
                            name="city"
                            label="City"
                            iserror={onBlurElement.includes('city')|| (!FormStateData.formData.city.isValid && currentElement==='city')}
                            value={FormStateData.formData.city.city}
                            onClick={()=>clickHandler('city')}
                            onBlur={textBlurHandler}

                            />
                        </div>
         </form>
  }else if(props.formType==='basicInfo_form2'){

    myForm=<form onSubmit={props.submitHandler} className={Style.formHead}>
    <div className={`${Style.DefaultInputStyle}`}>
            <DefaultInput
            // required={true}
            // onChangeHandler={onChangeHandler}
            // type="text"
            // name="employerName"
            // label="Employer Name"
            // iserror={onBlurElement.includes('employerName')|| (!FormStateData.formData.employerName.isValid && currentElement==='employerName')}
            // value={FormStateData.formData.employerName.employerName}
            // onClick={()=>clickHandler('employerName')}
            // helperText={!!(textError && currentElement==='employerName')?"Enter the employerName":""}
            // onBlur={textBlurHandler}

            inputType='selectType'
            // required={true}
             onChangeHandler={onChangeSelectHandler}
            // helperText
            // type="text"
            
            label="Employer Name"
            name="employerName"
            iserror={onBlurElement.includes('employerName')|| (!FormStateData.formData.employerName.isValid && currentElement==='employerName')}
            value={FormStateData.formData.employerName.employerName}
            onClick={()=>clickHandler('employerName')}
            onBlur={textBlurHandler}
            ArrayData={props.companyData}
            errorText="Please Enter Company name"

           />
        </div>
        

        
        <div className={`${Style.DefaultInputStyle}`}>
            <DefaultInput
            required={true}
            onChangeHandler={onChangeHandler}
            type={dateType}
            name="workingSince"
            label="Working Since"
            width={260}

            iserror={onBlurElement.includes('workingSince')|| (!FormStateData.formData.workingSince.isValid && currentElement==='workingSince')}
            value={FormStateData.formData.workingSince.workingSince}
            onClick={()=>clickHandler('workingSince')}
            onFocus={DateFocusHandler}
            onBlur={onBlurHandler}
            // onBlur={textBlurHandler}

            />
        </div>

     
        <div className={`${Style.DefaultInputStyle}`}>
            <DefaultInput
            inputType='selectType'
            // required={true}
             onChangeHandler={onChangeSelectHandler}
            // helperText
            // type="text"
            width={260}

            
              label="Designation"
              name="designation"
              iserror={onBlurElement.includes('designation')|| (!FormStateData.formData.designation.isValid && currentElement==='designation')}
             value={FormStateData.formData.designation.designation}
             onClick={()=>clickHandler('designation')}
             onBlur={textBlurHandler}

            />
        </div>





        <div className={`${Style.DefaultInputStyle}`}>
            <DefaultInput
                inputType='salaryReceviedAs'
                name="salaryReceviedAs"
                value1="Account Transfer"
                value2="Cheque"
                value3="Cash"
                label1="Account Transfer"
                label2="Cheque"
                label3="Cash"
                error={false}

                title="Salary Recevied as"
                radioValue={FormStateData.formData.salaryReceviedAs.salaryReceviedAs}
                RadioChangeHandler={onChangeHandler}
                // onClick={()=>clickHandler('salaryReceviedAs')}
            />
        </div>
    

        <div className={`${Style.DefaultInputStyle}`}>
            <DefaultInput
            required={true}
            onChangeHandler={onChangeHandler}
            type="number"
            name="netSalaryReceived"
            label="Net Salary Received"
            width={260}
            iserror={onBlurElement.includes('netSalaryReceived')|| (!FormStateData.formData.netSalaryReceived.isValid && currentElement==='netSalaryReceived')}
            value={FormStateData.formData.netSalaryReceived.netSalaryReceived}
            onClick={()=>clickHandler('netSalaryReceived')}
            helperText="Enter your monthly salary"
            helperText={!!(textError && currentElement==='netSalaryReceived')?"Enter your monthly salary":""}
            onBlur={textBlurHandler}

            />
        </div>

        
        <div className={`${Style.DefaultInputStyle}`}>
            <DefaultInput
                inputType='agreeRadioButton'
                agreeText=" I confirm and accept CASHe Product TERMS & condition
                                and Privacy Policy. i provided consent to use my
                              Aadhar number for teh purpose of eSigning loan agreement."

            />
        </div>


</form>
  }else if(props.formType==='Residential_address_form'){
      myForm=(
          <form>

                <div className={`${Style.DefaultInputStyle}`}>
                    <DefaultInput
                    inputType='stayingWithRadioButton'
                    RadioChangeHandler={onChangeHandler}

                        radioValue={FormStateData.formData.stayingWith.stayingWith}
                        name="stayingWith"
                        value1="Family"
                        value2="Friends"
                        value3="Alone"

                        label1="Family"
                        label2="Friends"
                        label3="Alone"

                        error={false}
                        title="Residence Type (Staying With)"
                              
                            />
                </div>



                <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            required={true}
                            onChangeHandler={onChangeHandler}
                            type={dateType}
                            name="livingSince" 
                            label="Living Since DD/MM/YYY "
                            width={260}
                            iserror={onBlurElement.includes('livingSince')||(!FormStateData.formData.livingSince.isValid && currentElement==='livingSince')}
                            value={FormStateData.formData.livingSince.livingSince}
                            onClick={()=>clickHandler('livingSince')}
                            onFocus={DateFocusHandler}
                            onBlur={onBlurHandler}
                            />
                        </div>


               <div className={`${Style.DefaultInputStyle}`}>
                    <DefaultInput
                        inputType='textareaField'
                        onChangeHandler={onChangeHandler}
                        name="residentialAddress"
                        value={FormStateData.formData.residentialAddress.residentialAddress}
                        onClick={()=>clickHandler('residentialAddress')}
                        onFocus={textAreaFocusHandler}
                         onBlur={textBlurHandler}
                        iserror={onBlurElement.includes('residentialAddress')|| (!FormStateData.formData.residentialAddress.isValid  && currentElement==='residentialAddress')}
                        texErrorFlag={onBlurElement.includes('residentialAddress')||(textAreaError &&!FormStateData.formData.residentialAddress.isValid)}
                        errorText="Please enter the Address"

                    />
              </div>

              <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            required={true}
                            onChangeHandler={onChangeHandler}
                            type='text'
                            name="pinCode" 
                            label="Pin Code"
                            width={240}

                            iserror={onBlurElement.includes('pinCode')||(!FormStateData.formData.pinCode.isValid && currentElement==='pinCode')}
                            value={FormStateData.formData.pinCode.pinCode}
                            onClick={()=>clickHandler('pinCode')}
                            errorText="Enter the Pin Code"
                             onBlur={textBlurHandler}
                            />
                </div>


                <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            required={true}
                            onChangeHandler={onChangeHandler}
                            type="text"
                            name="city"
                            label="City"
                            iserror={onBlurElement.includes('city')|| (!FormStateData.formData.city.isValid && currentElement==='city')}
                            value={FormStateData.formData.city.city}
                            onClick={()=>clickHandler('city')}
                            onBlur={textBlurHandler}
                            errorText="Enter the City"
                            />
                        </div>
          </form>
      )
    }else if(props.formType==='Profile_Details_Form'){
    return(
        <form>
            {/* maritalStatus */}
                <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            required={true}
                            onChangeHandler={onChangeHandler}
                            type="text"
                            name="name"
                            label="Name"

                            iserror={onBlurElement.includes('name')|| (!FormStateData.formData.name.isValid && currentElement==='name')}
                            value={FormStateData.formData.name.name}
                            onClick={()=>clickHandler('name')}
                            onBlur={textBlurHandler}
                            errorText="Enter Your Name As Per PAN"
                            />
                </div>   

                 <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                                inputType='genderRadioButton'
                                RadioChangeHandler={onChangeHandler}
                                radioValue={FormStateData.formData.gender.gender}
                                name="gender"
                                value1="Male"
                                value2="Female"
                                label1="Male"
                                label2="Female"
                                error={false}
                                title="Gender"
                              
                            />
                </div>   

                <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                            required={true}
                            onChangeHandler={onChangeHandler}
                            type="text"
                            name="PAN"
                            width={260}

                            label="PAN"
                            iserror={onBlurElement.includes('PAN')|| (!FormStateData.formData.PAN.isValid && currentElement==='PAN')}
                            value={FormStateData.formData.PAN.PAN.toLocaleUpperCase()}
                            onClick={()=>clickHandler('PAN')}
                            onBlur={textBlurHandler}
                            errorText="Enter Your Name As Per PAN"
                            />
                </div>  

                  <div className={`${Style.DefaultInputStyle}`}>
                            <DefaultInput
                                inputType='genderRadioButton'
                                RadioChangeHandler={onChangeHandler}
                                radioValue={FormStateData.formData.maritalStatus.maritalStatus}
                                name="maritalStatus"
                                value1="Married"
                                value2="Single"
                                label1="Married"
                                label2="Single"
                                error={false}
                                title="Marital Status"
                            />
                </div>  

                <div className={`${Style.DefaultInputStyle}`}>
                        <DefaultInput
                            inputType='selectType'
                            // required={true}
                            onChangeHandler={onChangeSelectHandler}    
                            label="Education"
                            name="education"
                            // label="Education"
                            iserror={onBlurElement.includes('education')|| (!FormStateData.formData.education.isValid && currentElement==='education')}
                            value={FormStateData.formData.education.education}
                             onClick={()=>clickHandler('education')}
                             errorText="Select Your Qualification"
                             onBlur={textBlurHandler}
                            />
                </div>
        </form>
    )
    
    } else if(props.formType==='Employment_Details_Form'){
return(
<form className={'form-group'}>
                
        <div className={`${Style.DefaultInputStyle} `}>
            <DefaultInput
                inputType='employmentStatusRadioButton'
                name="employmentStatus"
                value1="Salaried Full Time"
                value2="Self Employed"
                value3="UnEmployed"

                label1="Salaried Full Time"
                label2="Self Employed"
                label3="UnEmployed"

                error={false}
                title="Employment Status"
                radioValue={FormStateData.formData.employmentStatus.employmentStatus}
                RadioChangeHandler={onChangeHandler}
                // onClick={()=>clickHandler('salaryReceviedAs')}
            />
        </div>

        <div className={`${Style.DefaultInputStyle}`}>
            <DefaultInput
            required={true}
            onChangeHandler={onChangeHandler}
            type="text"
            name="employerName"
            label="Company Name"
            iserror={onBlurElement.includes('employerName')|| (!FormStateData.formData.employerName.isValid && currentElement==='employerName')}
            value={FormStateData.formData.employerName.employerName}
            onClick={()=>clickHandler('employerName')}
            helperText={!!(textError && currentElement==='employerName')?"Enter the employerName":""}
            onBlur={textBlurHandler}

           />
        </div>

        <div className={`${Style.DefaultInputStyle}`}>
            <DefaultInput
            required={true}
            onChangeHandler={onChangeHandler}
            type={dateType}
            name="workingSince"
            width={240}
            label="Working Since"
            iserror={onBlurElement.includes('workingSince')|| (!FormStateData.formData.workingSince.isValid && currentElement==='workingSince')}
            value={FormStateData.formData.workingSince.workingSince}
            onClick={()=>clickHandler('workingSince')}
            onFocus={DateFocusHandler}
            onBlur={onBlurHandler}
            onBlur={textBlurHandler}

            />
        </div>

        <div className={`${Style.DefaultInputStyle}`}>
            <DefaultInput
            required={true}
            onChangeHandler={onChangeHandler}
            type={'text'}
            width={240}
            name="officialEmailId"
            label="Official Email Id"
            iserror={onBlurElement.includes('officialEmailId')|| (!FormStateData.formData.officialEmailId.isValid && currentElement==='officialEmailId')}
            value={FormStateData.formData.officialEmailId.officialEmailId}
            onClick={()=>clickHandler('officialEmailId')}
            onBlur={textBlurHandler}

            />
        </div>

        <div className={`${Style.DefaultInputStyle}`}>
            <DefaultInput
                inputType='salaryReceviedAs'
                name="salaryReceviedAs"
                value1="Account Transfer"
                value2="Cheque"
                value3="Cash"
                label1="Account Transfer"
                label2="Cheque"
                label3="Cash"
                error={false}
                title="Mode of Salary Received"
                radioValue={FormStateData.formData.salaryReceviedAs.salaryReceviedAs}
                RadioChangeHandler={onChangeHandler}
                // onClick={()=>clickHandler('salaryReceviedAs')}
            />
        </div>



      


          </form>
        )
    } 
    else if(props.formType==='Bank_Details_Form'){
     return(
         <form>
  

            <div className={`${Style.DefaultInputStyle}`}>
                        <DefaultInput
                            inputType='selectType'
                            // required={true}
                            onChangeHandler={onChangeSelectHandler}    
                            label="Bank Name"
                            name="bankName"
                            ArrayData={props.bankData}
                            // label="Education"
                            iserror={onBlurElement.includes('bankName')|| (!FormStateData.formData.bankName.isValid && currentElement==='bankName')}
                            value={FormStateData.formData.bankName.bankName}
                             onClick={()=>clickHandler('bankName')}
                             errorText="Select Your Bank"
                             onBlur={textBlurHandler}
                            />

                </div>

            <div className={`${Style.DefaultInputStyle}`}>
                <DefaultInput
                required={true}
                onChangeHandler={onChangeHandler}
                type={'text'}
                name="accountNumber"
                label="Account Number"
                iserror={onBlurElement.includes('accountNumber')|| (!FormStateData.formData.accountNumber.isValid && currentElement==='accountNumber')}
                value={FormStateData.formData.accountNumber.accountNumber}
                onClick={()=>clickHandler('accountNumber')}
                onBlur={textBlurHandler}
                />
            </div>

            <div className={`${Style.DefaultInputStyle}`}>
                <DefaultInput
                required={true}
                onChangeHandler={onChangeHandler}
                type={'text'}
                name="retypeAccountNumber"
                label="Re-Type Account Number"
                iserror={onBlurElement.includes('retypeAccountNumber')|| (!FormStateData.formData.retypeAccountNumber.isValid && currentElement==='retypeAccountNumber')}
                value={FormStateData.formData.retypeAccountNumber.retypeAccountNumber}
                onClick={()=>clickHandler('retypeAccountNumber')}
                onBlur={textBlurHandler}
                />
            </div>

            <div className={`${Style.DefaultInputStyle}`}>
                <DefaultInput
                required={true}
                onChangeHandler={onChangeHandler}
                type={'text'}
                name="IFSC_code"
                width={200}

                label="IFSC Code"
                iserror={onBlurElement.includes('IFSC_code')|| (!FormStateData.formData.IFSC_code.isValid && currentElement==='IFSC_code')}
                value={FormStateData.formData.IFSC_code.IFSC_code}
                onClick={()=>clickHandler('IFSC_code')}
                onBlur={textBlurHandler}
                />

                <span className={Style.bankIfsc}>Search IFSC Code</span>
            </div>
            
         </form>
     )
    }
    else if(props.formType==='Required_Loan_Amount_Form'){
    return(

        <div className={`${Style.DefaultInputStyle}  ${Style.bankIfscHead}` }>
                <DefaultInput
                required={true}
                onChangeHandler={onChangeHandler}
                type={'text'}
                name="customerRequiredLoanAmount"
                label="Enter Loan Amount"
                iserror={onBlurElement.includes('customerRequiredLoanAmount')|| (!FormStateData.formData.customerRequiredLoanAmount.isValid && currentElement==='customerRequiredLoanAmount')}
                value={FormStateData.formData.customerRequiredLoanAmount.customerRequiredLoanAmount}
                onClick={()=>clickHandler('customerRequiredLoanAmount')}
                onBlur={textBlurHandler}
                />
            </div>

    )
    
    
    }else if(props.formType==='documents_upload_form'){

        // if(camStateData.cameraOpen ){
        //     return (
        //         <Camera iscamOpen={iscamOpen} camOpenHandler={camOpenHandler} camCloseHandler={camCloseHandler}/>
        //     )
        // }else{
        return(
          
            <div className={`${Style.docUploadMain} container`}>


                {openDefaultModal&&
                    <DefaultModal
                    openModalFlag={openDefaultModal}
                    closeModalHandler={closeDefaultModalHandler}
                    modalType='All_PDF_Image_View_Modal'
                    pdfData={pdfData}
                    contentType='PDF'
                    />
                }

               <div className={`${Style.panCard} container`}>
                   <p className={Style.title}>PAN Card</p>

                   <div style={{display:'none'}}>
                    <input type="file" name="panCardDocumnet" accept=".png,.jpeg,.jpg" ref={fileRef1} onChange={docChangeHandler} onClick={(e)=>e.target.value=null}/>
                    <input type="file" name="panCardDocumnet" accept=".png,.jpeg,.jpg" ref={fileRef2} onChange={docChangeHandler} onClick={(e)=>e.target.value=null}/>
                   </div>

                   <div className={Style.uploadButtons}>
                       <div className={Style.ButtonHead}>
                          <button onClick={()=>pickHandler('Front','panCardDocumnet')}><AddCircleOutlineIcon style={{fontSize:20,color:blue[300]}}/>Front Side</button>
                          {/* {!!FormStateData.formData.panCardDocumnet.Front&& */}
                        <div className={Style.docName}>
                              {!!FormStateData.formData.panCardDocumnet.Front&&<span className={Style.deleIcon} onClick={()=>removeDocHandler('Front','panCardDocumnet')}>{'X'}</span>}
                             <span className={Style.docNameText}>{!!FormStateData.formData.panCardDocumnet.Front&&FormStateData.formData.panCardDocumnet.Front.name}</span>
                          </div>
                            {/* } */}
                       </div>
                       
                         
                       <div className={Style.ButtonHead}>
                       <button onClick={()=>pickHandler('Back','panCardDocumnet')}><AddCircleOutlineIcon style={{fontSize:20,color:blue[300]}}/>Back Side</button>
                       {/* {!!FormStateData.formData.panCardDocumnet.Back&& */}
                        <div className={Style.docName}>
                              {!!FormStateData.formData.panCardDocumnet.Back&&<span className={Style.deleIcon} onClick={()=>removeDocHandler('Back','panCardDocumnet')}>{'X'}</span>}
                             <span className={Style.docNameText}>{!!FormStateData.formData.panCardDocumnet.Back&&FormStateData.formData.panCardDocumnet.Back.name}</span>
                          </div>
                            {/* } */}
                        </div>
                   </div>

               </div>

               <div className={`${Style.idProof}`}>
                    <p className={`${Style.title} container`}>ID Proof <span style={{color:'#f58e36'}}>( Upload any 1 )</span></p>

                    <div className={`${Style.DefaultInputStyle}`}>
                        <DefaultInput
                            inputType='selectType'
                            // required={true}
                            onChangeHandler={onChangeSelectHandler}    
                            label="Select Document"
                            name="idProofDocumentType"
                            ArrayData={props.docData}
                            // label="Education"
                            iserror={onBlurElement.includes('idProofDocumentType')|| (!FormStateData.formData.idProofDocumentType.isDocTypeText && currentElement==='idProofDocumentType')}
                            value={FormStateData.formData.idProofDocumentType.idProofDocumentType}
                             onClick={()=>clickHandler('idProofDocumentType')}
                             errorText="Select Your Document Type"
                             onBlur={textBlurHandler}
                            />

                </div>

                    <div className={`${Style.title} container`}>
                        <p>{FormStateData.formData.idProofDocumentType.isDocTypeText&&FormStateData.formData.idProofDocumentType.idProofDocumentType}</p>
                    </div>



                    <div style={{display:'none'}}>
                    <input type="file" name="idProofDocumentType" accept=".png,.jpeg,.jpg" ref={Proof_fileRef1} onChange={docChangeHandler2} onClick={(e)=>e.target.value=null}/>
                    <input type="file" name="idProofDocumentType" accept=".png,.jpeg,.jpg" ref={Proof_fileRef2} onChange={docChangeHandler2} onClick={(e)=>e.target.value=null}/>
                   </div>
                <div className={`${Style.uploadButtons} container`}>
                       <div className={Style.ButtonHead}>
                          <button disabled={!FormStateData.formData.idProofDocumentType.isDocTypeText} onClick={()=>pickHandler('Front','idProofDocumentType')}><AddCircleOutlineIcon style={{fontSize:20,color:blue[300]}}/>Front Side</button>
                        <div className={Style.docName}>
                              {!!FormStateData.formData.idProofDocumentType.Front&&<span className={Style.deleIcon} onClick={()=>removeDocHandler('Front','idProofDocumentType')}>{'X'}</span>}
                             <span className={Style.docNameText}>{!!FormStateData.formData.idProofDocumentType.Front&&FormStateData.formData.idProofDocumentType.Front.name}</span>
                          </div>
                       </div>
                       
                         
                       <div className={Style.ButtonHead}>
                       <button disabled={!FormStateData.formData.idProofDocumentType.isDocTypeText} onClick={()=>pickHandler('Back','idProofDocumentType')}><AddCircleOutlineIcon style={{fontSize:20,color:blue[300]}}/>Back Side</button>
                        <div className={Style.docName}>
                             {!!FormStateData.formData.idProofDocumentType.Back&& <span className={Style.deleIcon} onClick={()=>removeDocHandler('Back','idProofDocumentType')}>{'X'}</span>}
                             <span className={Style.docNameText}>{!!FormStateData.formData.idProofDocumentType.Back&&FormStateData.formData.idProofDocumentType.Back.name}</span>
                          </div>
                        </div>
                   </div>
               </div>
               {/* bankStatementDocument  fileRef3*/}

               <div className={`${Style.bankStatement} container`}>

               <p className={`${Style.title} `}>Bank Statement</p>
                    <div style={{display:'none'}}>

                        <input type="file" multiple={true} name="bankStatementDocument2" accept=".pdf,.doc" ref={fileRef3}
                        onChange={docChangeHandler} onClick={(e)=>e.target.value=null}/>
                    </div>

                    <div className={Style.bankStatement_toggleContainer}>
                    <p className={Style.bankStatement_toggleText}>Upload individual bank statement of 3 months</p>


                        <span className={Style.bankStatement_toggleBtn}>
                                <ToggleButton2 
                                ischecked={toggleChecked}
                                toggleChangeHandler={toggleChangeHandler}
                                
                                />
                        </span>

                    </div>


                    {!toggleChecked?
                    
                    <div className={Style.ButtonHead}>

                        <div className={Style.ButtonContainer}>
                          <button  disabled={  
                              !(FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length==0) &&
                              FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length>=0 
                              

                        
                                }
                                
                          onClick={()=>pickHandler('None')}><AddCircleOutlineIcon style={{fontSize:20,color:blue[300]}}/>Upload PDF</button>

                              {
                            (FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length>0 && 
                                FormStateData.formData.bankStatementDocument2.bankStatementDocument2[0]&&
                                FormStateData.formData.bankStatementDocument2.bankStatementDocument2[0].data!==null )&&
                              <div className={Style.pdfLogo}>
                                <img src={ic_pdf} width="40px" height="40px" onClick={()=>openDefaultModalHandler('PDF',FormStateData.formData.bankStatementDocument2.bankStatementDocument2[0].data)}/>
                                <span className={Style.deleIcon}
                                 onClick={()=>removeDocHandler('None','bankStatementDocument2', FormStateData.formData.bankStatementDocument2.bankStatementDocument2[0].id)}>X</span>

                             </div>
                              
                              }
                                   

                        </div>
                    

                          {/* {FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length>0&&
                            FormStateData.formData.bankStatementDocument2.bankStatementDocument2.map(ele=>{
                                return <div key={ele.id} className={Style.docName}>
                                                         <span className={Style.deleIcon} onClick={()=>removeDocHandler('None','bankStatementDocument2',ele.id)}>X</span>
                                                            <span className={Style.docNameText}>{ele.data.name}</span>
                                        </div>
                                    
                          })
                          
                          } */}

                          <div>

                          </div>
                     
                    </div>

                    :
                    <div className={Style.ButtonHead}>

                            <div className={Style.ButtonContainer}>
                                <button  disabled={ 
                                     !(FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length==0) ||
                                     (FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length==1 && FormStateData.formData.bankStatementDocument2.bankStatementDocument2[0].data==null)
                                    
                                    }
                                style={{margin:'5px 0px 5px 0px'}}
                                onClick={()=>pickHandler('None')}><AddCircleOutlineIcon style={{fontSize:20,color:blue[300]}}/>Month 1 PDF</button>

                                    {
                                    (FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length>0 && 
                                        FormStateData.formData.bankStatementDocument2.bankStatementDocument2[0]&&
                                        FormStateData.formData.bankStatementDocument2.bankStatementDocument2[0].data!==null )&&

                                    <div className={Style.pdfLogo}>
                                        <img src={ic_pdf} width="40px" height="40px"
                                        
                                        onClick={()=>openDefaultModalHandler('PDF',FormStateData.formData.bankStatementDocument2.bankStatementDocument2[0].data)}
                                        />
                                        <span className={Style.deleIcon}
                                        onClick={()=>removeDocHandler('None','bankStatementDocument2', FormStateData.formData.bankStatementDocument2.bankStatementDocument2[0].id)}>X</span>

                                    </div>
                                    
                                    }
                                        

                                </div>


                                {/* ------------------ */}

                                <div className={Style.ButtonContainer}>
                                <button 
                                 disabled={
                                    !(FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length==1)
                                           
                                
                                }
                                style={{margin:'5px 0px 5px 0px'}}
                                onClick={()=>pickHandler('None')}><AddCircleOutlineIcon style={{fontSize:20,color:blue[300]}}/>Month 2 PDF</button>

                                    {
                                    (FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length>0 && 
                                     FormStateData.formData.bankStatementDocument2.bankStatementDocument2[1]&&
                                     FormStateData.formData.bankStatementDocument2.bankStatementDocument2[1].data!==null )&&
                                    <div className={Style.pdfLogo}>
                                        <img src={ic_pdf} width="40px" height="40px"
                                        onClick={()=>openDefaultModalHandler('PDF',FormStateData.formData.bankStatementDocument2.bankStatementDocument2[1].data)}
                                        />
                                        <span className={Style.deleIcon}
                                        onClick={()=>removeDocHandler('None','bankStatementDocument2', FormStateData.formData.bankStatementDocument2.bankStatementDocument2[1].id)}>X</span>

                                    </div>
                                    
                                    }
                                        

                                </div>


                                 {/* ------------------ */}

                                 <div className={Style.ButtonContainer}>
                                <button     disabled={
                                    !(FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length==2)      
                                }
                                style={{margin:'5px 0px 5px 0px'}}
                                onClick={()=>pickHandler('None')}><AddCircleOutlineIcon style={{fontSize:20,color:blue[300]}}/>Month 3 PDF</button>

                                    {
                                   (FormStateData.formData.bankStatementDocument2.bankStatementDocument2.length>0 && 
                                    FormStateData.formData.bankStatementDocument2.bankStatementDocument2[2]&&
                                    FormStateData.formData.bankStatementDocument2.bankStatementDocument2[2].data!==null )&&
                                    <div className={Style.pdfLogo}>
                                        <img src={ic_pdf} width="40px" height="40px"
                                        onClick={()=>openDefaultModalHandler('PDF',FormStateData.formData.bankStatementDocument2.bankStatementDocument2[2].data)}
                                        />
                                        <span className={Style.deleIcon}
                                        onClick={()=>removeDocHandler('None','bankStatementDocument2', FormStateData.formData.bankStatementDocument2.bankStatementDocument2[2].id)}>X</span>

                                    </div>
                                    
                                    }
                                        

                                </div>

                    </div>

                   
                        
                    }

                
               </div>

               <div className={`${Style.profilePic} container`}>

                   <div className={Style.profilePicInnerHead}>
                        <p className={`${Style.title} `}>Profile Pic</p>

                        <div className={Style.profileButton}>
                        <button ><AddCircleOutlineIcon style={{fontSize:20,color:blue[300]}}/>Upload</button>

                        <button onClick={camOpenHandler}><AddCircleOutlineIcon style={{fontSize:20,color:blue[300]}} />Camera</button>

                        </div>

                   </div>

                   <div className={Style.avatarHead}>
                        <div className={Style.avatarImg}>

                            <img src={ camStateData.avatar?camStateData.avatar:ic_profile} />
                       </div>
                       {/* <span className={Style.avatarName}>
                           
                        </span> */}


                   </div>


               </div>


            </div>
        )

    }


    else{


      myForm="plz select form type"
  }



  console.log(FormStateData)

        return (
            <div className={Style.welcomeform}>

            <div className={`container-fluid  ${Style.DefaultModelStyle}`}>
                        {props.openDefaultModel &&
                                <RequiredModel 
                                modelType={props.modelType}
                                // onChange={ownerStatusChangeHandler}
                                // onClick={modelClosHandler}
                                submitSuccessHandler={props.submitSuccessHandler}
                                />
                            }
                            
            </div>
                {myForm}
            </div>
        )
    }
export default DefaultForm
