import React, { Component } from 'react'
import Style from "./default.module.css";
import {ArrowBackIos,ArrowForwardIos} from '@material-ui/icons';
import { grey } from "@material-ui/core/colors";
import ic_cashe from '../../#cashe/ic_cashe.png'
const DefaultBar =(props)=> {
        return (
            <div className={`${Style.main} container`}>
                {/* {props.children} */}

                       <button onClick={props.goBackHandler}> 
                           <ArrowBackIos  style={{color:grey[800],fontSize:18}} />
                            Back
                         </button>
                        <span>
                            <img src={ic_cashe} alt="logo" className={Style.Imglogo}/>
                        </span>
                        <button disabled={true} onClick={props.goNextHandler}>Next 
                           <ArrowForwardIos style={{color:grey[500] ,fontSize:18}}/>  
                         </button>
            </div>
        )
    
}

export default DefaultBar