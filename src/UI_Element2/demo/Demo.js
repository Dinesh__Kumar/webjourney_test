import React, { Component ,useCallback,useEffect,useRef, useState} from 'react'
import Webcam from "react-webcam";
import Style from './style.module.css';
import { useDispatch,useSelector } from 'react-redux';
import { useHistory} from 'react-router-dom';
// import {  captureImage,cameraOpenHandler} from '../../reduxstore/Actions/cameraActions';

const  Camera=(props)=> {
    const webcamRef = useRef(null);
    const [openCamera,setOpenCamera]=useState(false)
    
    const [imageData,setImageData]=useState('')
    // const  dispatch = useDispatch()
    // const camStateData=useSelector(state=>state.CameraData)


const history=useHistory()

    const videoConstraints = {
        width: 300,
        height: 300,
        facingMode: "user"
      };

 
      const capture = useCallback(() => {
          const imageSrc = webcamRef.current.getScreenshot();
          setImageData(imageSrc)
          setOpenCamera(false)

        },[webcamRef]);
     
       
useEffect(()=>{

//    navigator.mediaDevices.enumerateDevices().then(capture).catch(e=>{
//        console.log('error',e)
//    })

    return ()=>{
        console.log('cam unmounted')
        // props.camCloseHandler(false)
        setOpenCamera(false)
    }
},[])


const openCameraHandlder=()=>{
    setOpenCamera(true)
}

const closeCamera=()=>{
    setOpenCamera(false)
}

console.log(imageData)

        return (
            <div className={Style.camHead}>

                {openCamera&&
                        <div className={Style.camera}>

<input accept='image/*' id='icon-button-file' type='file' capture='environment'/>

                                {/* <button onClick={capture} className={Style.buttonCapture}>Capture</button> */}

                        </div>
                }
                        

                        <button onClick={openCameraHandlder}>Open Camera</button>
            
            </div>

            
        )
    }

    export default Camera