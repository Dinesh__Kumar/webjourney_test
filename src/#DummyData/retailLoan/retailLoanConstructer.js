class MerchantLoan{
    constructor(id,loanType,maxAmount,tenure,processingFee,interest,header,description,color,buttoncolor){
        this.id=id;
        this.loanType=loanType;
        this.maxAmount=maxAmount;
        this.tenure=tenure;
        this.processingFee=processingFee
        this.interest=interest
        this.header=header
        this.description=description
        this.color=color
        this.buttoncolor=buttoncolor

    }
}

export default MerchantLoan;