import React from 'react';
import DummyData from "../DummyCompanyDataModel";
import MerchantLoan from '../merchantLoan/merchantLoanConstructer';
import RetailLoan from '../retailLoan/retailLoanConstructer';


export const CompanyData=[
    new DummyData('1',"CASHe","Telangana","Hyderabad"),
    new DummyData('3',"Amazon","Telangana","Hyderabad"),
    new DummyData('4',"Google","Telangana","Hyderabad"),
    new DummyData('2',"OYO ROOMS" ,"Telngana","Hyderabad"),

    new DummyData('5',"TCS","Telangana","Hyderabad"),
    new DummyData('6',"WIPRO","Telangana","Hyderabad"),
    new DummyData('7',"BIGBASKET","Telangana","Hyderabad"),
    new DummyData('8',"CASHe","Telangana","Hyderabad"),
    new DummyData('9',"Amazon","Telangana","Hyderabad"),
    new DummyData('10',"Google","Telangana","Hyderabad"),
    new DummyData('11',"OYO ROOMS" ,"Telngana","Hyderabad"),

    new DummyData('12',"TCS","Telangana","Hyderabad"),
    new DummyData('13',"WIPRO","Telangana","Hyderabad"),
    new DummyData('14',"BIGBASKET","Telangana","Hyderabad"),
    new DummyData('15',"CASHe","Telangana","Hyderabad"),
    new DummyData('16',"Amazon","Telangana","Hyderabad"),
    new DummyData('17',"Google","Telangana","Hyderabad"),
    new DummyData('18',"OYO ROOMS" ,"Telngana","Hyderabad"),

    new DummyData('19',"TCS","Telangana","Hyderabad"),
    new DummyData('20',"WIPRO","Telangana","Hyderabad"),
    new DummyData('21',"BIGBASKET","Telangana","Hyderabad")

];

// export const MerchantLoanData=[
//     new MerchantLoan('1','Cashe 92','2','Rs.xxxx'),
//     new MerchantLoan('2','Cashe 180','3','Rs.xxxx'),
//     new MerchantLoan('3','Cashe 272','6','Rs.xxxx'),
//     new MerchantLoan('4','Cashe 365','12','Rs.xxxx'),

// ];



export const RetailLoanData=[
    new RetailLoan('1','62 DAYS',15000,'2','Rs.xxxx','2.75%','CASHe 62','*principle amount to be repaid in 2 EMIs**Levied per month Applicanle processing fee and interestrates will be deducted at source','#c8b3e8','#9c4ad4'),
    new RetailLoan('2','90 DAYS',15000,'3','Rs.xxxx','2.75%','CASHe 90','*principle amount to be repaid in 3 EMIs**Levied per month Applicanle processing fee and interestrates will be deducted at source','#b1f2d7','#32ad7a'),
    new RetailLoan('3','180 DAYS',20000,'6','Rs.xxxx','2.75%','CASHe 180','principle amount to be repaid in 6 EMIs**Levied per month Applicanle processing fee and interestrates will be deducted at source','#f9d2d2','#de3526'),
    new RetailLoan('4','270 DAYS',25000,'9','Rs.xxxx','2.75%','CASHe 270','principle amount to be repaid in 9 EMIs**Levied per month Applicanle processing fee and interestrates will be deducted at source','#7c8da3','#1f62a6'),
    new RetailLoan('5','365 DAYS',35000,'12','Rs.xxxx','2.75%','CASHe 1 Year','principle amount to be repaid in 12 EMIs**Levied per month Applicanle processing fee and interestrates will be deducted at source','#c29ff5','#9c4ad4'),

]