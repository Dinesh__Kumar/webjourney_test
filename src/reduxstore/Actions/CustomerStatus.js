export const NEW_CUSTOMER="NEW_CUSTOMER"

export const customerStatus=(status,policyFlag)=>{
    return{
        type:"CUSTOMER_STATUS",
        status,
        policyFlag
    }
}

export const personalDataProfile=(payload,flag)=>{

    return{
        type:'PERSONAL_PROFILE',
        payload,
        flag
    }

}

export const referencesHandler=(data,flag)=>{
    return{
        type:'REFERENCE_ADDED',
        data,
        flag
    }
}
