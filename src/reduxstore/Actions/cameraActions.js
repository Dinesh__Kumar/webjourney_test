export const IMAGE_CAPTURE="IMAGE_CAPTURE"


export const captureImage=(imgData)=>{
    return{
        type:IMAGE_CAPTURE,
        imgData
    }
}



export const cameraOpenHandler=(flag)=>{
    return{
        type:'CAMERA_OPEN_HANDLER',
        flag
    }
}