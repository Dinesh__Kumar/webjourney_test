export const NUMBER_CAPTURE_FOR_OTP="NUMBER_CAPTURE_FOR_OTP"
export const OTP_VERIFICATION_STATUS="OTP_VERIFICATION_STATUS"
export const NumberCaptureForOTP=(customer_number)=>{
    return{
        type:NUMBER_CAPTURE_FOR_OTP,
        payload:customer_number
    }
}

export const OTPverfication=(flag)=>{
    return{
        type:OTP_VERIFICATION_STATUS,
        payload:flag
    }
}