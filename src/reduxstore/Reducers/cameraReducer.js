
const initialState={
   avatar:'',
   cameraOpen:false
}


const Reducer=(oldstate=initialState,actions)=>{
    switch(actions.type){
        case 'IMAGE_CAPTURE':return{
            ...oldstate,
            avatar:actions.imgData
           
        }

        case 'CAMERA_OPEN_HANDLER':return{
            ...oldstate,
            cameraOpen:actions.flag

        }
       
        default:return oldstate
    }

}

export default Reducer