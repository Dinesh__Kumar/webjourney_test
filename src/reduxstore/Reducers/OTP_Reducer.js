import { NUMBER_CAPTURE_FOR_OTP } from '../Actions/OTP_actions';

const initialState={
    
       cusotmerPhone:'',
       OTP_verificationFlag:false,
       
}


const Reducer=(oldstate=initialState,actions)=>{
    switch(actions.type){
        case 'NUMBER_CAPTURE_FOR_OTP':
            return{
                ...oldstate,
                cusotmerPhone:actions.payload
        }
        case 'OTP_VERIFICATION_STATUS':
            return{
                ...oldstate,
                OTP_verificationFlag:actions.payload
            }
        default:return oldstate
    }
}

export default Reducer