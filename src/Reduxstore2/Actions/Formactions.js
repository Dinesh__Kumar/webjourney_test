export const addBasicFromData=(eleType,value)=>{
    return{
       type:'ADD_FORM_ELEMENT_VALUE',
       eleType,
       value
    }
}

export const addMobileNumber=(eleType,value)=>{
    return{
        type:'ADD_MOBILE_NUMBER',
        eleType,
        value
    }
}

export const addCustomerLoanAmount=(eleType,value)=>{
    return{
        type:'ADD_CUSTOMER_LOAN_AMOUNT',
        eleType,
        value
    }
}

export const addDoucumnetData=(eleType,value,side,flag,documentType)=>{
    return{
       type:'ADD_DOCUMENT_DATA',
       eleType,
       value,
       side,
       flag,
       documentType
    }
}



export const delteDoucumnetData=(eleType,value,side,id,flag)=>{
    return{
       type:'DELETE_DOCUMENT_DATA',
       eleType,
       value,
       side,
       id,
       flag
    }
}
