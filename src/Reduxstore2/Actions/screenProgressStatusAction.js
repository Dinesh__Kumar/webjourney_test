export const addCurrentScreen=(currentScreen)=>{
        return{
            type:'ADD_CURRENT_SCREEN',
            currentScreen,
        }
}


export const removeCurrentScreen=(currentScreen)=>{
    return{
        type:'REMOVE_CURRENT_SCREEN',
        currentScreen
    }
}