

export const  applyLoanHandler=(loanType,loanData,loanPurpose)=>{

    return{
        type:'APPLY_LOAN_START',
        loanType,
        loanData,
        loanPurpose
    }

}

export const setIsloanAppliedFlagHandler=(IOU_Flag,Loanflag)=>{
    return{
        type:'IS_LOAN_APPLIED',
        IOU_Flag,
        Loanflag,
    }
}