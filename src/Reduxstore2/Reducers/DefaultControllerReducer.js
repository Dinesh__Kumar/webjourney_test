let initialState={
   isProductModelOpen:false,
   isModalOpen:false
}

const Reducer=(oldstate=initialState,actions)=>{
    switch(actions.type){
     case 'PRODUCT_MODEL_OPEN_CLOSE_HANDLER':{
        return{
            ...oldstate,
            isProductModelOpen:actions.flag        }
     }
     case 'DEFAULT_MODEL_OPEN_CLOSE_HANDLER':
         return{
            ...oldstate,
            isModalOpen:actions.flag   
         }
     
     default :return oldstate
    }
}

export default Reducer