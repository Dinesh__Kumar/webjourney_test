

const initialState={
   formData:{
       //OTP screen

       mobileNumber:{
           mobileNumber:"+91 ",
           isValid:false
       },

       //-------Residential Address-----------
        stayingWith:{
            stayingWith:'',
            isValid:false,
        },
        livingSince:{
            livingSince:'',
            isValid:false
        },

        residentialAddress:{
            residentialAddress:'',
            isValid:false
        },

         //------Personal Details-------------

        PAN:{
            PAN:'',
            isValid:false
        },
        maritalStatus:{
            maritalStatus:'',
            isValid:false
        },
        //  ---------Employment Info--------------
        employmentStatus:{
            employmentStatus:'',
            isValid:false
        },
        officialEmailId:{
            officialEmailId:'',
            isValid:false
        },
        // -----------Bank Details----------------
             bankName:{
                 bankName:'',
                 isValid:false
             },

             accountNumber:{
                accountNumber:'',
                isValid:false
             },

             retypeAccountNumber:{
                retypeAccountNumber:'',
                isValid:false
             },

             IFSC_code:{
                 IFSC_code:'',
                 isValid:false
             },

        // ---------user loan amount-----------
               customerRequiredLoanAmount:{
                  customerRequiredLoanAmount:'',
                  isValid:false,
                  flag:false
               },
        //-----------Doucmnets Uploads-----------------
               panCardDocumnet:{
                Front:'',
                Back:'',
                isValid:false
               },

               idProofDocumentType:{
                idProofDocumentType:'',
                isDocTypeText:false,
                isValid:false,

                Front:'',
                Back:'',

               },

               bankStatementDocument:{
                bankStatementDocument:'',
                isValid:false

               },

               bankStatementDocument2:{
                bankStatementDocument2:[],
                statement1:[],
                statement2:[],
                statement3:[],
                isValid:false

               },
               profilePicDocument:{
                profilePicCocument:'',
                isValid:false
               },


        //--------------------------------

      name:{
          name:"",
          isValid:false
      },
      gender:{
        gender:"",
        isValid:false
    },
    email:{
        email:"",
        isValid:false
    },
    DOB:{
        DOB:"",
        isValid:false
    },

    education:{
        education:"",
        isValid:false
    },
    
    pinCode:{
        pinCode:"",
        isValid:false
    },
    city:{
        city:"",
        isValid:false
    },


        employerName:{
            employerName:"",
            isValid:false
        },
        workingSince:{
            workingSince:"",
            isValid:false
        },
        designation:{
            designation:"",
            isValid:false
        },
        salaryReceviedAs:{
            salaryReceviedAs:"",
            isValid:false
        },
        netSalaryReceived:{
            netSalaryReceived:"",
            isValid:false
        },
   },

   basicFormisValid:false,
   form1isValid:false,
   form2isValid:false,
   residentialAddress_IsValid:false,
   personalDetails_IsValid:false,
   employmentDetails_IsValid:false,
   bankDetails_IsValid:false,
   documnetDetails_IsValid:false
}

const Reducer=(oldstate=initialState,actions)=>{

    switch(actions.type){

        case 'ADD_DOCUMENT_DATA':
            if(actions.side!=='None'){
                oldstate.formData[actions.eleType][actions.side]=actions.value 

                if(actions.side!=='None' && !!oldstate.formData[actions.eleType].Front && !!oldstate.formData[actions.eleType].Back){
                    oldstate.formData[actions.eleType].isValid=true
                }else{
                    oldstate.formData[actions.eleType].isValid=false
                }
            }else{
                // oldstate.formData[actions.eleType][actions.eleType]=actions.value 
                // oldstate.formData[actions.eleType].isValid= !!oldstate.formData[actions.eleType][actions.eleType]?true:false


                // oldstate.formData[actions.eleType][actions.eleType].push(actions.value) 

                oldstate.formData[actions.eleType][actions.eleType].push({
                                                                        id:new Date().getTime(),
                                                                        data:actions.value  })

               
                // oldstate.formData[actions.eleType].isValid= !!oldstate.formData[actions.eleType][actions.eleType]?true:false

            }
           
            return{
                ...oldstate,

                documnetDetails_IsValid: oldstate.formData.panCardDocumnet.isValid
                                    && oldstate.formData.idProofDocumentType.isValid
                                    && oldstate.formData.bankStatementDocument.isValid

            }
            
        case 'DELETE_DOCUMENT_DATA':
            if(actions.side!=='None'){

            
            oldstate.formData[actions.eleType][actions.side]=actions.value 
            if(actions.side!=='None' && !!oldstate.formData[actions.eleType].Front && !!oldstate.formData[actions.eleType].Back){
                oldstate.formData[actions.eleType].isValid=true
            }else{
                oldstate.formData[actions.eleType].isValid=false
            }

        }else{
                // oldstate.formData[actions.eleType][actions.eleType]=actions.value 
                // oldstate.formData[actions.eleType].isValid= !!oldstate.formData[actions.eleType][actions.eleType]?true:false

                let dummyData={
                    id:new Date().getTime(),
                    data:null
                  }

                  let idx= oldstate.formData[actions.eleType][actions.eleType].findIndex(ele=>ele.id==actions.id)

                  oldstate.formData[actions.eleType][actions.eleType][idx]= dummyData

                // oldstate.formData[actions.eleType][actions.eleType]= oldstate.formData[actions.eleType][actions.eleType].filter(ele=>ele.id!==actions.id)
                oldstate.formData[actions.eleType].isValid= !!oldstate.formData[actions.eleType][actions.eleType]?true:false
 
        }
            return{
                ...oldstate,
                
                documnetDetails_IsValid: oldstate.formData.panCardDocumnet.isValid
                                    && oldstate.formData.idProofDocumentType.isValid
                                    && oldstate.formData.bankStatementDocument.isValid

            }
        case "ADD_FORM_ELEMENT_VALUE":
            if(actions.eleType==='retypeAccountNumber' || actions.eleType==='accountNumber'){
                const pattern=/^[0-9]+$/ 
                // console.log(oldstate.formData.mobileNumber.mobileNumber.slice(4).length)
                    if(pattern.test(actions.value)){
                        oldstate.formData[actions.eleType][actions.eleType]=actions.value 
                        oldstate.formData[actions.eleType].isValid=oldstate.formData[actions.eleType][actions.eleType].length>0?true:false
                
                    }    
            }else if(actions.eleType==='idProofDocumentType'){
                oldstate.formData[actions.eleType][actions.eleType]=actions.value 
                oldstate.formData[actions.eleType].isDocTypeText=oldstate.formData[actions.eleType][actions.eleType].length>0?true:false
            
            }else{  
        oldstate.formData[actions.eleType][actions.eleType]=actions.value 
        oldstate.formData[actions.eleType].isValid=oldstate.formData[actions.eleType][actions.eleType].length>0?true:false
            }
        return{
            ...oldstate,
            basicFormisValid:oldstate.formData.name.isValid
                           &&oldstate.formData.gender.isValid
                           &&oldstate.formData.email.isValid
                           &&oldstate.formData.DOB.isValid
                           &&oldstate.formData.education.isValid
                           &&oldstate.formData.pinCode.isValid
                           &&oldstate.formData.city.isValid
                           &&oldstate.formData.employerName.isValid
                           &&oldstate.formData.workingSince.isValid
                           &&oldstate.formData.salaryReceviedAs.isValid
                           &&oldstate.formData.netSalaryReceived.isValid
                           &&oldstate.formData.designation.isValid,

            form1isValid:oldstate.formData.name.isValid
                        &&oldstate.formData.gender.isValid
                        &&oldstate.formData.email.isValid
                        &&oldstate.formData.DOB.isValid
                        &&oldstate.formData.education.isValid
                        &&oldstate.formData.pinCode.isValid
                        &&oldstate.formData.city.isValid,

            form2isValid:oldstate.formData.employerName.isValid
                        &&oldstate.formData.workingSince.isValid
                        &&oldstate.formData.designation.isValid            
                        &&oldstate.formData.salaryReceviedAs.isValid
                        &&oldstate.formData.netSalaryReceived.isValid,

            residentialAddress_IsValid:oldstate.formData.livingSince.isValid
                                        &&oldstate.formData.stayingWith.isValid
                                        &&oldstate.formData.city.isValid            
                                        &&oldstate.formData.pinCode.isValid
                                        &&oldstate.formData.residentialAddress.isValid,
// ---------------------------------------------------------------------------------
            personalDetails_IsValid:oldstate.formData.name.isValid
                            &&oldstate.formData.PAN.isValid
                            &&oldstate.formData.gender.isValid            
                            &&oldstate.formData.maritalStatus.isValid
                            &&oldstate.formData.education.isValid,

//-----------------------------------------------------------------------------------------------------
            employmentDetails_IsValid:oldstate.formData.employmentStatus.isValid
                            &&oldstate.formData.employerName.isValid
                            &&oldstate.formData.workingSince.isValid            
                            &&oldstate.formData.officialEmailId.isValid
                            &&oldstate.formData.salaryReceviedAs.isValid,
// ----------------------------------------------------------------------------------


            bankDetails_IsValid:oldstate.formData.bankName.isValid
                            &&oldstate.formData.IFSC_code.isValid
                            &&oldstate.formData.accountNumber.isValid            
                            &&oldstate.formData.retypeAccountNumber.isValid
//---------------------------------------------------------------------------------------------                            

            }

        case "ADD_MOBILE_NUMBER":{
            const pattern=/^[0-9]+$/ 
            // console.log(oldstate.formData.mobileNumber.mobileNumber.slice(4).length)
        if(pattern.test(actions.value.slice(4)  || oldstate.formData.mobileNumber.mobileNumber.slice(4)  )   ){
            oldstate.formData[actions.eleType][actions.eleType]=actions.value 
        }
        oldstate.formData[actions.eleType].isValid=oldstate.formData[actions.eleType][actions.eleType].slice(4).length>9?true:false
            
            return{
               ...oldstate
            }
        }


        case "ADD_CUSTOMER_LOAN_AMOUNT":{
            oldstate.formData[actions.eleType][actions.eleType]=actions.value 
            oldstate.formData[actions.eleType].isValid=oldstate.formData[actions.eleType][actions.eleType].length>0?true:false

                return{
                    ...oldstate,
                }

        }

        default:return oldstate
    }
}

export default Reducer;