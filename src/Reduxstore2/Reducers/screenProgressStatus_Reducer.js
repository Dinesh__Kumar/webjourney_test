let initialState={
    currentScreen:[]
}

const Reducer=(oldstate=initialState,actions)=>{
    switch(actions.type){
     case 'ADD_CURRENT_SCREEN':{
        return{
            ...oldstate,
            currentScreen:[...oldstate.currentScreen,actions.currentScreen]
        }
     }

     case 'REMOVE_CURRENT_SCREEN':{
        return{
            ...oldstate,
            currentScreen:oldstate.currentScreen.filter(ele=>ele!==actions.currentScreen)
        }
     }
     default :return oldstate
    }
}

export default Reducer