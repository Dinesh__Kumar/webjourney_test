let initialState={

    loanData:{
        loanType:'',
        loanData:'',
        loanPurpose:'',
        IOUDeclaration:false,
        isLoanDataValid:false
    },
    isLoanAppliedSuccess:false

}


const Reducer=(oldstate=initialState,actions)=>{
    switch(actions.type){
        case 'APPLY_LOAN_START':
            console.log('diensh')
            let isLoanDataValidFlag=false
            if(!!oldstate.loanData.loanType && !!oldstate.loanData.IOUDeclaration && !!oldstate.loanData.loanData && !!oldstate.loanData.loanPurpose){
                isLoanDataValidFlag=true
            }else{
                isLoanDataValidFlag=false  
            }
            return{
                    ...oldstate,
                    loanData:{ 
                        loanType:actions.loanType,
                        loanData:actions.loanData,
                        IOUDeclaration:false,
                        loanPurpose:actions.loanPurpose,
                        isLoanDataValid:isLoanDataValidFlag
                    }
            }
            case "IS_LOAN_APPLIED":
                // if()
                oldstate.loanData.IOUDeclaration=actions.IOU_Flag
                oldstate.loanData.isLoanDataValid=actions.IOU_Flag
                oldstate.isLoanAppliedSuccess=actions.Loanflag
                return{
                    ...oldstate,
                    // isLoanAppliedSuccess:actions.Loanflag
                }


            default :return oldstate

    }
}

export default Reducer