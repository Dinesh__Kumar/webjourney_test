import React, { Component } from 'react'
import Style from "./Navigation.module.css";
import { NavLink  } from 'react-router-dom';
import { useSelector } from 'react-redux';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import EuroSymbolIcon from '@material-ui/icons/EuroSymbol';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import {orange} from '@material-ui/core/colors';
import ic_cashe from "../../#cashe/ic_cashe.png";
const  Navigation =(props)=> {

    const CustomerStateStatusData=useSelector(state=>state.CustomerStatusData)

    const clickHandler=(e)=>{
            console.log(e)
    }

        return (
            <div className={` ${Style.Navhead}`}>
           <NavLink to="/GetCash" activeClassName={Style.active} exact={true} onClick={clickHandler} id="n1">

           <span><img src={ic_cashe} width="24px"/> GetLoan </span>
           </NavLink>
          
           <NavLink to="/MerchantShop" exact={true} activeClassName={Style.active}  onClick={clickHandler} id="n2">

            <span>
                <ShoppingCartIcon style={{color:orange[500]}}/>
                Shop
                </span>

            </NavLink>

            <NavLink to="/Myprofile" exact={true} activeClassName={Style.active}  onClick={clickHandler} id="n3">

            <span>
                <AccountCircleIcon  style={{color:orange[500]}}/>
                My Profile
            </span>

            </NavLink>

            
            <NavLink to="/MyCASHe" exact={true}  activeClassName={Style.active}  onClick={clickHandler} id="n4">
            <span><FormatListBulletedIcon style={{color:orange[500]}}/> My CASHe</span>
            </NavLink>

            <NavLink to="/More" exact={true}  activeClassName={Style.active}  onClick={clickHandler} id="n5">
            <span><MoreHorizIcon/> More</span>
            </NavLink>
            {props.children}
                
            </div>
        )
    }
export default Navigation
