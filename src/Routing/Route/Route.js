import React, { Component } from 'react'
import {  Route,Switch} from 'react-router-dom';
import LoginPageComponent from '../../components/Login/Loginpage';
import PrivacyPolicyComponent from '../../components/privacyPolicyPage2/PrivacyPolicy';
import MyProfileHomeScreen from '../../component2/New ui/Myprofile/MyprofileHomeScreen/MyProfileHomeScreen';



import OTPverficationPage from '../../components/Welcomepage/OTPverification/OTPverificationPage/OTPverificationpage';
import { useSelector } from 'react-redux';

import CurrentAddressComponent from "../../component2/New ui/Myprofile/ResidentialDetails/ResidentialDetails";




// ------------------
import MobileNumberScreen from '../../component2/New ui/OTPComponent/MobileNumberScreen/MobileNumberScreen';
import BasicInfo from '../../component2/New ui/BasicInfo/BasicInfo'
import PersonalDetailsComponent from "../../component2/New ui/Myprofile/PersonalDetails/PersonalDetails";
import EmploymentsDetailsScreen from "../../component2/New ui/Myprofile/EmploymentsDetailsScreen/EmploymentsDetailsScreen";
import BankDetailsComponent from "../../component2/New ui/Myprofile/BankDetails/BankDetails";
import DocumentsUploadScreen from "../../component2/New ui/Myprofile/DocumentsDetailsScreen/DocumentsDetailsScreen"
import SelectLoanAmountScreen from "../../component2/New ui/LoanApplyComponents/SelectLoanAmountScreen/SelectLOanAmountScreen"
import LoanDetailsComponent from "../../component2/New ui/LoanApplyComponents/SelectedLoanDetailsScreen/SelectedLoanDetailsScreen";
import IOU_DeclarationComponent from "../../component2/New ui/LoanApplyComponents/IOUDeclarationScreen/IOUDeclarationScreen";
import MyLoanScreen from '../../component2/New ui/MyLoansScreen/MyLoanScreen'

import CameraScreen from '../../UI_Element2/Camera/camera'



const RouteComponent =()=>  {
    
    // const CustomerStateStatusData=useSelector(state=>state.CustomerStatusData)


    const route=(
        <Switch>
        <Route path='/' exact={true} component={LoginPageComponent}/>
        {/* <Route path='/welcomepage' exact={true} component={WelcomepageComponent}/> */}
        {/* <Route path='/welcomepage' exact={true} component={BasicInfo}/> */}

        <Route path='/BasicInfo' exact={true} component={BasicInfo}/>


        <Route path='/MobileNumber' exact={true} component={MobileNumberScreen}/>


        <Route path='/OTPverificationpage' exact={true} component={OTPverficationPage}/>
        {/* <Route path='/searchEmployer' exact={true} component={SearchComponent}/> */}
       <Route path='/Myprofile' exact={true} component={MyProfileHomeScreen}/>
        <Route path="/CurrentAddress" exact={true} component={CurrentAddressComponent}/>
        <Route path="/PersonalDetails" exact={true} component={PersonalDetailsComponent}/>
        {/* <Route path='/References' exact={true} component={ReferenceComponent}/> */}

        <Route path="/EmploymentDetails" exact={true} component={EmploymentsDetailsScreen}/>
        <Route path="/BankDetails" exact={true} component={BankDetailsComponent}/>
        {/* <Route path="/PhotoDetails" exact={true} component={PhotoDetailsComponent}/> */}
        <Route path="/DocumentsUpload" exact={true} component={DocumentsUploadScreen}/>

        <Route path="/DocumentsUpload/UploadProfile/:cid" exact={true} component={CameraScreen}/>

      

       

        <Route path="/IOU_Declaration" exact={true} component={IOU_DeclarationComponent}/>
        <Route path="/LoanDetails/:loanType" exact={true} component={LoanDetailsComponent}/>
        <Route path='/privacyPolicy' exact={true} component={PrivacyPolicyComponent}/>
        <Route path='/SelectLoanAmount/:loanType' exact={true} component={SelectLoanAmountScreen}/>
        <Route path="/MyLoans" exact={true} component={MyLoanScreen}/>
        
        
        
        
        
        
        
        
        
        
        
        </Switch>
    )
        return (
         <div>
            

             {route}
         </div>
               
        
        )
    
}


export default RouteComponent
