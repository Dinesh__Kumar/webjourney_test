import React, { Component, useState,useEffect } from 'react'
import Style from './OTPverificationpage.module.css'
import {useSelector,useDispatch} from 'react-redux'
import DefaultModel from '../../../../UI_Element/Models/defaultModel/DefaultModel';
import {OTPverfication } from '../../../../reduxstore/Actions/OTP_actions';
import Spinner from '../../../../UI_Element/Spinner/spinner';

import Backdrop2 from '../../../../UI_Element/backdrop/Backdrop2';
import EditIcon from '@material-ui/icons/Edit';
import otpvalidation from "../../../../#cashe/otpvalidation.png";
import OTP_page from '../../../../UI_Element/OTPpage/OTP_page';
import DefaultBar from '../../../../UI_Element2/defaultBar/DefaultBar'
import DefaultFooter from '../../../../UI_Element2/DefaultFooter/DefaultFooter'
const OTPverificationpage =(props)=>{
    const OTPStateData=useSelector(state=>state.OTPStateData)
    const FormStateData=useSelector(state=>state.FormData2)

     const dispatch = useDispatch()
//   console.log(OTPStateData)
  const [editFlag,setEditFlag]=useState(true)
  const [error,setError]=useState('')
  const [isNumberCorrect,setIsNumberCorrect]=useState(true)
  const [showModel,setShowModel]=useState(false)
  const [OTPverifyFlag,setOTPverifyFlag]=useState(false)

  const [phoneNumber,setPhoneNumber]=useState(FormStateData.formData.mobileNumber.mobileNumber)

  console.log(FormStateData.formData)
const onchageHandler=(e)=>{
    setPhoneNumber(e.target.value)
    setError('Please provide valid mobile number!')
}
  const ChangeEditFlagHandler=()=>{
      setEditFlag(false)
  }

  useEffect(() => {
   
    if(phoneNumber.length==10){
        setError('')
        setIsNumberCorrect(true)
    } else{
        setIsNumberCorrect(false)
    }
    }, [phoneNumber])
    


  const ValidateOTPHandler=()=>{
    setOTPverifyFlag(true)
      console.log('hey')
      setTimeout(() => {
          setOTPverifyFlag(false)
          setShowModel(true)
          dispatch(OTPverfication(true))
      }, 2000);
   
  }

  const ValidateOTPModelHandler=()=>{
      setShowModel(false)
      props.history.push('/BasicInfo')
  }
        return (
            <div className={` ${Style.container}`}>

              
                  <div className={Style.bar}>
                      <DefaultBar/>
                  </div>
                 

                {showModel&&
            <div  className={` container ${Style.DefaultModelStyle}`}>
            <DefaultModel>
                <div style={{padding:'10px',boxSizing:'border-box'}}>
                <div className={Style.imageBody}>
                   <img src={otpvalidation} width="100px"/>
                   </div>
                <p style={{fontWeight:'500'}}>Your mobile number verified successfully</p>

                </div>
                    <div className={`container-fluid ${Style.bottmBtn}`}>
                        <button   onClick={ValidateOTPModelHandler}>Ok</button>
                    </div>
           
            </DefaultModel>
            </div>}
                 
            
               <div className={`${Style.containerInner} container`}>

                    {OTPverifyFlag&&<div className={`${Style.backdrop}`}><Backdrop2/></div>}

                    {OTPverifyFlag&&<div className={`${Style.Spinner}`}><Spinner/></div>}
                                    
                            <div className={Style.title}>
                                  <h1>Enter OTP</h1>
                            </div>

                    <div className={Style.imageBody}>
                    <img src={otpvalidation} />
                    </div>
                
                    <div className={` ${Style.fromhead}`}>
                        <div className="form-group">
                            <label htmlFor="phone">We have sent OTP to the below number </label>
                            <div className={`${Style.inputform}`}>

                            <input type="text" className={`form-control ${Style.inputInner}`} placeholder="1234567890" 
                            value={phoneNumber} onChange={onchageHandler} disabled={editFlag}/>


                            <button onClick={ChangeEditFlagHandler} className={Style.editBtn}><EditIcon/></button>
                            </div>

                            <div>
                        <p style={{color:"red",fontSize:"15px",margin:"5px 0px"}}>{error}</p>
                             </div>   
                            
                        </div>
                    
                        <div className="form-group">
                            <label className={Style.enterOptText}>Enter OTP</label>

                            <div className={`${Style.inputform1}`}>

                                   <OTP_page ValidateOTPHandler={ValidateOTPHandler}/>
                            </div>
                        
                        <div className={Style.textFiled}>
                            <span>Didn't get otp? <span style={{color:'orange',cursor:'pointer'}}>RESEND</span></span>
                             

                             <span style={{color:'gray'}}>Resend OTP in 0.45</span>
                      
                        </div>
                        </div>

                    </div>
             


               </div>

               <div className={`${Style.footer} container`}>
                        <DefaultFooter disabled={true} 
                        value="submit"
                         width="100%"
                         backgroundColor="#ea7713"
                         
                         />
               </div>
           </div>
        )
    
}

export default OTPverificationpage