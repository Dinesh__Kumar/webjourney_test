import React, { Component, useState } from 'react'
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom'
import DefaultBar from '../../UI_Element2/defaultBar/DefaultBar'

import DefaultFooter from '../../UI_Element2/DefaultFooter/DefaultFooter'
import Style from './privacyPolicy.module.css';
import {customerStatus} from '../../reduxstore/Actions/CustomerStatus'
import Defaultinput from '../../UI_Element2/defaultInput2/DefaultInput';
const PrivacyPolicy =(props)=>{
    const dispatch=useDispatch();
    
    
 const [isAgree,setIsAgree]=useState(false)
 const privacyAcceptHandler=(e)=>{
    //  console.log(e.target)
     setIsAgree(true)
 }
 const changeprivacyAcceptHandler=()=>{
     setIsAgree(false)
 }
 const privacyAcceptProceedHandler=()=>{

    dispatch(customerStatus('new',true))
    
    // props.history.push('/CurrentAddress')
    props.history.push('/Myprofile')

 }
 
 console.log(isAgree)
        return (
            <div className={Style.privacyPageMainStyle}>

                        <div className={` ${Style.bar}`}>
                            <DefaultBar/>
                        </div>


                <div className="container">
                    <div className={`jumbotron ${Style.jumbotron}`}>

                        <p style={{textAlign:'start'}}>
                        A privacy policy is a statement or legal document that discloses
                         some or all of the ways a party gathers, uses, discloses, 
                         and manages a customer or client's data. A privacy policy is a statement or legal document that discloses
                         some or all of the ways a party gathers, uses, discloses, 
                         and manages a customer or client's data.
                        </p>
                   

                        <div className={`${Style.DefaultInputStyle}`}>
                            <Defaultinput  
                                inputType="agreeRadioButton"
                                agreeText=" i accept."
                                onChange={privacyAcceptHandler} 
                                confirmHandler={changeprivacyAcceptHandler} 
                                checked={isAgree}
                                value={isAgree}
                                />
                        </div>

                        
                </div>

                </div>

                <div className={`${Style.footer}`}>
                            <DefaultFooter
                             value="Proceed"
                             width="320px"
                             backgroundColor="#ea7713" 
                             disabled={isAgree}
                             onClick={privacyAcceptProceedHandler}
                             />
                        </div>
            </div>
        )
    
}


export default PrivacyPolicy