


import React, { Component, useState } from 'react'
import {NavLink} from 'react-router-dom'
import { facebookLoginHandler,googleLoginHandler } from '../../reduxstore/Actions/AuthActions';
import { useDispatch } from 'react-redux';
import Style from './Login.module.css';


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGoogle ,faFacebookF,faLinkedinIn} from '@fortawesome/free-brands-svg-icons';

// import casheLoginLogo from '../../#NewLogos/Login/casheLoginLogo.png';
import casheLoginLogo from '../../#NewLogos/Login/ic_cashelogo.png';
import LoginAssestLogo from '../../#NewLogos/Login/landing-page/pana.png';
import googleLogo from '../../#NewLogos/Login/google.png'
import FacebookLogo from '../../#NewLogos/Login/facebook.png'
// import FacebookLogin from 'react-facebook-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import GoogleLogin from 'react-google-login';
import Spinner from '../../UI_Element/Spinner/spinner';

const LoginPageComponent =(props)=> {

    const dispatch=useDispatch()
    const [openSpinner,setOpenSpinner]=useState(false)
    const [timer,setTimer]=useState(10000000*900000)

    const loginSubmitHandler=(responseData,type)=>{
        // console.log(responseData)

        if(type==='google'){
            console.log(responseData)

            setOpenSpinner(true)
            dispatch(googleLoginHandler(type,responseData.profileObj,responseData.tokenObj,true))
            setTimeout(() => {
                setOpenSpinner(false)
                setTimer(0)
                props.history.push('/MobileNumber')
            }, 2000);

        }else if(type==='facebook'){
            console.log(responseData,type)
            setOpenSpinner(true)

            const profileData={
                email:responseData.email,
                name:responseData.name,
                id:responseData.id,
                graphDomain:responseData.graphDomain,
                userID:responseData.userID,
                profilePicture:responseData.picture.data.url,
                
            }
            const tokenData={
                accessToken:responseData.accessToken,
                data_access_expiration_time:responseData.data_access_expiration_time,
                expiresIn:responseData.expiresIn,
                signedRequest:responseData.signedRequest

            }
               dispatch(facebookLoginHandler(type,profileData,tokenData,true))
              setTimeout(() => {
                setOpenSpinner(false)
                setTimer(0)

                 props.history.push('/MobileNumber')
              }, 2000);

        }

    }

    
const responseFacebook = (response) => {
    try {
        // console.log(response);
       loginSubmitHandler(response,'facebook')
    } catch (error) {
        console.log('error')

    }
  }
  
const responseGoogle = (response) => {
    try {
        // console.log(response);
        loginSubmitHandler(response,'google')

    } catch (error) {
        console.log('error')
    }
  }

  const onFailGoogle = (response) => {
    console.log(response);
  }
        return (
            <div className={`container-fluid ${Style.mainStyle1}`}>

                {
                openSpinner&&
                        <div className={Style.Spinner}>
                            <Spinner time={timer}/>
                        </div>
                    }
             
             

                <div  className={` ${Style.Logo}`}>

                        <img src={casheLoginLogo}/>
                        <span>The Social Loan Company</span>

                </div>


                <div  className={` ${Style.LoginAssest}`}>

                       <img src={LoginAssestLogo} alt="logo"/>
                </div>

               <div className={` ${Style.loginStyle}`}>

                   <div className={`${Style.loginbuttons}`}>
               
                   <div>
                   <GoogleLogin
                        clientId="17777237169-2uejc46e0pkkb9lrb9tv72eilksf5lih.apps.googleusercontent.com"
                        render={renderProps => (
                        <button onClick={renderProps.onClick} disabled={renderProps.disabled} className={Style.google}>
                           {/* <span><FontAwesomeIcon icon={faGoogle} size="2x" /></span> */}
                            <img src={googleLogo} width="26px" height="26px"/>
                            <span>SIGN IN WITH GOOGLE</span>
                            
                            </button>
                        )}
                        buttonText="Login"
                        onSuccess={responseGoogle}
                        onFailure={onFailGoogle}
                        cookiePolicy={'single_host_origin'}
                        // isSignedIn={false}
                        scope='email profile'
                    />
                   </div>

                

                        <div>
                    <FacebookLogin
                            appId="156515676159072"
                            autoLoad={false}
                            fields="name,email,picture"
                            callback={responseFacebook}
                            version="3.1"
                            render={renderProps => (
                                <button onClick={renderProps.onClick}  className={Style.facebook} >
                                    {/* <span><FontAwesomeIcon icon={faFacebookF } size='2x'/></span> */}
                                    
                                <img src={FacebookLogo} width="26px" height="26px"/>

                                <span>SIGN IN WITH FACEBOOK</span>
                                    </button>
                            )}
                        />
                        </div>

                   </div>


               </div>
          

               </div>
   
        )

}

export default LoginPageComponent



     






































// import React, { Component, useState } from 'react'
// import {NavLink} from 'react-router-dom'
// import { facebookLoginHandler,googleLoginHandler } from '../../reduxstore/Actions/AuthActions';
// import { useDispatch } from 'react-redux';
// import Style from './Login.module.css';
// import Defaultinput from '../../UI_Element/DefaultInput/Defaultinput';
// import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
// import {grey} from '@material-ui/core/colors'
// import MoreVertIcon from '@material-ui/icons/MoreVert';
// import LinkedInIcon from '@material-ui/icons/LinkedIn';
// import FacebookIcon from '@material-ui/icons/Facebook';
// import Backdrop from '../../UI_Element/backdrop/Backdrop2';

// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faGoogle ,faFacebookF,faLinkedinIn} from '@fortawesome/free-brands-svg-icons';

// import icon_cashelogo from '../../#cashe/icon_cashelogo.png';
// // import FacebookLogin from 'react-facebook-login';
// import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
// import GoogleLogin from 'react-google-login';
// import Spinner from '../../UI_Element/Spinner/spinner';

// const LoginPageComponent =(props)=> {

//     const dispatch=useDispatch()
//     const [openSpinner,setOpenSpinner]=useState(false)
//     const [timer,setTimer]=useState(10000000*900000)

//     const loginSubmitHandler=(responseData,type)=>{
//         // console.log(responseData)

//         if(type==='google'){
//             console.log(responseData)

//             setOpenSpinner(true)
//             dispatch(googleLoginHandler(type,responseData.profileObj,responseData.tokenObj,true))
//             setTimeout(() => {
//                 setOpenSpinner(false)
//                 setTimer(0)
//                 props.history.push('/MobileNumber')
//             }, 2000);

//         }else if(type==='facebook'){
//             console.log(responseData,type)
//             setOpenSpinner(true)

//             const profileData={
//                 email:responseData.email,
//                 name:responseData.name,
//                 id:responseData.id,
//                 graphDomain:responseData.graphDomain,
//                 userID:responseData.userID,
//                 profilePicture:responseData.picture.data.url,
                
//             }
//             const tokenData={
//                 accessToken:responseData.accessToken,
//                 data_access_expiration_time:responseData.data_access_expiration_time,
//                 expiresIn:responseData.expiresIn,
//                 signedRequest:responseData.signedRequest

//             }
//                dispatch(facebookLoginHandler(type,profileData,tokenData,true))
//               setTimeout(() => {
//                 setOpenSpinner(false)
//                 setTimer(0)

//                  props.history.push('/MobileNumber')
//               }, 2000);

//         }

//     }

    
// const responseFacebook = (response) => {
//     try {
//         // console.log(response);
//        loginSubmitHandler(response,'facebook')
//     } catch (error) {
//         console.log('error')

//     }
//   }
  
// const responseGoogle = (response) => {
//     try {
//         // console.log(response);
//         loginSubmitHandler(response,'google')

//     } catch (error) {
//         console.log('error')
//     }
//   }

//   const onFailGoogle = (response) => {
//     console.log(response);
//   }
//         return (
//             <div className={`container-fluid ${Style.mainStyle1}`}>

//                 {
//                 openSpinner&&
//                         <div className={Style.Spinner}>
//                             <Spinner time={timer}/>
//                         </div>
//                     }
//                  <div className={Style.backdrop}>
//                  <Backdrop/>
//                  </div>

//                 <div className={`${Style.header} `}>
//                     <span><KeyboardBackspaceIcon style={{color:grey[100]}} /></span>

//                     <span><MoreVertIcon style={{color:grey[100]}}/></span>
//                 </div>
               

//                <div  className={` ${Style.mainStyle2}`}>

   
//               <div className={Style.textheader}>


//                   <div className={Style.logoStyle}>

//                       <img src={icon_cashelogo}/>
                     
//                   </div>

//                      <div className={Style.para}>


//                     <p className={Style.para}>

//                     <span>It's quick,Easy & secure</span>
//                     <span>NO paper work, No phone call</span>
//                     </p>

//                     <p className={Style.para}>

//                         <span>...</span>
//                         </p>
                                        
                     
//                     <p className={Style.para}>
//                    <span>It's quick,Easy & secure</span>
//                     <span>NO paper work, No phone call</span>



//                    </p>

//                    </div>
//               </div>

//                <div className={` ${Style.loginStyle}`}>

//                    <div className={`${Style.loginbuttons}`}>

             

               
//                    <div>
//                    <GoogleLogin
//                         clientId="17777237169-2uejc46e0pkkb9lrb9tv72eilksf5lih.apps.googleusercontent.com"
//                         render={renderProps => (
//                         <button onClick={renderProps.onClick} disabled={renderProps.disabled} className={Style.google}>
//                            <span><FontAwesomeIcon icon={faGoogle} size="2x" /></span>
//                             <span>SIGN IN WITH GOOGLE</span>
                            
//                             </button>
//                         )}
//                         buttonText="Login"
//                         onSuccess={responseGoogle}
//                         onFailure={onFailGoogle}
//                         cookiePolicy={'single_host_origin'}
//                         // isSignedIn={false}
//                         scope='email profile'
//                     />
//                    </div>

                

//                         <div>
//                     <FacebookLogin
//                             appId="156515676159072"
//                             autoLoad={false}
//                             fields="name,email,picture"
//                             callback={responseFacebook}
//                             version="3.1"
//                             render={renderProps => (
//                                 <button onClick={renderProps.onClick}  className={Style.facebook} >
//                                     <span><FontAwesomeIcon icon={faFacebookF } size='2x'/></span>
//                                 <span>SIGN IN WITH FACEBOOK</span>
//                                     </button>
//                             )}
//                         />
//                         </div>

//                    </div>


//                          <div className={Style.radiobuttonStyle}>
//                            <Defaultinput  
//                                 inputType="agreeRadioButton"
//                                 agreeText="Yes, i have a referal code!"
//                                 />
//                        </div> 
//                </div>
          

//                </div>
   
//             </div>
//         )

// }

// export default LoginPageComponent



     