import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import {  Provider} from 'react-redux';
import {  createStore,applyMiddleware,compose,combineReducers} from 'redux';


import thunk from 'redux-thunk';
import  OTPReducer  from './reduxstore/Reducers/OTP_Reducer';
// import  CompanyDataReducer  from './reduxstore/Reducers/CompanyDataReducer';
import AuthReducer from './reduxstore/Reducers/AuthReducer';

import CameraReducer from './reduxstore/Reducers/cameraReducer';

import DefaultControllerReducer from './Reduxstore2/Reducers/DefaultControllerReducer'
// --------------------------------------------------
import FromReducer2 from "./Reduxstore2/Reducers/FromReducer";
import ScreenReducer from "./Reduxstore2/Reducers/screenProgressStatus_Reducer";

import LoanReducer from './Reduxstore2/Reducers/LoanReducers'
const rootReducer=combineReducers({
  OTPStateData:OTPReducer,
  AuthData:AuthReducer,
  CameraData:CameraReducer,
  FormData2:FromReducer2,
  CurrentScreenData:ScreenReducer,
  DefaultControllerData:DefaultControllerReducer,
  LoanData:LoanReducer
})



const composeEnhancers=window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store=createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)) )
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter >
    <App />
    </BrowserRouter>
    </Provider>
,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
