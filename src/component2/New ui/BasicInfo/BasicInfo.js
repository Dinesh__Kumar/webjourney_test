import Style  from './basicinfo.module.css'
import React, { Component, useState } from 'react'
import DefaultBar  from "../../../UI_Element2/defaultBar/DefaultBar";
import DefaultForm from "../../../UI_Element2/defaultForm2/defaultForm";
import {ArrowBackIos,ArrowForwardIos} from '@material-ui/icons';
import { grey } from "@material-ui/core/colors";
import ic_cashe from '../../../#cashe/ic_cashe.png'
import { useSelector } from "react-redux";
const BasicInfo =(props)=>  {
    const FormStateData=useSelector(state=>state.FormData2)
    const [openDefaultModel,setopenDefaultModel]=useState(false)

    const [viewpage1,setViewPage1]=useState(true)
    const [viewpage2,setViewPage2]=useState(false)

    const NextPageHandler=()=>{
        setViewPage2(true)
        setViewPage1(false)
    }

    const goBackHandler=()=>{
        setViewPage2(false)
        setViewPage1(true)
    }
    const onclickHandler=()=>{
        setopenDefaultModel(true)

    }

    const submitSuccessHandler=()=>{
        setopenDefaultModel(false)

        props.history.push('/privacyPolicy')
    }

    let companyData=[
        'OYO','Amazon','cognizent','TCS','Infosys','CGi','Google',
        'Microsoft','Wipro private limited','Wipro','Facebook','twitter',
        'Tata Consentency services','OLA','UBER','ZOMATO',''

                ]


    let educationData=[
        'Post Graduation','Bachelors Degree','Masters Degree'
    ]            
        return (
            <div className={` ${Style.main}`}>
                <div className={Style.bar}>
                    <DefaultBar goBackHandler={goBackHandler} />
                        
                </div>

                <div className={`${Style.maincontainer}`}>
                  
                        <div className={Style.BasicInfo}>
                            <span>Basic Info</span> 
                            <p className={Style.line_36}></p>
                        </div>
                        
                        <div className={Style.eclipseHead}>
                            <span className={(viewpage1 || FormStateData.form1isValid)?Style.enableEclipse:Style.disableEclipse}></span>
                            <span className={(viewpage2|| FormStateData.form2isValid)?Style.enableEclipse:Style.disableEclipse}></span>
                        </div>


                        <div className={Style.formContent}>
                            {viewpage1&&
                                <DefaultForm formType="basicInfo_form1"
                                educationData={educationData}
                                // openDefaultModel={openDefaultModel}
                                // modelType="welcomeform" 
 
                                />
                            }
                            {viewpage2&&
                               <DefaultForm formType="basicInfo_form2" 
                                modelType='welcomesuccess'
                                openDefaultModel={openDefaultModel} 
                                submitSuccessHandler={submitSuccessHandler} 
                                companyData={companyData}
                                
                                />
                            }
                        </div>

                        {viewpage1&&
                        <div className={Style.nextButton}>
                            <button onClick={NextPageHandler}>Next</button>
                        </div>}

                        {viewpage2&&
                        <div className={Style.confirmButton}>
                            <button onClick={onclickHandler}>Calculate My Eligibility</button>
                        </div>
                        
                        }
                </div>
            </div>
        )
    }
export default BasicInfo