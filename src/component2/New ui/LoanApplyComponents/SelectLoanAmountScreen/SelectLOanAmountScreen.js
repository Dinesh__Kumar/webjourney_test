import React, { Component, useEffect } from 'react'
import Cashe62 from './cashe62'
import Cashe90 from './cashe90'
import Cashe180 from './cashe180'
import {useSelector} from 'react-redux'
const SelectLoanAmountScreen =(props)=>  {
    const loanStateData=useSelector(state=>state.LoanData)
    console.log(loanStateData)

const redirectHandler=()=>{
    props.history.push('/Myprofile')
}


useEffect(()=>{
    if(!(!!loanStateData.loanData.loanType)){
        props.history.push('/Myprofile') 
    }
},[])
    if(loanStateData.loanData.loanType==='Cashe62'){
        return(
            <div>
               <Cashe62/>
            </div>
        )
    }else if(loanStateData.loanData.loanType==='Cashe90'){
        return (
            <div>
               <Cashe90/>
            </div>
        )
    }else if(loanStateData.loanData.loanType==='Cashe180'){
        return (
            <div>
               <Cashe180/>
            </div>
        )
    }else{

        return(
            <div>
                <button onClick={redirectHandler}>Home</button>
            </div>
        )
    }


}

export default SelectLoanAmountScreen
