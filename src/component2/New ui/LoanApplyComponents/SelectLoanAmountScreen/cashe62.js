import Style from './loanStyle.module.css'
import React, { useEffect, useState } from 'react'
import DefaultBar from '../../../../UI_Element2/defaultBar/DefaultBar'
import DefaultFooter from '../../../../UI_Element2/DefaultFooter/DefaultFooter'
import cash62 from '../../../../#NewLogos/Home/Group 1324.png'
import Slider from '../../../../UI_Element2/amountSlider/AmountSlider';
import NumberFormat from 'react-number-format';
import DefaultInput from '../../../../UI_Element2/defaultInput2/DefaultInput'
import {useSelector,useDispatch} from 'react-redux'

import {applyLoanHandler} from '../../../../Reduxstore2/Actions/LoansActions'
import {useHistory} from 'react-router-dom'
// import cash90 from '../../#NewLogos/Home/Group 1325.png'
// import cash180 from '../../#NewLogos/Home/Group 1326.png'


const Cashe62 =(props)=>  {

    const history=useHistory()
    useEffect(()=>{
        window.scrollTo(0,0)
        
    },[])

    const dispatch = useDispatch()
    const loanStateData=useSelector(state=>state.LoanData)

    const [amountValue,setAmountValue]=useState(30000)
    const [sliderValue,setsliderValue]=useState(0)

    const inputAmountChangeHandler=(e)=>{
        console.log(e.target.value)
        setAmountValue(e.target.value)
    }

    const onChangeCommitted=(a,b)=>{
        setAmountValue(parseInt(b*1000))

    }
    const onChangeSliderHandler=(e,newvalue)=>{
        setsliderValue(newvalue)
          setAmountValue(parseInt(newvalue*1000))
        
         
    }

    



 
    const onChangeSelectHandler=(e,value)=>{
        if(value!==null){
            dispatch(applyLoanHandler('Cashe62',{loanAmount:amountValue},value))
        }else{
            dispatch(applyLoanHandler('Cashe62',{loanAmount:amountValue},""))
 
        }

    } 
    const clickHandler=()=>{
        if(loanStateData.loanData.loanType!=='Cashe62'){
            history.push('/Myprofile')

        }else{
            history.push(`/LoanDetails/${loanStateData.loanData.loanType}`)

        }

        
    }

    
    const followersMarks = [

        {
          value: 0,
          scaledValue: 7000,
          label: "7k"
        },
  
        {
          value: 10,
          scaledValue: 1000,
          label: "10k"
        },
        {
          value: 20,
          scaledValue: 20000,
          label: "20k"
        },
        {
          value: 30,
          scaledValue: 30000,
          label: "30k"
        },
        // {
        //   value: 40,
        //   scaledValue: 40000,
        //   label: "40k"
        // },
        // {
        //   value: 50,
        //   scaledValue: 50000,
        //   label: "50k"
        // },
        // {
        //   value: 60,
        //   scaledValue: 60000,
        //   label: "60k"
        // },
        // {
        //   value: 125,
        //   scaledValue: 100000,
        //   label: "100k"
        // },
        // {
        //   value: 150,
        //   scaledValue: 250000,
        //   label: "250k"
        // },
        // {
        //   value: 175,
        //   scaledValue: 500000,
        //   label: "500k"
        // },
        // {
        //   value: 200,
        //   scaledValue: 1000000,
        //   label: "1M"
        // }
      ];
      useEffect(()=>{
  
      },[])
      const scale = (value,b) => {
        let label=value.toString()
        return label
      };
      
      function numFormatter(num) {
        if (num >= 1 && num <= 50000) {
            return num + "K"; // convert to K for number from > 1000 < 1 million
        } else if (num >= 1000000) {
          return (num / 1000000).toFixed(0) + "M"; // convert to M for number from > 1 million
        } else if (num < 900) {
          return num; // if value < 1000, nothing to do
        }
      }

   
 
        return (
            <div className={`${Style.main} container`}>

                <div className={Style.bar}>
                  <DefaultBar/>
                </div>

                <div className={Style.container}>

                    <p className={Style.title}>
                        Select Loan Amount
                    </p>

                    <div className={Style.logo}>
                        <img src={cash62} alt="cashe62"/>

                    </div>

                    <div className={Style.loanAmount}>

                        <NumberFormat 
                        className={Style.loanAmountInput}
                        thousandSeparator={true}
                        thousandsGroupStyle="lakh" 
                        prefix={'₹'}
                        value={amountValue}
                        onChange={inputAmountChangeHandler}
                        
                        />
                        

                        

                    </div>

                    <div className={Style.slider}>
                        <span>Amount in multiples of 1,000 only</span>

                        <div className={`${Style.slider_ele} `}>
                        <Slider
                            onChange={onChangeSliderHandler}
                            onChangeCommitted={onChangeCommitted}
                            value={sliderValue}
                            numFormatter={numFormatter}
                            followersMarks={followersMarks}
                            scale={scale}
                            min={7}
                            max={30}
                            defaultValue={30000}
                            />
                        </div>

                    </div>

                    <div className={Style.EMI_Details}>
                        <div className={Style.EMI_Details_textTitle}>
                            <span>No. of EMI’s</span>
                            <span>Max. Amount</span>
                            <span>Monthly Interest</span>
                        </div>
                        <span className={Style.dash}></span>
                        <div className={Style.EMI_Details_textAmount}>
                            <span>2</span>
                            <span>Rs 10000</span>
                            <span>2.75**%</span>


                        </div>

                    </div>

                    <div className={Style.purpose}>

                        <DefaultInput
                         inputType='selectType'
                         width="240px"
                         placeholder='Purpose'
                         ArrayData={['Wedding','Trip','Birthday Celebration','Festival']}
                         onChangeHandler={onChangeSelectHandler}
                        />

                    </div>


                    <div className={Style.EMI_Description}>
                        <p>
                        *Principal amount to be repaid in 3 EMIS **Levied per month.
                        Processing fee and interest charges will be collected upfront.
                        </p>

                    </div>

                

                </div>

                <div className={Style.footer}>
                  <DefaultFooter
                  value="Next"
                  onClick={clickHandler}
                disabled={!!loanStateData.loanData.loanPurpose}
                  />
                </div>
            </div>
        )
    }

    export default Cashe62
