import Style from './loanDetails.module.css'
import React, { useEffect, useState } from 'react'
import DefaultBar from '../../../../UI_Element2/defaultBar/DefaultBar'
import DefaultFooter from '../../../../UI_Element2/DefaultFooter/DefaultFooter'
// import cash62 from '../../../../#NewLogos/Home/Group 1324.png'
// import Slider from '../../../../UI_Element2/amountSlider/AmountSlider';
// import NumberFormat from 'react-number-format';
// import DefaultInput from '../../../../UI_Element2/defaultInput2/DefaultInput'
import {useSelector,useDispatch} from 'react-redux'

import {applyLoanHandler} from '../../../../Reduxstore2/Actions/LoansActions'
// import {useHistory} from 'react-router-dom'


const SelectedLoanDetailsScreen =(props)=> {

    useEffect(()=>{
        window.scrollTo(0,0);
        if(!(!!loanStateData.loanData.loanData && !!loanStateData.loanData.loanPurpose && !!loanStateData.loanData.loanType)){

            props.history.push('/Myprofile')
        }
        
        },[])


        const dispatch = useDispatch()
    const loanStateData=useSelector(state=>state.LoanData)

    const clickHandler=()=>{
        // loanType,
        // loanData,
        // loanPurpose
        let loanData={
            loanAmount:!!loanStateData.loanData.loanData&&loanStateData.loanData.loanData.loanAmount,
            In_HandAmount:10000,
            Interest_Upfront:200,
            Interest_part_of_EMI:300,
            Processing_fee:100,
            GST:60,
            Total_CASHe:10000,
            Number_of_EMIs:3,
            EMI_Amount:300



        }
        dispatch(applyLoanHandler(loanStateData.loanData.loanType,
            loanData,
            loanStateData.loanData.loanPurpose,
            ))
        props.history.push('/IOU_Declaration')
    }

        return (
            <div className={`${Style.main} container`}>

            <div className={Style.bar}>
              <DefaultBar/>
            </div>

            <div className={Style.container}>
                <p className={Style.title}>Loan Details</p>

                <p className={Style.loanDescriptiontext}>Here’s your loan summary on the account you have chosen</p>

                    <div className={Style.details}>

                        <div>
                            <span>In-Hand Amount</span>
                            <span className={Style.loanDetailsInAmount}>Rs 10,000</span>
                        </div>

                        <div>
                            <span>Interest Upfront (2.75%)</span>
                            <span className={Style.loanDetailsInAmount}>Rs 275</span>
                        </div>

                        <div>
                            <span>Interest part of EMI (2.75%)</span>
                            <span className={Style.loanDetailsInAmount}>Rs 3,308</span>
                        </div>

                        <div>
                            <span>Processing fee</span>
                            <span className={Style.loanDetailsInAmount}>Rs 100</span>
                        </div>

                        <div>
                            <span>GST (18%)</span>
                            <span className={Style.loanDetailsInAmount}>Rs 100</span>
                        </div>

                        <div>
                            <span>Total CASHe</span>
                            <span className={Style.loanDetailsInAmount} style={{color:'#f48120'}}>Rs 10,000</span>
                        </div>

                        <div>
                            <span>Number of EMI’s</span>
                            <span className={Style.loanDetailsInAmount} style={{color:'#2f73da'}}>3</span>
                        </div>

                        <div>
                            <span>EMI Amount</span>
                            <span className={Style.loanDetailsInAmount} style={{color:'#2f73da'}}>Rs 3,608</span>
                        </div>

                    </div>


                </div>


            <div className={Style.footer}>
              <DefaultFooter
              value="Next"
              onClick={clickHandler}
            disabled={true}
              />
            </div>
        </div>
        )
    }

    export default SelectedLoanDetailsScreen




// import cash90 from '../../#NewLogos/Home/Group 1325.png'
// import cash180 from '../../#NewLogos/Home/Group 1326.png'
