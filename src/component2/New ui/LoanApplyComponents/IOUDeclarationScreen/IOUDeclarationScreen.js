import Style from './IOU.module.css'
import React, { useEffect, useState } from 'react'
import DefaultBar from '../../../../UI_Element2/defaultBar/DefaultBar'
import DefaultFooter from '../../../../UI_Element2/DefaultFooter/DefaultFooter'
// import cash62 from '../../../../#NewLogos/Home/Group 1324.png'
// import Slider from '../../../../UI_Element2/amountSlider/AmountSlider';
// import NumberFormat from 'react-number-format';
// import DefaultInput from '../../../../UI_Element2/defaultInput2/DefaultInput'
import {useSelector,useDispatch} from 'react-redux'
import IOULogo from '../../../../#NewLogos/IOU/IOU.PNG'
import {setIsloanAppliedFlagHandler} from '../../../../Reduxstore2/Actions/LoansActions'
// import {useHistory} from 'react-router-dom'
import {defaultModalHandler} from '../../../../Reduxstore2/Actions/DefaultControllerActions'

// defaultModalHandler

const SelectedLoanDetailsScreen =(props)=> {
    const loanStateData=useSelector(state=>state.LoanData)
console.log(loanStateData)
    useEffect(()=>{
        window.scrollTo(0,0)
        // loanType,
        // loanData,
        // loanPurpose
        if(!(!!loanStateData.loanData.loanData && !!loanStateData.loanData.loanPurpose && !!loanStateData.loanData.loanType)){
            props.history.push('/Myprofile')
        }
        
        },[])


        const dispatch = useDispatch()

    const clickHandler=()=>{
        let IOUFLag=!!loanStateData.loanData.loanData && !!loanStateData.loanData.loanPurpose && !!loanStateData.loanData.loanType
        console.log('latha',IOUFLag)   
        dispatch(setIsloanAppliedFlagHandler(IOUFLag,true))
        dispatch(defaultModalHandler(true))
            props.history.push('/Myprofile')
    }

        return (
            <div className={`${Style.main} container`}>

            <div className={Style.bar}>
              <DefaultBar/>
            </div>

            <div className={Style.container}>
                <p className={Style.title}>IOU Decleration</p>

                <div className={Style.logo}>
                    <img src={IOULogo} alt="IOU"/>

                </div>


                <div className={Style.Declaration}>

                    <p>
                    I confirm that I have read, understood and agreed to the CASHe Product terms and Conditions and related Policies.
                     I grant my irrevocable consent to lender and/or any authorized party nominated by lender for making public, in case I commit wilful default.
                      I hereby promise to pay CASHe/ lender or order on demand Rs.<span>{"ROHIT"}</span>                         
                      together with interest and delayed interest, if any , as prescribed in the terms and condition of the Lender from due date until repayment thereof.
                    </p>

                </div>



                
                        


                </div>


            <div className={Style.footer}>
              <DefaultFooter
              value="Apply Now"
              backgroundColor="#f48120"
              width="240px"
              onClick={clickHandler}
            disabled={true}
              />
            </div>
        </div>
        )
    }

    export default SelectedLoanDetailsScreen




// import cash90 from '../../#NewLogos/Home/Group 1325.png'
// import cash180 from '../../#NewLogos/Home/Group 1326.png'
