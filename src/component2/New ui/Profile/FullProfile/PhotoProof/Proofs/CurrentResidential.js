import React, { Component,useState } from 'react'
import Navbar from '../../../../../UI_Element/Navbar/Navbar'
// import ic_profile from '../../../../../#cashe/ic_profile.png';
import ic_rightArrow from '../../../../../#cashe/ic_rightArrow.png';
import Style from './Proofs.module.css';

import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { NavLink } from 'react-router-dom';
import { grey } from '@material-ui/core/colors';
import Toggle from 'react-toggle'
import "react-toggle/style.css" 
import '../../../../../UI_Element/GlobalCss/globalStyle.css'
import DefaultProofELement from "../../../../../UI_Element/DefaultProofElement/DefaultProofELement";
import ic_residentialproof from '../../../../../#cashe/ic_residentialproof.png';


const CurrentResidential =()=>  {
        return (
            <div>

 
              <div>
            <Navbar>
                <div className={Style.navbarinnerleft} style={{width:"280px"}}>
                            <NavLink to={`/PhotoDetails`} exact={true}>
                            <span><KeyboardBackspaceIcon style={{color:grey[100]}}/></span>
                            </NavLink>
                            <span style={{fontSize:'20px'}}>Current Residential Proofs </span>
                </div>
            </Navbar>
            </div>


            {/* ----------------------- */}



            <div>

            <DefaultProofELement
                    lefticon={ic_residentialproof}
                    righticon={ic_rightArrow}
                    label="Aadhar card"
                    innerText="Front and back side photo"
                
                />


                
            <DefaultProofELement
                    lefticon={ic_residentialproof}
                    righticon={ic_rightArrow}
                    label="Utility Bill(Electricity/Piped Gas/Water)"
                    innerText="Bills not older than 3 months"
                
                />



                
            <DefaultProofELement
                    lefticon={ic_residentialproof}
                    righticon={ic_rightArrow}
                    label="Telephone Bill(Postpaid-Mobile/Land-line)"
                    innerText="Bills not older than 3 months"
                
                />



                
            <DefaultProofELement
                    lefticon={ic_residentialproof}
                    righticon={ic_rightArrow}
                    label="Passport"
                    innerText="Front and last page photo"
                
                />



                
            <DefaultProofELement
                    lefticon={ic_residentialproof}
                    righticon={ic_rightArrow}
                    label="Voter ID"
                    innerText="Front and back side photo"
                
                />

            <DefaultProofELement
                        lefticon={ic_residentialproof}
                        righticon={ic_rightArrow}
                        label="Driving License"
                        innerText="Front and back side photo"
                    
                    />


            <DefaultProofELement
                    lefticon={ic_residentialproof}
                    righticon={ic_rightArrow}
                    label="Rental Agreement"
                    innerText="Front and back side photo"
                
                />


            <DefaultProofELement
                    lefticon={ic_residentialproof}
                    righticon={ic_rightArrow}
                    label="Company HR Letter"
                   
                
                />

            <DefaultProofELement
                    lefticon={ic_residentialproof}
                    righticon={ic_rightArrow}
                    label="House Purches Agreement Letter"
                   
                
                />


            </div>

        </div>
        )
    }
export default CurrentResidential
