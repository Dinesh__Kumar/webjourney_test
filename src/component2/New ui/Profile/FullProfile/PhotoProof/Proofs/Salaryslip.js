import React, { Component,useState } from 'react'
import Navbar from '../../../../../UI_Element/Navbar/Navbar'
// import ic_profile from '../../../../../#cashe/ic_profile.png';
import ic_add from '../../../../../#cashe/ic_add.png';
import ic_pan from '../../../../../#cashe/ic_pan.png';
import Style from './Proofs.module.css';

import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { NavLink } from 'react-router-dom';
import { grey } from '@material-ui/core/colors';
import Toggle from 'react-toggle'
import "react-toggle/style.css" 
import '../../../../../UI_Element/GlobalCss/globalStyle.css'
import DefaultProofELement from "../../../../../UI_Element/DefaultProofElement/DefaultProofELement";
import ic_financialproof from '../../../../../#cashe/ic_financialproof.png';

const  Salaryslip =()=> {
    const [addressFalg,setAddressFalg]=useState(false)


    const handleToggleChange=(e)=>{
      console.log(e.target.checked)
      setAddressFalg(e.target.checked)
      
    }
            return (
                <div>
    
     
                  <div>
                <Navbar>
                    <div className={Style.navbarinnerleft} style={{width:"150px"}}>
                                <NavLink to={`/PhotoDetails`} exact={true}>
                                <span><KeyboardBackspaceIcon style={{color:grey[100]}}/></span>
                                </NavLink>
                                <span style={{fontSize:'20px'}}>Salary Slip</span>
                    </div>
                    <span>HELP</span>
                </Navbar>
                </div>
    
    
                {/* ----------------------- */}
                <div className={`container ${Style.toggleStyle}`}>  
    
                        <span className={Style.textStyle}>Upload individual salary for
                              the last 3 months
                         </span>
    
                            <Toggle
                            defaultChecked={false}
                            className='custom-classname'
                            onChange={handleToggleChange}
                            icons={false}
                            />
                </div>
    
    
                {/* .------------------------------------------------ */}
                        
    
                        
                {addressFalg?
                <div>
                <DefaultProofELement
                    lefticon={ic_financialproof}
                    righticon={ic_add}
                    label="First month"
                
                />

                       <DefaultProofELement
                            lefticon={ic_financialproof}
                            righticon={ic_add}
                            label="Second month"
                        
                        />

                            <DefaultProofELement
                                lefticon={ic_financialproof}
                                righticon={ic_add}
                                label="Third month"
                            
                            />

                </div>
                
               :
                 <div>
    
               
                  <DefaultProofELement
                  
                  lefticon={ic_financialproof}
                  righticon={ic_add}
                  label="Last 3 month Salary slips"
                  
                  />
    
                  </div>
                }
            </div>
            )
    
}


export default Salaryslip