import React, { Component } from 'react'
import Navbar from '../../../../../UI_Element/Navbar/Navbar'
import Style from './Proofs.module.css'
import ic_profile from '../../../../../#cashe/ic_profile.png';
import ic_add from '../../../../../#cashe/ic_add.png';
import ic_pan from '../../../../../#cashe/ic_pan.png';


import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { NavLink } from 'react-router-dom';
import { grey } from '@material-ui/core/colors';

export default class PANcard extends Component {
    render() {
        return (
            <div className={Style.MainHead}>
            <Navbar>
                <div className={Style.navbarinnerleft} style={{width:'120px'}}>
                            <NavLink to="/PhotoDetails" exact={true}>
                            <span><KeyboardBackspaceIcon style={{color:grey[100]}}/></span>
                            </NavLink>

                            <span>PAN Card</span>
                </div>

                <span>HELP</span>
            </Navbar>


            <div className={`container ${Style.ProofInput}`}>
                  <img src={ic_pan} width="45px"/>

                      <div className={Style.proofInner}>

                          <label>PAN Card </label>

                          <span className={Style.arrowStyle}>
                              
                              <img src={ic_add} width="34px"/>
                          </span>

                      </div>
                  </div>
           


        </div>
        )
    }
}
