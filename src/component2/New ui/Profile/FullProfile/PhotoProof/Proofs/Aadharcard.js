import React, { Component,useState } from 'react'
import Navbar from '../../../../../UI_Element/Navbar/Navbar'
// import ic_profile from '../../../../../#cashe/ic_profile.png';
import ic_add from '../../../../../#cashe/ic_add.png';
import ic_pan from '../../../../../#cashe/ic_pan.png';
import Style from './Proofs.module.css';

import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { NavLink } from 'react-router-dom';
import { grey } from '@material-ui/core/colors';
import Toggle from 'react-toggle'
import "react-toggle/style.css" 
import '../../../../../UI_Element/GlobalCss/globalStyle.css'
import DefaultProofELement from "../../../../../UI_Element/DefaultProofElement/DefaultProofELement";

const Aadharcard =()=>  {
    const [addressFalg,setAddressFalg]=useState(false)


const handleToggleChange=(e)=>{
  console.log(e.target.checked)
  setAddressFalg(e.target.checked)
  
}
        return (
            <div>

 
              <div>
            <Navbar>
                <div className={Style.navbarinnerleft} style={{width:"150px"}}>
                            <NavLink to={`/PhotoDetails`} exact={true}>
                            <span><KeyboardBackspaceIcon style={{color:grey[100]}}/></span>
                            </NavLink>
                            <span style={{fontSize:'20px'}}>Aadhar Card</span>
                </div>
                <span>HELP</span>
            </Navbar>
            </div>


            {/* ----------------------- */}
            <div className={`container ${Style.toggleStyle}`}>  

                    <span className={Style.textStyle}>Upload single document with
                     front and back
                    sides
                     </span>

                        <Toggle
                        defaultChecked={false}
                        className='custom-classname'
                        onChange={handleToggleChange}
                        icons={false}
                        />
            </div>


            {/* .------------------------------------------------ */}
                    

                    
            {addressFalg?
            <DefaultProofELement
                lefticon={ic_pan}
                righticon={ic_add}
                label="front and back sides"
            
            />
           :
             <div>

           
              <DefaultProofELement
              
              lefticon={ic_pan}
              righticon={ic_add}
              label="Aadhaar front side"
              
              />

              <DefaultProofELement
              
              lefticon={ic_pan}
              righticon={ic_add}
              label="Aadhaar back side"
             
              
              />
              </div>
            }
        </div>
        )
    }


    export default Aadharcard
