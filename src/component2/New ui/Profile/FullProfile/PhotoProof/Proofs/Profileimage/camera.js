import React, { Component ,useCallback,useEffect,useRef, useState} from 'react'
import Webcam from "react-webcam";
import Style from './style.module.css';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import {  captureImage} from '../../../../../../reduxstore/Actions/cameraActions';
const  Camera=(props)=> {
    const webcamRef = useRef(null);
    const [imageData,setImageData]=useState('')
    const  dispatch = useDispatch()
const history=useHistory()
    const videoConstraints = {
        width: 300,
        height: 300,
        facingMode: "user"
      };

 
      const capture = useCallback(() => {
          const imageSrc = webcamRef.current.getScreenshot();
          setImageData(imageSrc)
          dispatch(captureImage(imageSrc))
        //   console.log(imageSrc)
                // history.push('/Profileimage')
                props.closeCam(false)
        },[webcamRef]);
     
       
        return (
            <div className={Style.camHead}>

                  <Webcam
                    audio={false}
                    ref={webcamRef}
                    screenshotFormat="image/jpeg"
                    className={Style.cam}
                    videoConstraints={videoConstraints}
                   />

                      <button onClick={capture}>Capture</button>

            </div>

            
        )
    }

    export default Camera