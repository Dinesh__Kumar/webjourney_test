import React, { Component ,useState} from 'react'
import Navbar from '../../../../../../UI_Element/Navbar/Navbar'
import Style from '../Proofs.module.css'
import Style2 from './style.module.css'

import ic_profile from '../../../../../../#cashe/ic_profile.png';
import ic_edit from '../../../../../../#cashe/ic_edit.png';

import ic_add from '../../../../../../#cashe/ic_add.png';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { NavLink } from 'react-router-dom';
import { grey } from '@material-ui/core/colors';
import CameraComponent from '../Profileimage/camera';
import { useSelector,useDispatch } from 'react-redux';
import {  captureImage} from '../../../../../../reduxstore/Actions/cameraActions';

const  Profileimage =(props)=>  {
    const  dispatch = useDispatch()

    const [openCam,setOpenCam]=useState(false)
    const CameraStateData = useSelector(state => state.CameraData)
    console.log(CameraStateData)
    const CamHandler=()=>{
        setOpenCam(true)
        dispatch(captureImage(''))
    }

    const closeCam=(flag)=>{
        setOpenCam(flag)
    }
     
 

    let uiEle=(
        <div className={Style.profileHead}>

            {CameraStateData.avatar?<img src={CameraStateData.avatar}  className={Style2.avatar} /> :
            <img src={ic_profile} width="45px"/>
           }

            <div className={Style.proofInner}>

                <label >Profile </label>

                <span className={Style.arrowStyle}>

                {CameraStateData.avatar?<img src={ic_edit} width="34px" className={Style2.editicon} onClick={CamHandler} /> :
                <img src={ic_add} width="34px" onClick={CamHandler}/>
                  }
                    
                </span>

            </div>
        </div>
    )
        return (
            <div className={Style.MainHead}>
                <Navbar>
                    <div className={Style.navbarinnerleft} style={{width:'100px'}}>
                                <NavLink to="/PhotoDetails" exact={true}>
                                <span><KeyboardBackspaceIcon style={{color:grey[100]}}/></span>
                                </NavLink>

                                <span style={{fontSize:'20px'}}>Profile</span>
                    </div>

                    <span>HELP</span>
                </Navbar>


                <div className={`container ${Style.ProofInput}`}>

                    {openCam?
                        <div className={Style.cameraHead}>
                        <CameraComponent closeCam={closeCam}/>
                        </div>:
                          uiEle
                        }
                     
                 </div>
               


            </div>
        )
    
}


export default Profileimage