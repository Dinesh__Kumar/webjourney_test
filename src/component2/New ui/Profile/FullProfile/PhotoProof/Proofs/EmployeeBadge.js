import React, { Component } from 'react'
import Navbar from '../../../../../UI_Element/Navbar/Navbar'
// import ic_profile from '../../../../../#cashe/ic_profile.png';
import ic_add from '../../../../../#cashe/ic_add.png';
import ic_profile from '../../../../../#cashe/ic_profile.png';
import Style from './Proofs.module.css';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { NavLink } from 'react-router-dom';
import { grey } from '@material-ui/core/colors';
import Toggle from 'react-toggle'
import "react-toggle/style.css" 
import '../../../../../UI_Element/GlobalCss/globalStyle.css'
import DefaultProofELement from "../../../../../UI_Element/DefaultProofElement/DefaultProofELement";

const EmployeeBadge =()=>  {
   
        return (
            <div>
         
         <div>
            <Navbar>
                <div className={Style.navbarinnerleft} style={{width:"200px"}}>
                            <NavLink to={`/PhotoDetails`} exact={true}>
                            <span><KeyboardBackspaceIcon style={{color:grey[100]}}/></span>
                            </NavLink>
                            <span style={{fontSize:'20px'}}>Employee Badge</span>
                </div>
                <span>HELP</span>
            </Navbar>
            </div>


              <div>

              <DefaultProofELement
                lefticon={ic_profile}
                righticon={ic_add}
                label="Employee Badge"
            
            />
              </div>


            </div>
        )
    }

export default EmployeeBadge