import React, { Component } from 'react'
import Navbar from '../../../../UI_Element/Navbar/Navbar'
import DefaultForm from '../../../../UI_Element/DefaultForm/DefaultForm';
const PhotoDetails =(props)=>  {
    const submitSuccessHandler=()=>{

    }

    const clickElementHandler=(value)=>{
        console.log(value)
        props.history.push(`/${value}`)

    }
        return (
            <div>


              <DefaultForm  formType='photoProofs' 
              submitSuccessHandler={submitSuccessHandler}
              onClick={clickElementHandler}
              />

            
            </div>
        )
    }
    export default PhotoDetails
