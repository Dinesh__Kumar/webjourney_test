import React, { Component, useState } from 'react'
import DefaultBar from '../../../../UI_Element2/defaultBar/DefaultBar';
import Style from './Mobilenumber.module.css'
import DefaultFooter from '../../../../UI_Element2/DefaultFooter/DefaultFooter'
import {useDispatch,useSelector} from 'react-redux';
import {addMobileNumber} from '../../../../Reduxstore2/Actions/Formactions'
const MobileNumberScreen =(props)=>  {

    const FormStateData=useSelector(state=>state.FormData2)
     const dispatch = useDispatch()

     const [addnumber,setAddNumber]=useState(false)
     const [error,setError]=useState(false)

    const onChangeHandler=(e)=>{
       
        if(addnumber  && e.target.value.slice(4).length<=10){        
                dispatch(addMobileNumber(e.target.name,e.target.value));
                setError(false)
        }

        // }
    }

    const onkeypressHandler=(e)=>{
        if(e.keyCode===8 && FormStateData.formData.mobileNumber.mobileNumber.length===4  ){
            setAddNumber(false)
        }else{
            setAddNumber(true)
        }
    }

    const onClickHandler=()=>{
        props.history.push('/OTPverificationpage')
    }


    const onBlurHandler=(e)=>{
        if(FormStateData.formData.mobileNumber.mobileNumber.slice(4).length<=0){
            console.log('error occur')
            setError(true)
        }else{
            setError(false)
        }
    }
    // console.log(FormStateData)
        return (
            <div className={`${Style.main}`}>
               <div className={Style.bar}>
                   <DefaultBar/>
               </div>

               <div className={`${Style.formContainer} container`}>

                   <div className={Style.title}>
                       <h1>Enter Mobile </h1>
                   </div>

                   <div className={Style.inputHead}>
                         <span className={Style.label}>Mobile Number</span>
                       <input type="text"  
                        value={FormStateData.formData.mobileNumber.mobileNumber}
                        name="mobileNumber" onChange={onChangeHandler}
                         onKeyDown={onkeypressHandler}
                         onBlur={onBlurHandler}
                         style={error?{border:'2px solid red '}:{}}
                         
                         className={FormStateData.formData.mobileNumber.mobileNumber.length>4?Style.successInput:Style.defaultInput} />

                   </div>

               </div>


               <div className={Style.Footer}>
                 <DefaultFooter 
                 disabled={FormStateData.formData.mobileNumber.isValid} 
                 value="Next"
                 onClick={onClickHandler}/>
               </div>
            </div>
        )
    }



export default MobileNumberScreen