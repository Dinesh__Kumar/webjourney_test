import React, { Component, useState } from 'react'
import Style from './activeLoan.module.css'
import DropdownElement from '../../../../UI_Element2/defaultDropdownElements/defaultDropdownElements'


 
const ActivesLoans =()=> {

        return (
            <div className={Style.main}>
               <p className={Style.title}>2nd Installment Overdue</p>


            <div className={Style.list}>
                    <DropdownElement
                    label='1st installment'
                    status="PAID"
                    bgColor="#ddfded"
                    >

                    <div className={Style.details}>
                        <p>Installment Amount</p>
                        <span>Rs 7,630</span>
                    </div>
                    <div className={Style.details}>
                        <p>Due Date</p>
                        <span>03/12/20</span>
                    </div>
                    <div className={Style.details}>
                        <p>Due Amount</p>
                        <span>Rs 1,000</span>
                    </div>
                    <div className={Style.details}>
                        <p>Paid Amount</p>
                        <span>Rs 6,630</span>
                    </div>
                    </DropdownElement>




                    <DropdownElement
                   
                    label='2nd installment'
                    status="OVERDUE"
                    bgColor="#fef7b9"
                    statusTextColor="#c33e01"

                    >

                    <div className={Style.details}>
                        <p>Installment Amount</p>
                        <span>Rs 7,630</span>
                    </div>
                    <div className={Style.details}>
                        <p>Due Date</p>
                        <span>03/12/20</span>
                    </div>
                    <div className={Style.details}>
                        <p>Due Amount</p>
                        <span>Rs 1,000</span>
                    </div>
                    <div className={Style.details}>
                        <p>Paid Amount</p>
                        <span>Rs 6,630</span>
                    </div>
                    </DropdownElement>

                    
                    <DropdownElement
                    label='3rd installment'
                    status="OVERDUE"
                    bgColor="#ddfded"
                    >

                    <div className={Style.details}>
                        <p>Installment Amount</p>
                        <span>Rs 7,630</span>
                    </div>
                    <div className={Style.details}>
                        <p>Due Date</p>
                        <span>03/12/20</span>
                    </div>
                    <div className={Style.details}>
                        <p>Due Amount</p>
                        <span>Rs 1,000</span>
                    </div>
                    <div className={Style.details}>
                        <p>Paid Amount</p>
                        <span>Rs 6,630</span>
                    </div>
                    </DropdownElement>


                    
                    <DropdownElement
                    label='4th installment'
                    status="OVERDUE"
                    bgColor="#ddfded"
                    >

                    <div className={Style.details}>
                        <p>Installment Amount</p>
                        <span>Rs 7,630</span>
                    </div>
                    <div className={Style.details}>
                        <p>Due Date</p>
                        <span>03/12/20</span>
                    </div>
                    <div className={Style.details}>
                        <p>Due Amount</p>
                        <span>Rs 1,000</span>
                    </div>
                    <div className={Style.details}>
                        <p>Paid Amount</p>
                        <span>Rs 6,630</span>
                    </div>
                    </DropdownElement>


                    
                    <DropdownElement
                    label='5th installment'
                    status="OVERDUE"
                    bgColor="#ddfded"
                    >

                    <div className={Style.details}>
                        <p>Installment Amount</p>
                        <span>Rs 7,630</span>
                    </div>
                    <div className={Style.details}>
                        <p>Due Date</p>
                        <span>03/12/20</span>
                    </div>
                    <div className={Style.details}>
                        <p>Due Amount</p>
                        <span>Rs 1,000</span>
                    </div>
                    <div className={Style.details}>
                        <p>Paid Amount</p>
                        <span>Rs 6,630</span>
                    </div>
                    </DropdownElement>
            </div>
                
            </div>
        )
    }

    export default ActivesLoans
