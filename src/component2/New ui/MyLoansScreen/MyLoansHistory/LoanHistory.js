import React, { Component } from 'react'
import Style from './LoanHistory.module.css'
import DropdownElement from '../../../../UI_Element2/defaultDropdownElements/defaultDropdownElements'


const LoanHistory =()=>  {
        return (
            <div className={Style.main}>
            <p className={Style.title}>Your previous CASHe loans</p>


         <div className={Style.list}>
                 <DropdownElement
                 label='Cashe60'
                 status="ACTIVE"
                 bgColor="#eaf1fb"
                 rightbtnBgColor="#ddfded"
                 >

                 <div className={Style.details}>
                     <p>Total Amount</p>
                     <span>Rs 76,630</span>
                 </div>

                 <div className={Style.perMonthInsatllment}>
                     <p>Installements</p>
                     <span>Rs 25,500</span> 
                 </div>
                 <div className={Style.details}>
                     <p>Due Date</p>
                     <span>03/12/20</span>
                 </div>
                 <div className={Style.details}>
                     <p>Due Amount</p>
                     <span>Rs 1,000</span>
                 </div>
                 <div className={Style.details}>
                     <p>Paid Amount</p>
                     <span>Rs 6,630</span>
                 </div>
                 </DropdownElement>




                 <DropdownElement
                 bgColor="#fef7b9"
                 labelTextColor="white"
                 label='Cashe60'
                 status="03/06/2020"
                 bgColor="#2f73da"
                //  rightbtnBgColor="red"
                 statusTextColor="white"

                 >

                 <div className={Style.details}>
                     <p>Total Amount</p>
                     <span>Rs 7,630</span>
                 </div>

                 <div className={Style.perMonthInsatllment}>
                     <p>Installements</p>
                     <span>Rs 30,500</span> 
                 </div>
                 <div className={Style.details}>
                     <p>Due Date</p>
                     <span>03/12/20</span>
                 </div>
                 <div className={Style.details}>
                     <p>Due Amount</p>
                     <span>Rs 1,000</span>
                 </div>
                 <div className={Style.details}>
                     <p>Paid Amount</p>
                     <span>Rs 6,630</span>
                 </div>
                 </DropdownElement>

                 <DropdownElement
                 bgColor="#fef7b9"
                 labelTextColor="white"
                 label='Cashe60'
                 status="03/06/2020"
                 bgColor="#2f73da"
                //  rightbtnBgColor="red"
                 statusTextColor="white"

                 >

                 <div className={Style.details}>
                     <p>Total Amount</p>
                     <span>Rs 7,630</span>
                 </div>

                 <div className={Style.perMonthInsatllment}>
                     <p>Installements</p>
                     <span>Rs 30,500</span> 
                 </div>
                 <div className={Style.details}>
                     <p>Due Date</p>
                     <span>03/12/20</span>
                 </div>
                 <div className={Style.details}>
                     <p>Due Amount</p>
                     <span>Rs 1,000</span>
                 </div>
                 <div className={Style.details}>
                     <p>Paid Amount</p>
                     <span>Rs 6,630</span>
                 </div>
                 </DropdownElement>



                 
                 <DropdownElement
                 bgColor="#fef7b9"

                 label='Cashe60'
                 status="03/06/2020"
                 bgColor="#2f73da"
                //  rightbtnBgColor="red"
                 statusTextColor="white"
                 labelTextColor="white"


                 >

                 <div className={Style.details}>
                     <p>Total Amount</p>
                     <span>Rs 7,630</span>
                 </div>

                 <div className={Style.perMonthInsatllment}>
                     <p>Installements</p>
                     <span>Rs 30,500</span> 
                 </div>
                 <div className={Style.details}>
                     <p>Due Date</p>
                     <span>03/12/20</span>
                 </div>
                 <div className={Style.details}>
                     <p>Due Amount</p>
                     <span>Rs 1,000</span>
                 </div>
                 <div className={Style.details}>
                     <p>Paid Amount</p>
                     <span>Rs 6,630</span>
                 </div>
                 </DropdownElement>



         </div>
             
         </div>
        )
    }

    export default LoanHistory

