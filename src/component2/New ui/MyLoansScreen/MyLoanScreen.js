import Style from './MyLoan.module.css'
import React, { useState } from 'react'
import DefaultBar from '../../../UI_Element2/defaultBar/DefaultBar'
import DefaultFooter from '../../../UI_Element2/DefaultFooter/DefaultFooter'
import MyActivesLoans from './ActiveLoans/ActivesLoans'
import MyLoanHistory from './MyLoansHistory/LoanHistory'

const MyLoanScreen =()=> {
            const [barActive,setBarActive]=useState('bar1')

            const barActiveChangeHandler=(value)=>{
                if(value==='bar1'){
                    setBarActive('bar1') 

                }else if(value==='bar2'){
                    setBarActive('bar2') 

                }
            }
        return (
            <div className={`${Style.main} container`}>
                <div className={Style.bar}>
                    <DefaultBar/>
                </div>


                <div className={Style.container}>
                    <p className={Style.title}>Loan</p>

                   <div className={Style.progressBar}>
                            <div className={Style.progress1} onClick={()=>barActiveChangeHandler('bar1')}>
                                <p className={barActive=='bar1'?Style.activeText:Style.inactiveText}>Active Loan</p>
                                <span className={barActive=='bar1'?Style.ActiveBar:Style.inactiveBar}></span>

                            </div>

                            <div className={Style.progress2} onClick={()=>barActiveChangeHandler('bar2')}>
                                <p className={barActive=='bar2'?Style.activeText:Style.inactiveText}>History</p>
                                <span className={barActive=='bar2'?Style.ActiveBar:Style.inactiveBar}></span>
                            </div>
                   </div>

                   <div className={Style.innerContent}>
                       {barActive==='bar1'?
                        <MyActivesLoans/>

                       :
                       <MyLoanHistory/>

                       }
                   </div>

                </div>







                <div className={Style.footer}>
                    <DefaultFooter FooterType='profileFooter'/>
                </div>
            </div>
        )
    }

    export default MyLoanScreen
