import React, { useState } from 'react'
import Style from './myprofile.module.css'
import DefaultBar from "../../../UI_Element2/defaultBar/DefaultBar";
import DefaultFooter from '../../../UI_Element2/DefaultFooter/DefaultFooter';
import ProductCarousel from '../../../UI_Element2/ProductsCarousel/ProductCarousel';
import './demo.css';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import {grey,orange} from '@material-ui/core/colors';
import { useSelector,useDispatch } from "react-redux";
import ic_casheLogo from '../../../#cashe/ic_cashe.png';

import { ProgressBar } from 'react-bootstrap';

import HelloIcon from   '../../../#NewLogos/Home/hello/pana.png'
import sqlIconMini from   '../../../#NewLogos/Home/SLQ PNG_3 3.png'

import sqlScoreIcon from   '../../../#NewLogos/Home/SLQ Score 1.png'
import DefaultModel from '../../../UI_Element/Models/defaultModel/DefaultModel'
import Defaultinput from '../../../UI_Element2/defaultInput2/DefaultInput'
import DefaultForm from '../../../UI_Element2/defaultForm2/defaultForm'

import ProductsModel from '../../../UI_Element2/ProductsModel/ProductsModel'
import {productsModalHandler} from '../../../Reduxstore2/Actions/DefaultControllerActions'
import DefaultModal from '../../../UI_Element2/DefaultModal/DefaultModal'
import {useHistory,NavLink} from 'react-router-dom'
import {defaultModalHandler} from '../../../Reduxstore2/Actions/DefaultControllerActions'


const HomeScreen =(props)=> {
const history=useHistory()
    const dispatch=useDispatch();
    const FormStateData=useSelector(state=>state.FormData2)
    const DefaultControllerStateData=useSelector(state=>state.DefaultControllerData)
    const loanStateData=useSelector(state=>state.LoanData)
    const [openSuccessModal,setopenSuccessModal]=useState(loanStateData.isLoanAppliedSuccess&&loanStateData.loanData.isLoanDataValid)


console.log(DefaultControllerStateData)
    const Okhandler=()=>{
        history.push('/CurrentAddress')
    
       }

       const openProductModalHandler=()=>{

        dispatch(productsModalHandler(true))
       }
       const closeModalHandler=(modalType)=>{
           console.log('im closing')
            dispatch(defaultModalHandler(false))
            dispatch(productsModalHandler(false))
            setopenSuccessModal(false)
        if(modalType==='loanAppliedModel'&&loanStateData.isLoanAppliedSuccess&&loanStateData.loanData.isLoanDataValid){
                history.push('/MyLoans')
        }

       }
       console.log(loanStateData)
        return (
            <div className={Style.main}>
                {DefaultControllerStateData.isProductModelOpen&&
                <div className={Style.ProductsModel}>
                    <ProductsModel
                    openProductModalFlag={DefaultControllerStateData.isProductModelOpen}
                    closeModalHandler={closeModalHandler}
                    />
                </div>  
                }

                {(DefaultControllerStateData.isModalOpen&&openSuccessModal)&&

                <div className={Style.LoanAppliedSuccessModal}>
                        <DefaultModal
                        modalType="successModal"
                        openProductModalFlag={openSuccessModal}
                        closeModalHandler={()=>closeModalHandler('loanAppliedModel')}
                       />

                </div>
                
                }
               
                <div className={Style.bar}>
                   <DefaultBar />
                </div>

                {/* ----------------------- */}
                {(!FormStateData.residentialAddress_IsValid)&&
                <div  className={`container ${Style.DefaultModelStyle}`}>
                <DefaultModel>
                    <div style={{padding:'10px',boxSizing:'border-box'}}>
                                 <div className={Style.logoStyle}>
                                     <img src={ic_casheLogo} width="50px"/> <span style={{fontSize:'25px' }}>CASHe</span>
                                    </div>
                    <p>Help us to proceed your application quickly.Please enter your required loan amount!</p>

                            <DefaultForm
                              formType='Required_Loan_Amount_Form'
                            />
    
                    </div>
                        <div className={`container ${Style.bottmBtn}`}>
                            <button   onClick={Okhandler}>Ok</button>
                        </div>
                       
               
                </DefaultModel>
                </div>
                
                } 


                {/* ------------------ */}

                <div className={Style.container}>

                    <div className={Style.box1}>

                            {props.children}
                        
                    </div>


                    <div className={`${Style.box2} container`}>

                                <div className={`${Style.innerbox2_loanType} `}>

                                        <p className={Style.innerbox2_title}>Instant Loans</p>

                                            <div className={Style.innerbox2_loanTypeBox}>
                                        
                                                <ProductCarousel/>

                                            </div>

                                        <p className={Style.innerBox2_showAlltext} onClick={openProductModalHandler}>Show All Products</p>
                                        
                                </div>

                                        <div className={`${Style.innerbox2_profile_box}`}>

                                            <div className={Style.title}>
                                                <p>Complete Your Profile to apply for a loan</p>
                                            </div>
                                            

                                            <div className={`${Style.productDetails} container`}>
                                                <div className={Style.progressbarOuter}>
                                                    <div className={Style.labelHead}>
                                                    <span>Profile Complete</span>
                                                    <span>75%</span>
                                                    </div>
                                                <div className={Style.progressbar}>
                                                    <ProgressBar  now={75} 
                                                    variant={'progressBar'} 
                                                    style={{width:'100%',borderRadius:'10px 0px 0px 10px'}}
                                                    />
                                                    </div>
                                                </div>

                                                    <div className={Style.details}>
                                                    <div className={Style.detailsInner}>

                                                            <NavLink to="/CurrentAddress" exact={true}>Residential Address</NavLink>
                                                            <NavLink to="/PersonalDetails" exact={true}>Personal Details</NavLink>
                                                            <NavLink to="/EmploymentDetails" exact={true}>Employment Details</NavLink>
                                                            <NavLink to="/BankDetails" exact={true}>Bank Details</NavLink>
                                                            <NavLink to="/DocumentsUpload" exact={true}>Photo Proofs</NavLink>
                                                            {/* PersonalDetails */}
                                                        </div>

                                                        <div className={Style.detailsStatus}>
                                                            <NavLink to="/CurrentAddress" exact={true}>Fill Details <ArrowForwardIosIcon style={{color:orange[600],fontSize:15}}/></NavLink>
                                                            <NavLink to="/PersonalDetails" exact={true}>Fill Details <ArrowForwardIosIcon style={{color:orange[600],fontSize:15}}/></NavLink>
                                                            <NavLink to="/EmploymentDetails" exact={true}>Fill Details <ArrowForwardIosIcon  style={{color:orange[600],fontSize:15}}/></NavLink>
                                                            <NavLink  to="/BankDetails" exact={true}>Fill Details <ArrowForwardIosIcon  style={{color:orange[600],fontSize:15}}/></NavLink>
                                                            <NavLink to="/DocumentsUpload" exact={true}>Fill Details <ArrowForwardIosIcon  style={{color:orange[600],fontSize:15}}/></NavLink>
                                                        </div>
                                                    


                                                    </div>
                                                   </div>
                                           </div>

                                           <div className={Style.innerbox2_slq}>
                                                <div className={Style.slqLogo}>
                                                    <img src={sqlIconMini} alt="logo"/>
                                                </div>

                                                    <p className={Style.slqtitle}>
                                                        Social Loan Quotient</p>

                                                <div className={Style.slqMainlogo}>
                                                <img src={sqlScoreIcon} alt="logo"/>

                                                </div>


                                                <p className={Style.sqlText}>
                                                The next generation credit intelligence platform for a faster,
                                                 accurate lending
                                                </p>



                                           </div>
                        
                    </div>

                </div>


                <div className={Style.footer}>

                    <DefaultFooter
                    FooterType='profileFooter'
                    />

                </div>
            </div>
        )
    }

export default HomeScreen
