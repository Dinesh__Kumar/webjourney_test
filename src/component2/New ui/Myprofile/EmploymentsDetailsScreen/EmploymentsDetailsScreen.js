
import React, { useEffect } from 'react'
import Style from './EmploymentsDetailsScreen.module.css'
import DefaultBar from "../../../../UI_Element2/defaultBar/DefaultBar";
import DefaultFooter from '../../../../UI_Element2/DefaultFooter/DefaultFooter';
import DefaultForm from '../../../../UI_Element2/defaultForm2/defaultForm';
import { useSelector,useDispatch} from 'react-redux';
import { addCurrentScreen,removeCurrentScreen } from "../../../../Reduxstore2/Actions/screenProgressStatusAction";

import DefaultScreenProgressBar from '../../../../UI_Element2/DefaultScreenProgressBar/DefaultScreenProgressBar';
const EmploymentsDetailsScreen =(props)=> {

    const FormStateData=useSelector(state=>state.FormData2)
    console.log(FormStateData)
    const CurrentScreenStateData=useSelector(state=>state.CurrentScreenData)

    console.log(CurrentScreenStateData)
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(addCurrentScreen('EmploymentDetails'))
    
    },[])   
    const onClickHandler=()=>{
        props.history.push('/BankDetails')
    }

    const goBackHandler=()=>{
        props.history.push('/PersonalDetails')

        dispatch(removeCurrentScreen('EmploymentDetails'))

    
    }
        return (
            <div className={Style.main}>
                <div className={Style.bar}>
                   <DefaultBar
                    goBackHandler={goBackHandler}
                   />
                </div>

                <div className={Style.container}>

                    <div className={Style.progress}>
                        <DefaultScreenProgressBar/>
                        
                    </div>

                    <div>
                    <h1 className={`${Style.title} container`}>Employment Info</h1>

                    </div>



                    <div className={Style.formContainer}>
                          <DefaultForm
                          formType='Employment_Details_Form'
                          />
                    </div>

                </div>

                <div className={Style.footer}>

                    <DefaultFooter
                     value="Next"
                     disabled={FormStateData.employmentDetails_IsValid}
                     onClick={onClickHandler}
                    />

                </div>
            </div>
        )
    }

export default EmploymentsDetailsScreen
