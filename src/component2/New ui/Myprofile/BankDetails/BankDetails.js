
    
import React, { useEffect } from 'react'
import Style from './BankDetails.module.css'
import { addCurrentScreen,removeCurrentScreen } from "../../../../Reduxstore2/Actions/screenProgressStatusAction";

import DefaultBar from "../../../../UI_Element2/defaultBar/DefaultBar";
import DefaultFooter from '../../../../UI_Element2/DefaultFooter/DefaultFooter';
import DefaultForm from '../../../../UI_Element2/defaultForm2/defaultForm';
import {useDispatch, useSelector} from 'react-redux';
import DefaultScreenProgressBar from '../../../../UI_Element2/DefaultScreenProgressBar/DefaultScreenProgressBar';
const BankDetails =(props)=> {

    const CurrentScreenStateData=useSelector(state=>state.CurrentScreenData)

    console.log(CurrentScreenStateData)
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(addCurrentScreen('BankDetails'))
    
    },[])   

    const FormStateData=useSelector(state=>state.FormData2)
    console.log(FormStateData)

    const bankData=['SBI','HDFC','KOTAK MAHINDRA','PUNJAB NATIONAL BANK',
   'AXIS BANK'

      ]

      const clickHandler=()=>{
          props.history.push('/DocumentsUpload')
      }


      const goBackHandler=()=>{
        props.history.push('/EmploymentDetails')
        dispatch(removeCurrentScreen('BankDetails'))

    
    }

        return (
            <div className={Style.main}>
                <div className={Style.bar}>
                   <DefaultBar
                    
                    goBackHandler={goBackHandler}

                   
                   />
                </div>

                <div className={Style.container}>

                    <div className={Style.progress}>
                        <DefaultScreenProgressBar/>
                        
                    </div>

                    <div>
                    <h1 className={`${Style.title} container`}>Banking</h1>

                    </div>



                    <div className={Style.formContainer}>
                          <DefaultForm
                          formType='Bank_Details_Form'
                          bankData={bankData}
                          />
                    </div>

                </div>

                <div className={Style.footer}>

                    <DefaultFooter
                     value="Next"
                     onClick={clickHandler}
                     disabled={FormStateData.bankDetails_IsValid}
                    />

                </div>
            </div>
        )
    }



    export default BankDetails
