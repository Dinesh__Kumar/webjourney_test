

import React, { Component, useEffect } from 'react'
import Style from './personalDetails.module.css'
import DefaultBar from "../../../../UI_Element2/defaultBar/DefaultBar";
import DefaultFooter from '../../../../UI_Element2/DefaultFooter/DefaultFooter';
import DefaultForm from '../../../../UI_Element2/defaultForm2/defaultForm';
import { useSelector,useDispatch} from 'react-redux';
import { addCurrentScreen ,removeCurrentScreen} from "../../../../Reduxstore2/Actions/screenProgressStatusAction";
import DefaultScreenProgressBar from '../../../../UI_Element2/DefaultScreenProgressBar/DefaultScreenProgressBar';

const PersonalDetailsScreen =(props)=> {
    const FormStateData=useSelector(state=>state.FormData2)
    const CurrentScreenStateData=useSelector(state=>state.CurrentScreenData)

    console.log(CurrentScreenStateData)

    console.log(FormStateData)
const dispatch = useDispatch()
useEffect(()=>{
    dispatch(addCurrentScreen('PersonalDetails'))

},[])

const onClickHandler=()=>{
    props.history.push('/EmploymentDetails')
}

const goBackHandler=()=>{
    props.history.push('/CurrentAddress')
    dispatch(removeCurrentScreen('PersonalDetails'))


}
        return (
            <div className={Style.main}>
                <div className={Style.bar}>
                   <DefaultBar 

                    goBackHandler={goBackHandler}
                   />
                </div>

                <div className={Style.container}>

                    <div className={Style.progress}>
                        <DefaultScreenProgressBar/>
                        
                    </div>

                    <h1 className={`${Style.title} container`}>Profile</h1>


                    <div className={Style.formContainer}>
                          <DefaultForm
                          formType='Profile_Details_Form'
                          />
                    </div>

                </div>

                <div className={Style.footer}>

                    <DefaultFooter
                     value="Next"
                     disabled={FormStateData.personalDetails_IsValid}
                     onClick={onClickHandler}
                    />

                </div>
            </div>
        )
    }

export default PersonalDetailsScreen
