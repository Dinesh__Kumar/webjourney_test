import React, { useEffect,useState } from 'react'
import Style from './DocumentsDetailsScreen.module.css'
import { useSelector,useDispatch} from 'react-redux';
import { addCurrentScreen ,removeCurrentScreen} from "../../../../Reduxstore2/Actions/screenProgressStatusAction";
import { cameraOpenHandler} from "../../../../reduxstore/Actions/cameraActions";


import DefaultBar from "../../../../UI_Element2/defaultBar/DefaultBar";
import DefaultFooter from '../../../../UI_Element2/DefaultFooter/DefaultFooter';
import DefaultForm from '../../../../UI_Element2/defaultForm2/defaultForm';

import DefaultScreenProgressBar from '../../../../UI_Element2/DefaultScreenProgressBar/DefaultScreenProgressBar';


import DefaultBackDrop from '../../../../UI_Element2/Backdrop/Backdrop';

const DocumentsDetailsScreen =(props)=> {


    const [backDropOpen,setBackDropOpen]=useState(false)
    const [camOpen,setCamOpen]=useState(false)



    const FormStateData=useSelector(state=>state.FormData2)
    const camData=useSelector(state=>state.CameraData)
    console.log(FormStateData)
    console.log(camData)
    const CurrentScreenStateData=useSelector(state=>state.CurrentScreenData)

  
    console.log(CurrentScreenStateData)
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(addCurrentScreen('PhotoProofs'))
    
    },[])   

    useEffect(()=>{
        return ()=>{
            console.log('Documentupload screen Unmounted')
        }
    },[])


    const onClickHandler=()=>{
        setBackDropOpen(true)
        setTimeout(() => {
            props.history.push('/Myprofile')
            setBackDropOpen(false)


        }, 2000);
    }

    const goBackHandler=()=>{
        props.history.push('/BankDetails')
        setCamOpen(false)
        dispatch(removeCurrentScreen('PhotoProofs'))
    dispatch(cameraOpenHandler(false))
    
    }




        return (
                 <div className={Style.main}>

                     <div>
                         <DefaultBackDrop backDropOpen={backDropOpen} />
                     </div>
                     
                <div className={Style.bar}>
                   <DefaultBar
                   
                   goBackHandler={goBackHandler}
                   />
                </div>

                <div className={Style.container}>

                    <div className={Style.progress}>
                        <DefaultScreenProgressBar/>
                    </div>

                    <div>
                    <h1 className={`${Style.title} container`}>Documents Upload</h1>

                    </div>



                    <div className={Style.formContainer}>
                          <DefaultForm
                          formType='documents_upload_form'
                          docData={['Voter ID','Aadhar Card','Driving License']}
                          />
                    </div>

                </div>

                <div className={Style.footer}>

                    <DefaultFooter
                     value="Submit"
                     backgroundColor="orange"
                     disabled={FormStateData.documnetDetails_IsValid}
                     onClick={onClickHandler}
                    />

                </div>
            </div>
              
        )
    }

export default DocumentsDetailsScreen




