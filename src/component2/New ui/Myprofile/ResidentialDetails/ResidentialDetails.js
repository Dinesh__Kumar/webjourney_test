import  Style  from "./address.module.css"
import React, { useState,useEffect } from 'react'
import DefaultBar from '../../../../UI_Element2/defaultBar/DefaultBar';
import DefaultFooter from '../../../../UI_Element2/DefaultFooter/DefaultFooter';
import DefaultForm from "../../../../UI_Element2/defaultForm2/defaultForm";
import { useSelector ,useDispatch} from "react-redux";
import { addCurrentScreen,removeCurrentScreen } from "../../../../Reduxstore2/Actions/screenProgressStatusAction";
import DefaultScreenProgressBar from '../../../../UI_Element2/DefaultScreenProgressBar/DefaultScreenProgressBar';

const ResidentialDetails =(props)=> {
    const FormStateData=useSelector(state=>state.FormData2)
    const [showModel,setShowModel]=useState(false)
    const CurrentScreenStateData=useSelector(state=>state.CurrentScreenData)

    console.log(CurrentScreenStateData)

    const onClickHandler=()=>{
        if(CurrentScreenStateData.currentScreen.includes('CurrentAddressScreenFilledSuccess'||'PersonalDetails'||'EmploymentDetails'||'BankDetails' || 'DocumentsUpload')){
            props.history.push('/PersonalDetails')
        }else{
            setShowModel(true)
            dispatch(addCurrentScreen('CurrentAddressScreenFilledSuccess'))


        }
        // props.history.push('/Myprofile')
    }

    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(addCurrentScreen('CurrentAddress'))
    
    },[])   

    const addresAddedSuccess=()=>{
        setShowModel(false)
        props.history.push('/Myprofile')
    }


    const goBackHandler=()=>{
        props.history.push('/Myprofile')

        // dispatch(removeCurrentScreen('CurrentAddress'))

    
    }
        return (
            <div className={Style.main}>
                  <div className={Style.bar}>
                        <DefaultBar
                        goBackHandler={goBackHandler}
                        
                        />
                  </div>

                  <div className={`${Style.fromContent} `}>
                       
                  <div className={Style.progress}>
                        <DefaultScreenProgressBar/>
                        
                    </div>

                       <div className={Style.title}>
                           <span>Residential</span> 
                            <p className={Style.line_36}></p>
                        </div>
                        


                       <div className={`${Style.formInnerContent}`}>

                            <DefaultForm 
                            
                            formType="Residential_address_form"
                            modelType='residentialAddressSuccessModel'
                            openDefaultModel={showModel} 
                            submitSuccessHandler={addresAddedSuccess}
                            
                            />
                       </div>
                  </div>

                  <div className={Style.Footer}>
                        <DefaultFooter 
                        value="Next"
                        disabled={FormStateData.residentialAddress_IsValid}
                        onClick={onClickHandler}

                        

                        />
                  </div>
            </div>
        )
    }



export default ResidentialDetails